
#include <string.h>
#include "types.h"
#include "att.h"
#include "peripheral.h"
#include "OSAL.h"
#include "bcomdef.h"
#include "pwrmgr.h"
#include "bleuart.h"
#include "error.h"
#include "log.h"
#include "bleuart_protocol.h"
#include "bleuart_service.h"

//这里是命令枚举！！
BLE_TX_CMD ble_tx_command;    //根据指令,向蓝牙APP返回相应的数据字符串


BLE_Data_Buffer_Typedef Ble_Buffer;  //这里是蓝牙通讯数据缓冲区

/**
 * @brief   会触发Ble_Rx_Action,就是接受动作函数
 * @param   无
 * @retval  无
 */
static void ble_rx_action_timer(uint16_t timeout)
{
  LOG("ble_rx_action_timer\n");
  osal_start_timerEx(bleuart_TaskID, BUP_OSAL_EVT_BLE_TIMER, timeout);
}

/**
 * @brief   会触发函数,将数据从发送缓冲区,发送到蓝牙上
 * @param   无
 * @retval  无
 */
static void ble_tx_start_timer(uint16_t timeout)
{
  LOG("ble_tx_start_timer\n");  //触发Ble_data_send函数
  osal_start_timerEx(bleuart_TaskID, BUP_OSAL_EVT_UARTRX_TIMER, timeout);
}

/**
 * @brief 	对接收数据区域的数据进行解析
 * @param	无
 * @retval 	无
 */
int Ble_Rx_Action(void)
{
  BLE_Data_Buffer_Typedef* pctx = &Ble_Buffer;

  if(pctx->ble_rx_state != BUP_RX_ST_IDLE && pctx->ble_rx_size)
  {	
    pctx->ble_rx_state = BUP_RX_ST_SENDING;  //表示正在接受!! !!
	/*这里可以写,真正的数据处理函数*/

	/**
	 * 
	 * 
	 */
    pctx->ble_rx_size = 0;   //数据处理完成,才可对
  	pctx->ble_rx_state = BUP_RX_ST_IDLE;	//表示接受数据处理完成
    return PPlus_SUCCESS;
  }
  LOG("Ble_Rx_Action Error !\n");
  return PPlus_ERR_INVALID_STATE;
}

/**
 * @brief 	将蓝牙接收到的数据复制到蓝牙接受缓冲区  
 * @param	无
 * @retval 	无
 */
int Receive_data_from_ble(uint8_t* pdata, uint8_t size)
{	
	BLE_Data_Buffer_Typedef* pctx = &Ble_Buffer;

	switch(pctx->ble_rx_state)
	{   //这里是将数据copy一下
		case BUP_RX_ST_IDLE:  //将蓝牙接收到的数据,放到蓝牙接收缓冲区
			memcpy(pctx->ble_rx_buf + pctx->ble_rx_size, pdata, size); 
			pctx->ble_rx_size += size; 
			pctx->ble_rx_state = BUP_RX_ST_DELAY_SLOT;
			ble_rx_action_timer(1);  //1ms delay slot  其实就是触发Ble_Rx_Action
			break;
		case BUP_RX_ST_DELAY_SLOT:
		case BUP_RX_ST_SENDING:  //表示正在进行数据处理
			memcpy(pctx->ble_rx_buf + pctx->ble_rx_size, pdata, size);
			pctx->ble_rx_size += size;
			break;
		default:
			Ble_Buffer.ble_rx_state = BUP_RX_ST_IDLE;
			break;
	}
	return PPlus_SUCCESS;
}

/****************************************************************************************************/

/**
 * @brief 	将蓝牙发送缓冲区的数据通过蓝牙发送出去
 * @param	无
 * @retval 	当前发送的状态
 */
int Ble_data_send(void)   
{
	BLE_Data_Buffer_Typedef* pctx = &Ble_Buffer;
	bool start_flg = FALSE;
	uint16 mtu_size=0;
	bStatus_t ret = 0;
	
	mtu_size=gAttMtuSize[gapConnHandle];
  	LOG("mtu=%d,%d,%d\n",gAttMtuSize[gapConnHandle],pctx->ble_tx_state,pctx->ble_tx_size);
	{
		mtu_size -=3;
	}	
	
	if(pctx->ble_tx_state != BUP_TX_ST_IDLE && pctx->ble_tx_size)
	{ 
		if(bleuart_NotifyIsReady() == FALSE)  //还没有准备好
		{
			return PPlus_ERR_BLE_NOT_READY;
		}

		if(pctx->ble_tx_state == BUP_TX_ST_DELAY_SLOT)
		{	//处于合法的发送状态
			start_flg = TRUE;
			FLOW_CTRL_BLE_TX_LOCK();   //这个东东不知道是干啥的???
			pctx->ble_tx_state = BUP_TX_ST_SENDING;  //由BUP_RX_ST_DELAY_SLOT状态转变为BUP_RX_ST_SENDING
		}
		// BUP_RX_ST_IDLE = 0,
		// BUP_RX_ST_DELAY_SLOT,
		// BUP_RX_ST_SENDING
		while(1)
		{	//循环发送直到,发送完成   low_profile  
			uint8_t size =0;
			
			attHandleValueNoti_t notify_data={0};
			size = ((pctx->ble_tx_size - pctx->ble_tx_offset) > mtu_size) ? mtu_size : pctx->ble_tx_size - pctx->ble_tx_offset;
			//这里应该是控制每次发送的最大长度
			memcpy(notify_data.value,pctx->ble_tx_buf + pctx->ble_tx_offset, size);  //这里pctx->ble_tx_buf + pctx->rx_offset得好好探讨一下
			notify_data.len = size;  //要发送数据的长度
			
			ret = bleuart_Notify(gapConnHandle, &notify_data, bleuart_TaskID);  //这个函数是核心！！！！
			LOG("bleuart_Notify: %d, %d, %d\n", ret,pctx->ble_tx_offset, pctx->ble_tx_size);
			if(ret == SUCCESS)
			{  //将rx_offset的大小加上 size
				pctx->ble_tx_offset += size;    //数据位置移动
			}
			else
			{  //这里是失败了
				LOG("TX R=%x\n",ret);
				if(ret == MSG_BUFFER_NOT_AVAIL || ret == INVALID_INTERRUPT_ID)
				{
					if(start_flg)
					{
						ble_tx_start_timer(1);  //重新发送!!!!!!  触发Ble_data_send函数
					}
					else
					{ //
						ble_tx_start_timer(bleuart_conn_interval()-1); //重新发送!!!!!!  触发Ble_data_send函数 
					}
					return PPlus_SUCCESS;
				}
				else
				{
					return PPlus_ERR_BUSY;
				}
			}
		
			if(pctx->ble_tx_offset == pctx->ble_tx_size)	 //在发送的过程中rx_size还是会变化
			{	//rx_size是属于将要发送的数据得长度		这里表示数据发送完成
				LOG("Success\n");     
				pctx->ble_tx_state = BUP_TX_ST_IDLE;   //标记为发送空闲
				pctx->ble_tx_offset = 0;
				pctx->ble_tx_size = 0;
				FLOW_CTRL_BLE_TX_UNLOCK();
				return PPlus_SUCCESS;
			}
		}
	}
	else
	{
		LOG("U2B s=:%x,%x",pctx->ble_tx_state,pctx->ble_tx_size);
		pctx->ble_tx_state = BUP_TX_ST_IDLE;
		pctx->ble_tx_offset = 0;
		pctx->ble_tx_size = 0;
		FLOW_CTRL_BLE_TX_UNLOCK();  //解锁
	}	

	LOG("U2B ret : %x\n",ret);
	return PPlus_ERR_INVALID_STATE;
}

/**
 * @brief 	将待发送的数据转运到蓝牙发送数据缓冲区
 * @param	无
 * @retval 	无
 */
int Transfer_data_to_ble_Txbuffer(void)
{
  	BLE_Data_Buffer_Typedef* pctx = &Ble_Buffer;
	LOG("Transfer_data_to_ble_Txbuffer\n");
  	if(pctx->ble_conn_state == FALSE)
	{	//这里表示蓝牙没有连接
		LOG("no cnt\n");
		pctx->ble_tx_size = 0;
		pctx->ble_tx_offset = 0;
		pctx->ble_tx_buffer_size = 0;
		return PPlus_ERR_INVALID_STATE;
  	}  //                     紧跟着以前数据的尾巴
  	memcpy(pctx->ble_tx_buf + pctx->ble_tx_size, pctx->ble_tx_buffer, pctx->ble_tx_buffer_size);  //ble_tx_buffer_size表示将要被复制的数据的大小
  	if(pctx->ble_tx_offset != 0)
	{	//此时,表示还没发送完成   就不会触发Ble_data_send函数  上面的复制就没有用了
		return PPlus_ERR_BUSY;
  	}
  	pctx->ble_tx_size += pctx->ble_tx_buffer_size;
  
	pctx->ble_tx_buffer_size = 0;
  	switch(pctx->ble_tx_state)
	{
		case BUP_TX_ST_IDLE:
			pctx->ble_tx_state = BUP_TX_ST_DELAY_SLOT;
			ble_tx_start_timer(1);  //触发将蓝牙发送区的数据,往蓝牙上发送  触发Ble_data_send函数
			break;
		case BUP_TX_ST_DELAY_SLOT:
		case BUP_TX_ST_SENDING:
		default:
			//drop data
			LOG("U2B err:%d\n",pctx->ble_tx_state);
			return PPlus_ERR_INVALID_STATE;
  	}
  	return PPlus_SUCCESS;
}


/**
 * @brief  触发函数,将数据从发送缓冲区,搬运至蓝牙发送区
 * @param  无
 * @retval 无
 */
static void uartrx_timeout_timer_start(void)
{
  osal_start_timerEx(bleuart_TaskID, BUP_OSAL_EVT_UART_TO_TIMER, 10);
}

/**
 * @brief  触发函数,将数据从发送缓冲区,搬运至蓝牙发送区
 * @param  无
 * @retval 无
 */
static void uartrx_timeout_timer_stop(void)
{
  osal_stop_timerEx(bleuart_TaskID, BUP_OSAL_EVT_UART_TO_TIMER);
}

/**
 * @brief  蓝牙发送函数 会触发调用Transfer_data_to_ble_Txbuffer函数  这个只是基础的函数,其它发送函数得调用它
 * @param  无
 * @retval 无
 */
void Ble_Tx_Action(void)
{
  	BLE_Data_Buffer_Typedef* pctx = & Ble_Buffer;

	if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
	{  //数据超过缓冲区的大小
		break;
	}		
	uartrx_timeout_timer_stop();
	uartrx_timeout_timer_start();
}
/****************************************************************************************************/


/**
 * @brief   当蓝牙连接断开时,就会调用此函数
 * @param   无
 * @retval  无
 */
int BUP_disconnect_handler(void)
{
	memset(&Ble_Buffer, 0, sizeof(Ble_Buffer));
	return PPlus_SUCCESS;
}

/**
 * @brief   当蓝牙连接上时,就会调用此函数
 * @param   无
 * @retval  无
 */
int BUP_connect_handler(void)
{
	if(Ble_Buffer.ble_conn_state == FALSE)
	{
		memset(&Ble_Buffer, 0, sizeof(Ble_Buffer));
		Ble_Buffer.ble_conn_state = TRUE;
	}
	return PPlus_SUCCESS;
}









