
#ifndef _BLE_UART_PROTOCOL_H
#define _BLE_UART_PROTOCOL_H

#include "types.h"
#include "osal_snv.h"
#include "flash.h"

#define UART_RX_BUF_SIZE  1024 
#define UART_TX_BUF_SIZE  512

typedef struct 
{
  uint8_t ev;

}BUP_Evt_t;

typedef enum
{
	BLE_TX_STATE,
	BLE_TX_TRUE,
	BLE_TX_FALSE
}BLE_TX_CMD;


enum
{
  BUP_RX_ST_IDLE = 0,
  BUP_RX_ST_DELAY_SLOT,
  BUP_RX_ST_SENDING
};

enum
{
  BUP_TX_ST_IDLE = 0,
  BUP_TX_ST_DELAY_SLOT,
  BUP_TX_ST_SENDING
};

//通过这个地方建立双缓冲
typedef struct
{
  bool    ble_conn_state;

  uint8_t ble_tx_state;         //这里是针对蓝牙发送函数的
  uint8_t ble_tx_size;
  uint8_t ble_tx_offset;
  uint8_t ble_tx_buf[UART_TX_BUF_SIZE];      //这里表示将要发送到蓝牙的数据

  uint8_t ble_rx_state;         //这里是针对蓝牙接受函数的
  uint8_t ble_rx_size;
  uint8_t ble_rx_buf[UART_TX_BUF_SIZE];      //这里存储蓝牙接受到的数据


  uint8_t ble_tx_buffer_size;   //这里是针对蓝牙发送缓冲函数的
  uint8_t ble_tx_buffer[UART_RX_BUF_SIZE];   //这里存储蓝牙发送缓冲区中的数据  
  //uint8_t hal_uart_tx_buf[UART_TX_BUF_SIZE];
  
}BLE_Data_Buffer_Typedef;



typedef void (*BUP_CB_t)(BUP_Evt_t* pev);

int BUP_disconnect_handler(void);
int BUP_connect_handler(void);

int Ble_Rx_Action(void);  //这里主要写一些,命令处理函数
int Receive_data_from_ble(uint8_t* pdata, uint8_t size); //主要是将蓝牙数据复制到蓝牙接受数组中

int Ble_data_send(void);  //将蓝牙数据发送出去
int Transfer_data_to_ble_Txbuffer(void);  //主要是将蓝牙发送缓冲区中的数据,复制到蓝牙发送数据数组中
void Ble_Tx_Action(void); //这个是上层的函数都会调用的发送函数

#endif /*_BLE_UART_PROTOCOL_H*/

