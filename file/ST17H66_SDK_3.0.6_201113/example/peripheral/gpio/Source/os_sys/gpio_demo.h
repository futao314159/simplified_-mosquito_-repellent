/**************************************************************************************************
*******
**************************************************************************************************/

/**************************************************************************************************
  Filename:       gpio_demo.h
  Revised:        $Date $
  Revision:       $Revision $


**************************************************************************************************/

#ifndef __GPIO_DEMO_H__
#define __GPIO_DEMO_H__

#include "types.h"

#ifdef __cplusplus
extern "C"
{
#endif
    
#define GPIO_WAKEUP_PIN_NUM     3    

/*********************************************************************
 * FUNCTIONS
 */



void Control_ProcessInit( uint8 task_id );
uint16 Control_ProcessEvent( uint8 task_id, uint16 events);

void Key_ProcessInit( uint8 task_id );
uint16 Key_ProcessEvent( uint8 task_id, uint16 events);

void Buzzer_ProcessInit( uint8 task_id);
uint16 Buzzer_ProcessEvent( uint8 task_id, uint16 events);


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* HEARTRATE_H */
