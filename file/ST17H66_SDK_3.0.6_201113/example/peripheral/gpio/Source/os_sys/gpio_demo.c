
/**
 *   先把工程写出来,最后再重新组织工程文件 
 */

/*********************************************************************
 * INCLUDES
 */

#include "OSAL.h"
#include "gpio_demo.h"
#include "log.h"
#include "pwm.h"
#include "gpio.h"
#include "clock.h"
#include "pwrmgr.h"
#include "error.h"
#include "adc.h"

//这里是HC164的数据管脚
#define HC164_SDA		P34		//HC164的数据脚
#define HC164_SCL		P2      //HC164的时钟脚

//这里是按键的输入管脚(包括泵计数)
#define KEY_GPIO		P7		//按键GPIO
#define PUMP_CNT_GPIO   P11	    //泵计数GPIO

//这里是泵控制管脚
#define PUMP_CONTROL    P20     //泵控制GPIO

/*****************************************************************************************************/
#define CONTROL_CYCLE_TIMER     0x0001
static uint8 Control_TaskID;

#define KEY_CYCLE_TIMER     0x0002
static uint8 key_TaskID;

#define BUZZER_START_TIMER     0x0001
#define BUZZER_1TIMES_TIMER     0x0002
#define BUZZER_3TIMES_TIMER     0x0004
static uint8 buzzer_TaskID;

static uint8_t beep_type=0;

static uint16_t HC164_DATA=0x01FF;   //存储HC164待发送数据用的变量
/*****************************************************************************************************/

/*****************************************************************************************************/


/**
 *	@brief  HC164串转并驱动函数 
 *	@param	udata :待发送的数据
 *	@return	无
 */
static void SendDataTo164(uint16_t udata)
{
	uint16_t i;

	for (i = 0; i < 11; i++)
	{	//因为这里有11个数据需要发送,故循环11次
		if (udata & 0x0400)
		{
			hal_gpio_fast_write(HC164_SDA,1);
			hal_gpio_fast_write(HC164_SCL,0);
			hal_gpio_fast_write(HC164_SCL,1);
		}
		else
		{
			hal_gpio_fast_write(HC164_SDA,0);
			hal_gpio_fast_write(HC164_SCL,0);
			hal_gpio_fast_write(HC164_SCL,1);
		}
		udata = udata << 1;
	}
}

/**
 *	@brief  HC164串转并驱动函数 
 *	@param	udata :待发送的数据
 *	@return	无
 */
static void InitHC164(void)
{
	hal_gpio_pin_init(HC164_SDA,OEN);
	hal_gpio_pin_init(HC164_SCL,OEN);
	hal_gpioretention_register(HC164_SDA);//enable this pin retention
	hal_gpioretention_register(HC164_SCL);//enable this pin retention
	hal_gpio_fast_write(HC164_SDA,1);
	hal_gpio_fast_write(HC164_SCL,1);

	SendDataTo164(0x1FF);
}


/**
 *	@brief  5~15m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate5_15m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x7E;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  15~30m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate15_30m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x7D;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  30~50m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate30_50m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x7B;     //
	SendDataTo164(HC164_DATA);
}


/**
 *	@brief  50~80m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate50_80m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x77;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  80~100m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate80_100m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x6F;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  100~150m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate100_150m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x5F;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  150~200m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate150_200m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x3F;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  运行指示开 
 *	@param	无
 *	@return	无
 */
void Run_LED_Open(void)
{
	HC164_DATA=HC164_DATA & (~0x0100);		//开运行指示LED
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  运行指示关 
 *	@param	无
 *	@return	无
 */
void Run_LED_Close(void)
{
	HC164_DATA=HC164_DATA & (0x0100);		//关运行指示LED
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  开风机
 *	@param	无
 *	@return	无
 */
void OpenFan(void)
{
	HC164_DATA=HC164_DATA | (0x0400);       //开启风机
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  关风机
 *	@param	无
 *	@return	无
 */
void CloseFan(void)
{
	HC164_DATA=HC164_DATA & (~0x0400);		//先关闭风机
	SendDataTo164(HC164_DATA);
}


/**
 *	@brief  开加热
 *	@param	无
 *	@return	无
 */
void OpenHeat(void)
{
	HC164_DATA=HC164_DATA | (0x0200);       //开加热
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  关加热
 *	@param	无
 *	@return	无
 */
void CloseHeat(void)
{
	HC164_DATA=HC164_DATA & (~0x0200);		//关加热
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  开option灯
 *	@param	无
 *	@return	无
 */
void OpenOptionLED(void)
{
	HC164_DATA=HC164_DATA & ~(0x0001<<7);
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  关加热
 *	@param	无
 *	@return	无
 */
void CloseOptionLED(void)
{
	HC164_DATA=HC164_DATA | (0x0001<<7);
	SendDataTo164(HC164_DATA);
}
/*******************************************************************************************************/
/*******************************************************************************************************/

/*****************************************************************************************************/
static uint32_t pumpcnt=0;
static uint32_t PumpTargetCnt=0;  
static uint8_t  PumpRunFlag=0;

/**
 *	@brief  PUMP控制管脚初始化函数
 *	@param	无
 *	@return	无
 */
void PUMP_Control_GPIO_Init(void)
{
	hal_gpio_pin_init(PUMP_CONTROL,OEN);
	hal_gpioretention_register(PUMP_CONTROL);//enable this pin retention
	hal_gpio_fast_write(PUMP_CONTROL,0);
}

/**
  * @brief  设定泵电机转动的圈数         
  * @param  LoopCnt:要求泵转的圈数
  * @retval 无
  */
void SetPumpLoop(uint32_t LoopCnt)
{
  if (PumpRunFlag == 0)
  { 	//电机处于非运行状态                                                            
    PumpTargetCnt = LoopCnt + pumpcnt; //后一??参数??先前已经泵的次数(??写进了EEPROM??)
    PumpRunFlag = 1;                                             //该标志位会在电机??完指定的圈数后清??
	hal_gpio_fast_write(PUMP_CONTROL,1);  //开泵
  }
}

/**
  * @brief  根据泵电机的转动次数,来动态的改变当前计数值
  * @param  无
  * @retval 无
  */
static void IncPumpCntNum(void)
{
	pumpcnt++; //不需要排气时这个东东才会加加
	if (pumpcnt >= PumpTargetCnt)
	{                       //泵电机,转动了指定圈数后,电机停止   PumpBottleTargetCnt;这个数得动态的增加
		hal_gpio_fast_write(PUMP_CONTROL,0);  //关泵
		PumpRunFlag = 0;
	}
}

/**
 * @brief 控制线程初始化
 * @param task_id 线程的id号
 * @retval 无
 */
void Control_ProcessInit(uint8 task_id)
{
	Control_TaskID = task_id;
	LOG("Control task start...\n");
	PUMP_Control_GPIO_Init();
	InitHC164();
}


/**
 * @brief 控制线程
 * @param task_id 线程的id号
 * @param events 任务的事件参数
 * @retval 我也不知道返回啥??
 */
uint16 Control_ProcessEvent( uint8 task_id, uint16 events )
{
	if(task_id != Control_TaskID)
	{
		return 0;
	}
		
	if( events & CONTROL_CYCLE_TIMER)
	{

		return (events ^ CONTROL_CYCLE_TIMER);
	}
    return 0;
}

/*******************************************************************************************************/

/*****************************************************************************************************/
#define KEY_NUM 		2
#define KEY_DOWN_TIME_THRESHOLD		4000

//定义按键当前状态数组变量
static uint8_t key_state[KEY_NUM] = {0, 0};
//记录当前按键按下的时间(单位ms)
static uint32_t key_down_time[KEY_NUM - 1] = {0}; //这样写对吗????
uint8_t LongKeyFirstFlag=0;			//默认是不处于长按状态的

/**
 *	@brief  KEY管脚(包括泵计数管脚)初始化函数
 *	@param	无
 *	@return	无
 */
void KEY_GPIO_Init(void)
{
	hal_gpio_pin_init(KEY_GPIO,IE);   //将他们配置为输入状态
	hal_gpio_pin_init(PUMP_CNT_GPIO,IE);
}


void ControlDemo(void)
{
	static uint8_t cnt=1;

	switch (cnt)
	{
	case 1:Indicate5_15m2();cnt++;break;
	case 2:Indicate15_30m2();cnt++;break;
	case 3:Indicate30_50m2();cnt++;break;
	case 4:Indicate50_80m2();cnt++;break;
	case 5:Indicate80_100m2();cnt++;break;
	case 6:Indicate100_150m2();cnt++;break;
	case 7:Indicate150_200m2();cnt=1;break;
	default:
		break;
	}
}

/**
 *	@brief  按键动作函数,根据输入的键码,执行相应的动作
 *	@param	keycode :键码
 *	@return	无
 */
void KeyAction(uint8_t keycode)
{
	switch (keycode)
	{
	case 0:
		if (LongKeyFirstFlag == 0)
		{   //当处于短按状态时才会执行短按
			LOG("KEY_CODE:0\n");
			ControlDemo();
			beep_type=1;  //蜂鸣器响1下
			osal_start_timerEx(buzzer_TaskID, BUZZER_START_TIMER,10);
		}
		LongKeyFirstFlag = 0;      //按键弹起时会执行这个动作
		break;
	case 1:
		LOG("KEY_CODE:1\n");
		//这里执行泵计数函数
		IncPumpCntNum();
		break;
	case 2:
		if (LongKeyFirstFlag == 0)
		{ //程序进行到这里表明,是长按电容按键
			LongKeyFirstFlag=1;   //置位为1,表明当前处于长按状态
			//这里执行长按函数
			LOG("KEY_CODE:2\n");
			SetPumpLoop(5);
			beep_type=3;  //蜂鸣器响3下
			osal_start_timerEx(buzzer_TaskID, BUZZER_START_TIMER,10);
		}
		break;
	default:
		break;
	}
}

/**
 *	@brief  按键驱动函数,需要每隔一定时间进行一次调用
 *	@param	无
 *	@return	无
 */
void Key_Driver(void)
{
	static uint8_t key_state_buff[KEY_NUM] = {0, 0};   //默认初始电平是低电平
	uint8_t i;

	for (i = 0; i < KEY_NUM; ++i)
	{	//这里是短按
		if (key_state_buff[i] != key_state[i]) //按键被按下或弹起(只执行变化时的值)
		{
			//备份按键的状态
			key_state_buff[i] = key_state[i];
			if (key_state[i] == 0) //按键弹起
			{
				//调用KeyAction函数
				KeyAction(i);
			}
		}
	}

	for (i = 0; i < (KEY_NUM - 1); ++i)
	{   //这里是长按
		if (key_down_time[i] > 0) //很关键
		{
			if (key_down_time[i] > KEY_DOWN_TIME_THRESHOLD) //按键按下时间超过阈值时间
			{
				key_down_time[i] = 0;
				//调用KeyAction函数
				KeyAction(KEY_NUM + i);
			}
		}
	}
}

/**
 * @brief  按键扫描函数(通过软件滤波来更新当前的按键状态  4ms调用一次)
 * @param  无
 * @retval 无
 */
void KEY_Scan(void)
{
	static uint8_t key_buffer[KEY_NUM] = {0x00, 0x00}; //软件滤波用数组
	uint8_t i = 0;

	key_buffer[0] = (key_buffer[0] << 1) | ((uint8_t)hal_gpio_read(KEY_GPIO));	     //采样按键的状态
	key_buffer[1] = (key_buffer[1] << 1) | ((uint8_t)hal_gpio_read(PUMP_CNT_GPIO));	 //采样泵计数按键的状态
	for (i = 0; i < KEY_NUM; ++i)
	{	//要有价值,value
		if ((key_buffer[i] & 0x0F) == 0x00)
		{	//没有被按下
			key_state[i] = 0;
			if (i==0)
			{ //长按只针对,电容按键的
				key_down_time[i] = 0;
			}
		}
		else if ((key_buffer[i] & 0x0F) == 0x0F)
		{   //按下去了    每4ms调用一次,连续4次,就是16ms
			key_state[i] = 1;
			if (i == 0)
			{ //长按只针对,电容按键的
				if (LongKeyFirstFlag == 0)
				{   //当前不处于长按状态
					key_down_time[i] += 4;
				}
				else
				{  //当前处于长按状态,按键被长按后,时间就不允许再加了,并将时间归零
					key_down_time[i] = 0;
				}
			}
		}
	}
}



/**
 * @brief 按键线程初始化
 * @param task_id 线程的id号
 * @retval 无
 */
void Key_ProcessInit(uint8 task_id)
{
	key_TaskID = task_id;
	LOG("KEY task start...\n");
	KEY_GPIO_Init();
	osal_start_reload_timer(key_TaskID, KEY_CYCLE_TIMER , 4);   
}


/**
 * @brief 按键扫描线程
 * @param task_id 线程的id号
 * @param events 任务的事件参数
 * @retval 我也不知道返回啥??
 */
uint16 Key_ProcessEvent( uint8 task_id, uint16 events )
{
	if(task_id != key_TaskID)
	{
		return 0;
	}
		
	if( events & KEY_CYCLE_TIMER)
	{
		KEY_Scan();		
		Key_Driver();
		return (events ^ KEY_CYCLE_TIMER);
	}
    return 0;
}

/*******************************************************************************************************/
/*******************************************************************************************************/

/**
 *	@brief		初始化蜂鸣器使用的PWM外设 
 *	@param	    none.
 *	@return		none.
 */
void  Buzzer_Init(void)
{
	hal_gpio_pin2pin3_control(P3, Bit_ENABLE);
	hal_gpio_fmux(P3, Bit_DISABLE);
	hal_gpio_pin_init(P3, OEN);
	hal_gpio_write(P3, 0);
	
	hal_pwm_init(PWM_CH2, PWM_CLK_DIV_16, PWM_CNT_UP, PWM_POLARITY_FALLING);
	hal_pwm_open_channel(PWM_CH2,P3);
	hal_pwm_set_count_val(PWM_CH2, 0, 370);
	hal_pwm_start();
}

/**
 * @brief 蜂鸣器线程初始化
 * @param task_id 线程的id号
 * @retval 无
 */
void Buzzer_ProcessInit( uint8 task_id)
{
	buzzer_TaskID = task_id;
	LOG("Buzzer task start...\n");
	Buzzer_Init();
}

/**
 * @brief 蜂鸣器线程
 * @param task_id 线程的id号
 * @param events 任务的事件参数
 * @retval 我也不知道返回啥??
 */
uint16 Buzzer_ProcessEvent( uint8 task_id, uint16 events)
{
	static uint8_t cnt=0;

	if(task_id != buzzer_TaskID)
	{
		return 0;
	}

	if( events & BUZZER_START_TIMER)
	{
		if(cnt==0)
		{
			if(beep_type==1)
			{
				LOG("Buzzer once !\n");
				cnt=2;		
				osal_start_timerEx(buzzer_TaskID, BUZZER_1TIMES_TIMER,10);
			}
			else if(beep_type==3)
			{
				LOG("Buzzer three times !\n");
				cnt=6;
				osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,10);
			}
		}
		return (events ^ BUZZER_START_TIMER);
	}
	if( events & BUZZER_1TIMES_TIMER)
	{
		LOG("cnt:%d\n",cnt);
		switch (cnt)
		{
			case 2: hal_pwm_set_count_val(PWM_CH2, 185, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_1TIMES_TIMER,100);break;
			case 1: hal_pwm_set_count_val(PWM_CH2, 0, 370);cnt--;break;
			default: break;
		}

		return (events ^ BUZZER_1TIMES_TIMER);
	}
	if( events & BUZZER_3TIMES_TIMER)
	{
		LOG("_____cnt:%d\n",cnt);
		switch (cnt)
		{
			case 6: hal_pwm_set_count_val(PWM_CH2, 185, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,60);LOG("Buzzer........!\n");break;
			case 5: hal_pwm_set_count_val(PWM_CH2, 0, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,15);LOG("Buzzer........!\n");break;
			case 4: hal_pwm_set_count_val(PWM_CH2, 185, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,60);LOG("Buzzer........!\n");break;
			case 3: hal_pwm_set_count_val(PWM_CH2, 0, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,15);LOG("Buzzer........!\n");break;
			case 2: hal_pwm_set_count_val(PWM_CH2, 185, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,60);LOG("Buzzer........!\n");break;
			case 1: hal_pwm_set_count_val(PWM_CH2, 0, 370);cnt--;LOG("Buzzer........!\n");break;
			default: break;
		}
		return (events ^ BUZZER_3TIMES_TIMER);
	}
    return 0;
}

/*******************************************************************************************************/