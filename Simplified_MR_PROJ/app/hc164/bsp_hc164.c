#include "board.h"

static uint16_t HC164_DATA=0x03FF;   //存储HC164待发送数据用的变量



/**
 *	@brief  HC164串转并驱动函数 
 *	@param	udata :待发送的数据
 *	@return	无
 */
static void SendDataTo164(uint16_t udata)
{
	static uint16_t data_backup=0xFFFF;
	uint16_t i;

	if(data_backup != udata)
	{
		data_backup = udata;
		
		for (i = 0; i < 13; i++)
		{	//因为这里有13个数据需要发送,故循环13次
			if (udata & 0x1000)   //1 0000 0000 0000
			{
				hal_gpio_fast_write(HC164_SDA,1);
				hal_gpio_fast_write(HC164_SCL,0);
				hal_gpio_fast_write(HC164_SCL,1);
			}
			else
			{
				hal_gpio_fast_write(HC164_SDA,0);
				hal_gpio_fast_write(HC164_SCL,0);
				hal_gpio_fast_write(HC164_SCL,1);
			}
			udata = udata << 1;
		}
	}
}

/**
 *	@brief  HC164串转并驱动函数 
 *	@param	udata :待发送的数据
 *	@return	无
 */
void InitHC164(void)
{
	hal_gpio_pin_init(HC164_SDA,OEN);
	hal_gpio_pin_init(HC164_SCL,OEN);
	hal_gpioretention_register(HC164_SDA);//enable this pin retention
	hal_gpioretention_register(HC164_SCL);//enable this pin retention
	hal_gpio_fast_write(HC164_SDA,1);
	hal_gpio_fast_write(HC164_SCL,1);

	SendDataTo164(0x03FF);       //上电所有和HC164有关的东东全部关机
}


/**
 *	@brief  5~15m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate5_15m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x7E;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  15~30m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate15_30m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x7D;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  30~50m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate30_50m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x7B;     //
	SendDataTo164(HC164_DATA);
}


/**
 *	@brief  50~80m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate50_80m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x77;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  80~100m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate80_100m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x6F;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  100~150m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate100_150m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x5F;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  150~200m2指示 
 *	@param	无
 *	@return	无
 */
void Indicate150_200m2(void)
{
	HC164_DATA=HC164_DATA & 0xFF80;   //
	HC164_DATA=HC164_DATA | 0x3F;     //
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  灭蚊运行指示开 
 *	@param	无
 *	@return	无
 */
void Fumigate_Run_LED_Open(void)
{
	HC164_DATA=HC164_DATA & (~0x0100);		//开运行指示LED
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  灭蚊运行指示关 
 *	@param	无
 *	@return	无
 */
void Fumigate_Run_LED_Close(void)
{
	HC164_DATA=HC164_DATA | (0x0100);		//关运行指示LED
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  驱蚊运行指示开 
 *	@param	无
 *	@return	无
 */
void Repel_Run_LED_Open(void)
{
	HC164_DATA=HC164_DATA & (~0x0200);		//开运行指示LED
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  驱蚊运行指示关 
 *	@param	无
 *	@return	无
 */
void Repel_Run_LED_Close(void)
{
	HC164_DATA=HC164_DATA | (0x0200);		//关运行指示LED
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  开风机
 *	@param	无
 *	@return	无
 */
void OpenFan(void)
{
	HC164_DATA=HC164_DATA | (0x1000);       //开启风机
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  关风机
 *	@param	无
 *	@return	无
 */
void CloseFan(void)
{
	HC164_DATA=HC164_DATA & (~0x1000);		//先关闭风机
	SendDataTo164(HC164_DATA);
}


/**
 *	@brief  开驱蚊加热
 *	@param	无
 *	@return	无
 */
void OpenRepelHeat(void)
{
	HC164_DATA=HC164_DATA | (0x0400);       //开加热
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  关驱蚊加热
 *	@param	无
 *	@return	无
 */
void CloseRepelHeat(void)
{
	HC164_DATA=HC164_DATA & (~0x0400);		//关加热
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  开灭蚊加热
 *	@param	无
 *	@return	无
 */
void OpenFumigateHeat(void)
{
	HC164_DATA=HC164_DATA | (0x0800);       //开加热
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  关灭蚊加热
 *	@param	无
 *	@return	无
 */
void CloseFumigateHeat(void)
{
	HC164_DATA=HC164_DATA & (~0x0800);		//关加热
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  开option灯
 *	@param	无
 *	@return	无
 */
void OpenOptionLED(void)
{
	LOG("open\n");
	HC164_DATA=HC164_DATA & ~(0x0080);
	SendDataTo164(HC164_DATA);
}

/**
 *	@brief  关加热
 *	@param	无
 *	@return	无
 */
void CloseOptionLED(void)
{
		LOG("close\n");
	HC164_DATA=HC164_DATA | (0x0080);
	SendDataTo164(HC164_DATA);
}