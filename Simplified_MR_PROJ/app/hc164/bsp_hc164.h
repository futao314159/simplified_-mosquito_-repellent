#ifndef     __BSP_HC164__
#define     __BSP_HC164__

#include "types.h"

//这里是HC164的数据管脚
#define HC164_SDA		P34		//HC164的数据脚
#define HC164_SCL		P2      //HC164的时钟脚

void InitHC164(void);
void Indicate5_15m2(void);
void Indicate15_30m2(void);
void Indicate30_50m2(void);
void Indicate50_80m2(void);
void Indicate80_100m2(void);
void Indicate100_150m2(void);
void Indicate150_200m2(void);

void Fumigate_Run_LED_Open(void);
void Fumigate_Run_LED_Close(void);
void Repel_Run_LED_Open(void);
void Repel_Run_LED_Close(void);

void OpenOptionLED(void);
void CloseOptionLED(void);

void OpenFan(void);
void CloseFan(void);

void OpenRepelHeat(void);
void CloseRepelHeat(void);
void OpenFumigateHeat(void);
void CloseFumigateHeat(void);

void OpenOptionLED(void);
void CloseOptionLED(void);

#endif