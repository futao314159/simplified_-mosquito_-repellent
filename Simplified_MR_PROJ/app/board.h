#ifndef __BOARD_H__
#define __BOARD_H__

//这里保存的是整个工程需要的头文件
#include "ll.h"
#include "bus_dev.h"
#include "mcu.h"
#include "rf_phy_driver.h"
#include "flash.h"
#include "fs.h" 
#include "version.h"

#include "OSAL.h"
#include "OSAL_Tasks.h"
#include "OSAL_PwrMgr.h"
#include "osal_snv.h"
#include "ll_sleep.h"
#include "comdef.h"
#include "OSAL_Timers.h"
#include "jump_function.h"
#include "global_config.h"

#include "devinfoservice.h"
#include "hci_tl.h"
#include "l2cap.h"
#include "gap.h"
#include "gapgattserver.h"
#include "peripheral.h"

#include "bcomdef.h"
#include "linkdb.h"
#include "att.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "gapbondmgr.h"
#include "hci.h"

#include "types.h"
#include "uart.h"
#include "log.h"
#include "pwm.h"
#include "gpio.h"
#include "clock.h"
#include "pwrmgr.h"
#include "error.h"
#include "adc.h"
#include "timer.h"
#include "rom_sym_def.h"
#include "watchdog.h"

#include "math.h"
#include <string.h>
#include <stdio.h>
/************************app************************/

#include "bsp_ble_protocol.h"
#include "bsp_ble.h"
#include "bsp_ble_service.h"
#include "bsp_hc164.h"
#include "bsp_control.h"
#include "bsp_buzzer.h"
#include "bsp_sensor.h"
#include "bsp_key.h"
#include "bsp_config.h"

/**************************************************************/
extern pwrmgr_attribute_t pwrmgr_attribute;
extern uint32 ll_remain_time;
/**************************************************************/


#endif
