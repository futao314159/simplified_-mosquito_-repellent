#ifndef __BSP_CONFIG__
#define __BSP_CONFIG__

#include "types.h"

/******************************************************************************/

#define TIMES_OF_MEDICINE 400 //先假定可以泵500次
#define HOURS_OF_REPEL 300    //驱蚊药剂假定可以工作300小时

#define MARK 0x55 //标志位

#define TARGET_TEMP 190

#define PUMP_COUNTER 0
#define MEDICINE_TYPE NORMAL_MEDICINE
#define EMPTY_OR_NOT NOT_EMPTY
#define EVACUATE_OR_NOT NOT_NEED_EVACU

#define SOUNG_MODE BUZZER_ON //默认蜂鸣器是开启的

#define WORKING_MODE ENUM_FUMIGATE_NORMAL_MODE //默认工作模式为正常灭蚊
#define FUMIGATE_WORKING_MODE_BUF                                              \
  ENUM_FUMIGATE_NORMAL_MODE //默认工作模式为正常灭蚊
#define REPEL_WORKING_WORKING_MODE_BUF                                         \
  ENUM_REPEL_ENCHANCE_MODE //默认工作模式为增强驱蚊

#define WORKING_STATUS ENUM_WORKING_STOP //默认工作状态为停止

#define FUMI_HOUSE_HEIGHT 3.0    //默认的灭蚊房高
#define FUMI_AREA ENUM_5_15_M2_F //默认的灭蚊面积

#define REPEL_AREA ENUM_5_15_M2_R

/******************************************************************************/

/**上面这些都是默认值*/

/************下面还得将这些地址规划一下**************/
#define MARK_ID 0x80
#define WORKING_MODE_ID 0x81
#define WORKING_STATUS_ID 0x82
#define FUMIGATE_SETT_ID 0x83
#define REPEL_SETT_ID 0x84
#define FACTORY_SETT_ID 0x85
#define SOUND_ID 0x86
#define PUMP_ID 0x87
#define PUMPCNT_ID 0x88
/*************************************************/
#define MARK_LENGTH 1
#define WORKING_MODE_LENGTH 3
#define WORKING_STATUS_LENGTH 1
#define FUMIGATE_SETT_LENGTH 5
#define REPEL_SETT_LENGTH 5
#define FACTORY_SETT_LENGTH 32
#define SOUND_LENGTH 1
#define PUMP_LENGTH 3
#define PUMPCNT_LENGTH 4
/*************************************************/

typedef union {
  uint8_t dat_i[4];
  float dat_f;
} FLOAT_UINT8_T;

//下面的地址安排的很紧凑
//地址是不包含地址成员的
/**************************************************************************************/
typedef struct {
  uint8_t mark;
  uint8_t mark_id;
} MARK_Typedef;
/**************************************************************************************/
/**************************************************************************************/
enum mode {
  ENUM_FUMIGATE_NORMAL_MODE,
  ENUM_FUMIGATE_ENCHANCE_MODE,
  ENUM_REPEL_AIR_CONDI_MODE,
  ENUM_REPEL_MATERNAL_MODE,
  ENUM_REPEL_ENCHANCE_MODE,
  ENUM_REPEL_AROMA_MODE,
  MODE_ALL,
};

typedef struct {
  enum mode working_mode;
  enum mode fumigate_buf_mode; //存储熏蒸时的模式
  enum mode repel_buf_mode;    //存储驱蚊时的模式
  uint8_t working_mode_id;
} WORKING_MODE_Typedef;
/**************************************************************************************/
/**************************************************************************************/
enum status {
  ENUM_WORKING_STOP,  //工作结束
  ENUM_WORKING_READY, //工作就绪
  ENUM_WORKING_WAIT,  //等待工作
  ENUM_WORKING_START, //工作开始
  ENUM_FAN_WORKING,   //风机开始工作
  ENUM_STATUS_ALL,
}; //这里是针对熏蒸的

typedef struct {
  enum status working_status;
  uint8_t working_status_id;
} WORKING_STATUS_Typedef;
/**************************************************************************************/
/***********************************************************************************************************************/

/***********************************************************************************************************************/
/**************************************************************************************/
enum area_e {
  ENUM_5_15_M2_R,
  ENUM_15_30_M2_R,
  ENUM_30_50_M2_R,
  ENUM_AREA_ALL_R,
};
/**************************************************************************************/
/**************************************************************************************/
typedef struct {
  uint32_t total_repel_time;   //  4byte
  enum area_e repel_sett_area; //  1byte
  uint8_t repel_sett_id;
} REPELLENT_SETTING_Typedef; // 5byte
/***********************************************************************************************************************/

/***********************************************************************************************************************/
/**************************************************************************************/
enum area_f {
  ENUM_5_15_M2_F,
  ENUM_15_30_M2_F,
  ENUM_30_50_M2_F,
  ENUM_50_80_M2_F,
  ENUM_80_100_M2_F,
  ENUM_100_150_M2_F,
  ENUM_150_200_M2_F,
  ENUM_AREA_ALL_F,
};

typedef struct {
  FLOAT_UINT8_T fumi_house_height; //针对灭蚊房高      4byte
  enum area_f fumi_sett_area;      //灭蚊面积设置      1byte
  uint8_t fumi_sett_id;
} FUMIGATE_SETTING_Typedef;
/**************************************************************************************/
/**************************************************************************************/
typedef struct //出厂设置(调试时用到的一些参数)
{
  uint16_t evacuation_times;    // 2byte
  uint16_t fumigate_time;       // 2byte
  FLOAT_UINT8_T timers_per10m2; // 4byte
  uint16_t target_temp;        // 2byte   //这里温控就是使用开关量的
  uint16_t target_temp_backup; // 2byte   //  12

  uint16_t air_condi_open_time;  // 2byte
  uint16_t air_condi_close_time; // 2byte
  uint16_t maternal_open_time;   // 2byte
  uint16_t maternal_close_time;  // 2byte
  uint16_t aroma_open_time;      // 2byte
  uint16_t aroma_close_time;     // 2byte
  uint16_t area5_15m2_factory;   // 2byte
  uint16_t area15_30m2_factory;  // 2byte
  uint16_t area30_50m2_factory;  // 2byte
  uint16_t InitRepelTime;        // 2byte   //20

  uint8_t Factory_Sett_id;
} FACTORY_SETT_Typedef; // 32byte

/****************************************************************************************/

/**************************************************************************************************************************/
/**************************************************************************************/
enum sound {
  BUZZER_ON,
  BUZZER_OFF,
};

typedef struct {
  enum sound sound_status; // 1byte
  uint8_t sound_id;
} SOUND_Typedef;
/**************************************************************************************/

/**************************************************************************************/
enum medicine_type {
  NORMAL_MEDICINE,
  MEDIAN_MEDICINE,
  ENCHANCE_MEDICINE,
  ALL_MEDICINE,
};

enum is_empty_ {
  IS_EMPTY,
  NOT_EMPTY,
  ALL_EMPTY,
};

enum need_evacua {
  NEED_EVACU,
  NOT_NEED_EVACU,
};

typedef struct {
  enum need_evacua need_evacuation; // 1byte
  enum is_empty_ is_empty;          // 1byte
  enum medicine_type type;          // 1byte
  uint8_t pump_id;
} PUMP_Typedef; // 3byte
/**************************************************************************************/
/********************************************22******************************************/
typedef struct //因为这个频繁存储,故得单独列出来
{
  volatile uint32_t pumpcnt; // 4byte
  uint16_t pumpcnt_id;
} PUMPCNT_Typedef; // 4byte
/**************************************************************************************/

/*保存参数结构体*/
typedef struct {
  MARK_Typedef Mark; //标记,表明是否需要将数据初始化
  WORKING_MODE_Typedef WorkingMode;         //工作模式
  WORKING_STATUS_Typedef WorkingStatus;     //工作状态
  FUMIGATE_SETTING_Typedef FumigateSetting; //熏蒸设置
  REPELLENT_SETTING_Typedef RepelSetting;
  FACTORY_SETT_Typedef FactorySetting;
  SOUND_Typedef SoundMode;
  PUMP_Typedef Pump;
  PUMPCNT_Typedef PumpCNT;
} ConfigParam_Typedef;

extern ConfigParam_Typedef ConfigParam;

void ConfigParamInit(void);

//腰疼还好,如果腰疼都感觉不到,那就不对了

#endif