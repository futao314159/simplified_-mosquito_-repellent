#include "board.h"


//要学会享受,但同样要学会吃苦！

ConfigParam_Typedef ConfigParam =  
{   //注意给结构体参数赋值时后面是,号
    .Mark.mark_id=MARK_ID,
    .WorkingMode.working_mode_id=WORKING_MODE_ID,
    .WorkingStatus.working_status_id=WORKING_STATUS_ID,
    .FumigateSetting.fumi_sett_id=FUMIGATE_SETT_ID,
    .RepelSetting.repel_sett_id=REPEL_SETT_ID,
    .FactorySetting.Factory_Sett_id=FACTORY_SETT_ID,
    .SoundMode.sound_id=SOUND_ID,
    .Pump.pump_id=PUMP_ID,
    .PumpCNT.pumpcnt_id=PUMPCNT_ID,
};

ConfigParam_Typedef ConfigParamDefault=  //默认的配置结构体
{   //注意给结构体参数赋值时后面是,号  
    .Mark.mark=MARK,
    .Mark.mark_id=MARK_ID,

    .WorkingMode.working_mode=WORKING_MODE,
    .WorkingMode.fumigate_buf_mode=FUMIGATE_WORKING_MODE_BUF,
    .WorkingMode.repel_buf_mode=REPEL_WORKING_WORKING_MODE_BUF,
    .WorkingMode.working_mode_id=WORKING_MODE_ID,

    .WorkingStatus.working_status=WORKING_STATUS,
    .WorkingStatus.working_status_id=WORKING_STATUS_ID,

    .FumigateSetting.fumi_house_height.dat_f=FUMI_HOUSE_HEIGHT,
    .FumigateSetting.fumi_sett_area=FUMI_AREA,
    .FumigateSetting.fumi_sett_id=FUMIGATE_SETT_ID,

    .RepelSetting.repel_sett_area=REPEL_AREA,
    .RepelSetting.total_repel_time=HOURS_OF_REPEL*3600L,
    .RepelSetting.repel_sett_id=REPEL_SETT_ID,

    .FactorySetting.evacuation_times=20,     //默认每次换完瓶子,再次开启熏蒸时会泵20下用来排空气   
    .FactorySetting.fumigate_time=35,        //这里是以分钟来记的  这个我得把它动态计算出来
    .FactorySetting.timers_per10m2.dat_f=0.7,
    .FactorySetting.target_temp=TARGET_TEMP,
    .FactorySetting.target_temp_backup=TARGET_TEMP,

    .FactorySetting.air_condi_close_time=40,       //这里都是按照分钟来定的
    .FactorySetting.air_condi_open_time=30,
    .FactorySetting.maternal_close_time=30,
    .FactorySetting.maternal_open_time=30,
    .FactorySetting.aroma_close_time=20,
    .FactorySetting.aroma_open_time=60,
    .FactorySetting.area5_15m2_factory=60,
    .FactorySetting.area15_30m2_factory=70,
    .FactorySetting.area30_50m2_factory=100,
    .FactorySetting.InitRepelTime=30,        //初始熏蒸时间
    .FactorySetting.Factory_Sett_id=FACTORY_SETT_ID,


    .SoundMode.sound_status=SOUNG_MODE,
    .SoundMode.sound_id=SOUND_ID,

    .Pump.is_empty=EMPTY_OR_NOT,  //默认是非空的
    .Pump.type=MEDICINE_TYPE,    //默认是常规药剂
    .Pump.need_evacuation=EVACUATE_OR_NOT, //默认是需要排空气的
    .Pump.pump_id=PUMP_ID,

    .PumpCNT.pumpcnt=PUMP_COUNTER,
    .PumpCNT.pumpcnt_id=PUMPCNT_ID,
};


/**
  * @brief  读取FS中的数据,每次上电之后进行
  * @param  无
  * @retval 无
  */
static void ParamRead(void)
{
    // ConfigParam=ConfigParamDefault;   //主要是将地址copy过去   这里没有必要
    osal_snv_read(ConfigParam.Mark.mark_id,MARK_LENGTH,&(ConfigParam.Mark.mark));	
    osal_snv_read(ConfigParam.WorkingMode.working_mode_id,WORKING_MODE_LENGTH,&(ConfigParam.WorkingMode.working_mode));			
    osal_snv_read(ConfigParam.WorkingStatus.working_status_id,WORKING_STATUS_LENGTH,&(ConfigParam.WorkingStatus.working_status));			
    osal_snv_read(ConfigParam.FumigateSetting.fumi_sett_id,FUMIGATE_SETT_LENGTH,&(ConfigParam.FumigateSetting.fumi_house_height.dat_f));
    osal_snv_read(ConfigParam.RepelSetting.repel_sett_id,REPEL_SETT_LENGTH,&(ConfigParam.RepelSetting.total_repel_time));			
    osal_snv_read(ConfigParam.FactorySetting.Factory_Sett_id,FACTORY_SETT_LENGTH,&(ConfigParam.FactorySetting.evacuation_times));			
    osal_snv_read(ConfigParam.SoundMode.sound_id,SOUND_LENGTH,&(ConfigParam.SoundMode.sound_status));	
    osal_snv_read(ConfigParam.Pump.pump_id,PUMP_LENGTH,&(ConfigParam.Pump.need_evacuation));	
    osal_snv_read(ConfigParam.PumpCNT.pumpcnt_id,PUMPCNT_LENGTH,(void *)&(ConfigParam.PumpCNT.pumpcnt));					
}


/**
  * @brief  将默认的参数,写入到FS中
  * @param  无
  * @retval 无
  */
static void WriteDefaultParam(void)
{
    osal_snv_write(ConfigParamDefault.Mark.mark_id,MARK_LENGTH,&(ConfigParamDefault.Mark.mark));	
    osal_snv_write(ConfigParamDefault.WorkingMode.working_mode_id,WORKING_MODE_LENGTH,&(ConfigParamDefault.WorkingMode.working_mode));			
    osal_snv_write(ConfigParamDefault.WorkingStatus.working_status_id,WORKING_STATUS_LENGTH,&(ConfigParamDefault.WorkingStatus.working_status));			
    osal_snv_write(ConfigParamDefault.FumigateSetting.fumi_sett_id,FUMIGATE_SETT_LENGTH,&(ConfigParamDefault.FumigateSetting.fumi_house_height.dat_f));	
    osal_snv_write(ConfigParamDefault.RepelSetting.repel_sett_id,REPEL_SETT_LENGTH,&(ConfigParamDefault.RepelSetting.total_repel_time));		
    osal_snv_write(ConfigParamDefault.FactorySetting.Factory_Sett_id,FACTORY_SETT_LENGTH,&(ConfigParamDefault.FactorySetting.evacuation_times));			
    osal_snv_write(ConfigParamDefault.SoundMode.sound_id,SOUND_LENGTH,&(ConfigParamDefault.SoundMode.sound_status));	
    osal_snv_write(ConfigParamDefault.Pump.pump_id,PUMP_LENGTH,&(ConfigParamDefault.Pump.need_evacuation));	
    osal_snv_write(ConfigParamDefault.PumpCNT.pumpcnt_id,PUMPCNT_LENGTH,(void *)&(ConfigParamDefault.PumpCNT.pumpcnt));					
}
		

/**
  * @brief  配置参数初始化(一般在上电时进行)
  * @param  无
  * @retval 无
  */
void PrintSysParam(void)
{
    LOG("MARK:%02X\n",ConfigParam.Mark.mark);
    switch (ConfigParam.WorkingMode.working_mode)
    {
      case ENUM_FUMIGATE_NORMAL_MODE:
          LOG("ENUM_FUMIGATE_NORMAL_MODE\n");
        break;
      case ENUM_FUMIGATE_ENCHANCE_MODE:
          LOG("ENUM_FUMIGATE_ENCHANCE_MODE\n");
        break;
      case ENUM_REPEL_AIR_CONDI_MODE:
          LOG("ENUM_REPEL_AIR_CONDI_MODE\n");
        break;
      case ENUM_REPEL_MATERNAL_MODE:
          LOG("ENUM_REPEL_MATERNAL_MODE\n");
        break;
      case ENUM_REPEL_ENCHANCE_MODE:
          LOG("ENUM_REPEL_ENCHANCE_MODE\n");
        break;
      case ENUM_REPEL_AROMA_MODE:
          LOG("ENUM_REPEL_AROMA_MODE\n");
        break;
      default:
        break;
    }
    switch (ConfigParam.WorkingStatus.working_status)
    {
      case ENUM_WORKING_STOP:
          LOG("ENUM_WORKING_STOP\n");
        break;
      case ENUM_WORKING_READY:
          LOG("ENUM_WORKING_READY\n");
        break;
      case ENUM_WORKING_WAIT:
          LOG("ENUM_WORKING_WAIT\n");
        break;
      case ENUM_WORKING_START:
          LOG("ENUM_WORKING_START\n");
        break;
      case ENUM_FAN_WORKING:
          LOG("ENUM_FAN_WORKING\n");
        break;
      default:
        break;
    }
    LOG("fumi_house_height:%d\n",(uint16_t)(ConfigParam.FumigateSetting.fumi_house_height.dat_f*100));  //
    switch (ConfigParam.FumigateSetting.fumi_sett_area)
    {
      case ENUM_5_15_M2_F:
          LOG("ENUM_5_15_M2_F\n");
        break;
      case ENUM_15_30_M2_F:
          LOG("ENUM_15_30_M2_F\n");
        break;
      case ENUM_30_50_M2_F:
          LOG("ENUM_30_50_M2_F\n");
        break;
      case ENUM_50_80_M2_F:
          LOG("ENUM_50_80_M2_F\n");
        break;
      case ENUM_80_100_M2_F:
          LOG("ENUM_80_100_M2_F\n");
        break;
      case ENUM_100_150_M2_F:
          LOG("ENUM_100_150_M2_F\n");
        break;
      case ENUM_150_200_M2_F:
          LOG("ENUM_150_200_M2_F\n");
        break;
      default:
        break;
    }
    switch (ConfigParam.RepelSetting.repel_sett_area)
    {
      case ENUM_5_15_M2_R:
          LOG("ENUM_5_15_M2_R\n");
        break;
      case ENUM_15_30_M2_R:
          LOG("ENUM_15_30_M2_R\n");
        break;
      case ENUM_30_50_M2_R:
          LOG("ENUM_30_50_M2_R\n");
        break;
      default:
        break;
    }
    LOG("total_repel_time:%d\n",ConfigParam.RepelSetting.total_repel_time);

    LOG("evacuation_times:%d\n",ConfigParam.FactorySetting.evacuation_times);
    LOG("fumigate_time:%d\n",ConfigParam.FactorySetting.fumigate_time);
    LOG("timers_per10m2:%d\n",(uint16_t)(ConfigParam.FactorySetting.timers_per10m2.dat_f*100));
    LOG("target_temp:%d\n",ConfigParam.FactorySetting.target_temp);
    LOG("target_temp_backup:%d\n",ConfigParam.FactorySetting.target_temp_backup);

    LOG("air_condi_close_time:%d\n",ConfigParam.FactorySetting.air_condi_close_time);
    LOG("air_condi_open_time:%d\n",ConfigParam.FactorySetting.air_condi_open_time);
    LOG("maternal_close_time:%d\n",ConfigParam.FactorySetting.maternal_close_time);
    LOG("maternal_open_time:%d\n",ConfigParam.FactorySetting.maternal_open_time);

    LOG("aroma_close_time:%d\n",ConfigParam.FactorySetting.aroma_close_time);
    LOG("aroma_open_time:%d\n",ConfigParam.FactorySetting.aroma_open_time);

    LOG("area5_15m2_factory:%d\n",ConfigParam.FactorySetting.area5_15m2_factory);
    LOG("area15_30m2_factory:%d\n",ConfigParam.FactorySetting.area15_30m2_factory);
    LOG("area30_50m2_factory:%d\n",ConfigParam.FactorySetting.area30_50m2_factory);

    LOG("InitRepelTime:%d\n",ConfigParam.FactorySetting.InitRepelTime);

    switch (ConfigParam.Pump.is_empty)
    {
      case IS_EMPTY:
          LOG("IS_EMPTY\n");
        break;
      case NOT_EMPTY:
          LOG("NOT_EMPTY\n");
        break;
      default:
        break;
    }
    switch (ConfigParam.Pump.type)
    {
      case NORMAL_MEDICINE:
          LOG("NORMAL_MEDICINE\n");
        break;
      case MEDIAN_MEDICINE:
          LOG("MEDIAN_MEDICINE\n");
        break;
      case ENCHANCE_MEDICINE:
          LOG("ENCHANCE_MEDICINE\n");
        break;
      default:
        break;
    }
    switch (ConfigParam.Pump.need_evacuation)
    { 
      case NEED_EVACU:
          LOG("NEED_EVACU\n");
        break;
      case NOT_NEED_EVACU:
          LOG("NOT_NEED_EVACU\n");
        break;
      default:
        break;
    }
    LOG("pumpcnt:%d\n",ConfigParam.PumpCNT.pumpcnt);
}


/**
  * @brief  配置参数初始化(一般在上电时进行)
  * @param  无
  * @retval 无
  */
void ConfigParamInit(void)
{
    ParamRead();
    if(ConfigParam.Mark.mark != MARK)
    { //第一次使用,将其恢复至出厂设置
      WriteDefaultParam();
      ParamRead();   //
      PrintSysParam();
    }
}

