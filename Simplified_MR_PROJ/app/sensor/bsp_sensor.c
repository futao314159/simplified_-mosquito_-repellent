#include "board.h"

static uint8 Sensor_TaskID;

#define MAX_SAMPLE_POINT 64
static uint16_t adc_debug[6][MAX_SAMPLE_POINT];
static uint8_t channel_done_flag = 0;

uint32_t NTC_ADC_Value = 0.0;
uint32_t DESTROY_ADC_Value = 0.0;




uint8_t percentage_of_repel_medicine=0xFF;
uint8_t percentage_of_fumigate_medicine=0xFF;

/**
 * @brief  计算当前剩余的药量
 * @param  无
 * @retval 无
 */
void CalculateMedicineQuantity(void)
{
    uint16_t dat1,dat2;

    dat1=ConfigParam.RepelSetting.total_repel_time*100L/(HOURS_OF_REPEL*3600L);
    
    dat2=(TIMES_OF_MEDICINE-ConfigParam.PumpCNT.pumpcnt)*100L/TIMES_OF_MEDICINE;

    if(percentage_of_repel_medicine != dat1 || percentage_of_fumigate_medicine != dat2)
    {
        percentage_of_repel_medicine = dat1;
        percentage_of_fumigate_medicine = dat2;
		Remains_send();
    }
}


/*********************************************************************************************/

/**     3.3V
 *      | 
 *      |
 *     | |
 *     | |   NTC100K		    R(表示当前NTC的电阻值)  Ro(表示常温下NTC的标称电阻值)
 *     | |
 *      |
 *      |
 *      | __________Vin            //本例接法示意图
 *      |
 *     | |
 *     | | 
 *     | |   180R				R1
 *      |
 *      |
 *     GND
 */

#define DATA3_NUM 5
/**
 *	@brief	采集DATA3_NUM次数据，剔除最大和最小，后取平均值     
 *	@param  dat：待处理的数据
 *	@retval 处理后的数据
 */
static uint16_t DataFilter3(uint16_t dat)
{
	static uint32_t dat_buff1[DATA3_NUM]; //采集用数组
	uint32_t dat_buff2[DATA3_NUM];		  //运算用数组
	static int8_t i = 0;
	uint8_t j = 0;
	uint32_t sum = 0;

	++i;
	if (i <= DATA3_NUM)
	{ //对于前DATA3_NUM个数据只是取平均值
		dat_buff1[i - 1] = dat;
		for (j = 0; j < i; ++j)
		{
			sum += dat_buff1[j];
		}
		sum /= i;
	}
	else
	{
		//从DATA3_NUM个数据开始，就得去除最大值，最小值，然后取平均值
		i = DATA3_NUM + 1;
		for (j = 0; j < (DATA3_NUM - 1); ++j)
		{
			dat_buff1[j] = dat_buff1[j + 1]; //将数组的第一个元素空出来(元素依次往后挪动，最后一个被踢掉dat_buff1[0]被踢掉)
		}
		dat_buff1[j] = dat;
		for (j = 0; j < DATA3_NUM; ++j)
		{
			dat_buff2[j] = dat_buff1[j];
		}
		for (j = 0; j < DATA3_NUM; ++j)
		{
			if (dat_buff2[j] > dat_buff2[0]) //dat_buff[0]里面存max
			{
				sum = dat_buff2[0];
				dat_buff2[0] = dat_buff2[j];
				dat_buff2[j] = sum;
			}
			if (dat_buff2[j] < dat_buff2[1]) //dat_buff[1]里面存min
			{
				sum = dat_buff2[1];
				dat_buff2[1] = dat_buff2[j];
				dat_buff2[j] = sum;
			}
		}
		sum = (dat_buff2[2] + dat_buff2[3] + dat_buff2[4]) / 3;
	}
	return sum;
}

/**
  * @brief  获取当前NTC热敏电阻的值
  * @param  dat :原始的ADC值     这里还可以做一些文章,将精度提高
  * @retval 当前NTC电阻的值		
  */
static uint32_t get_resistence_ntc(uint32_t dat)
{
	dat = dat * 8000UL / 4096UL; //由原始的ADC值计算得出电压值
	dat = DataFilter3(dat);   //算出来的单位就是  0.1mv
	//Res of NTC (Ohm) = ((VCC * R1)/Vin) - R1
	return ((33000UL * 180UL) / dat) - 180UL;
}
/**
  * @brief  由当前的NTC电阻值计算出当前的温度值
  * @param  NTC_Res : 当前的NTC电阻值
  * @retval 当前温度值
  */
static double CalculateTemperature(double NTC_Res)
{
	double temp;

	temp = NTC_Res / 100000.0;	   // (R/Ro)
	temp = log(temp);			   // ln(R/Ro)
	temp /= 3950.0;				   // 1/B * ln(R/Ro)
	temp += 1.0 / (25.0 + 273.15); // + (1/To)
	temp = 1.0 / temp;			   // Invert
	temp -= 273.15;				   // convert to C

	return temp;
}

/**
  * @brief  获取当前的温度值
  * @param  无
  * @retval 当前温度值
  */
float GetTemperature(void)
{
	float temp;

	HAL_ENTER_CRITICAL_SECTION();
	temp = CalculateTemperature(get_resistence_ntc(NTC_ADC_Value));
	HAL_EXIT_CRITICAL_SECTION();
	
	return temp;
}

/**
 * @brief ADC线程初始化
 * @param task_id 线程的id号
 * @retval 无
 */
void Sensor_ProcessInit(uint8 task_id)
{
	Sensor_TaskID = task_id;
	LOG("Sensor task start...\n");
	osal_start_reload_timer(Sensor_TaskID, SENSOR_CYCLE_TIMER, 50); //这个东东每200ms采集一次 ,每1s温度软件滤波器就会更新一次
}

/******************************ADC 配置结构体************************************/
adc_Cfg_t adc_cfg = {
	.channel = ADC_BIT(ADC_CH2P_P14) | ADC_BIT(ADC_CH3N_P15),
	.is_continue_mode = FALSE,
	.is_differential_mode = 0x00,
	.is_high_resolution = 0x7f, //是高精度模式
};
/*******************************************************************************/


static void adc_evt(adc_Evt_t *pev)
{
	float value = 0;
	int i = 0;
	bool is_high_resolution = FALSE;
	bool is_differential_mode = FALSE;
	uint8_t ch = 0;

	if ((pev->type != HAL_ADC_EVT_DATA) || (pev->ch < 2))
		return;

	osal_memcpy(adc_debug[pev->ch - 2], pev->data, 2 * (pev->size));
	channel_done_flag |= BIT(pev->ch);

	if (channel_done_flag == adc_cfg.channel)
	{
		for (i = 2; i < 8; i++)
		{
			if (channel_done_flag & BIT(i))
			{
				is_high_resolution = (adc_cfg.is_high_resolution & BIT(i)) ? TRUE : FALSE;
				is_differential_mode = (adc_cfg.is_differential_mode & BIT(i)) ? TRUE : FALSE;
				value = hal_adc_value_cal((adc_CH_t)i, adc_debug[i - 2], pev->size, is_high_resolution, is_differential_mode);

				switch (i)
				{
				case ADC_CH1N_P11:
					ch = 11;
					break;
				case ADC_CH1P_P23:
					ch = 23;
					break;
				case ADC_CH2N_P24:
					ch = 24;
					break;
				case ADC_CH2P_P14:
					ch = 14;
					break;
				case ADC_CH3N_P15:
					ch = 15;
					break;
				case ADC_CH3P_P20:
					ch = 20;
					break;
				default:
					break;
				}

				if (ch == 14)
				{ //这里是温度值
					NTC_ADC_Value = value;
				}
				else if (ch == 15)
				{ //这里是药瓶电阻值
					DESTROY_ADC_Value = value;
				}
			}
		}
		channel_done_flag = 0;
	}
}



/**
 * @brief ADC采集函数
 * @param 无
 * @retval 无
 */
void ADCMeasureTask(void)
{
	int ret;

	ret = hal_adc_config_channel(adc_cfg, adc_evt);
	if (ret == 0)
	{
		hal_adc_start();
	}
}

static void BUP_ProcessOSALMsg( osal_event_hdr_t* pMsg )
{
    switch ( pMsg->event )
    {
		default:
        // do nothing
        break;
    }
}

/**
 * @brief ADC采集线程
 * @param task_id 线程的id号
 * @param events 任务的事件参数
 * @retval 我也不知道返回啥??
 */
uint16 Sensor_ProcessEvent(uint8 task_id, uint16 events)
{
    VOID task_id; // OSAL required parameter that isn't used in this function
    
    if (events & SYS_EVENT_MSG)
    {
      uint8* pMsg;

      if ((pMsg = osal_msg_receive(Sensor_TaskID)) != NULL)
      {
        BUP_ProcessOSALMsg((osal_event_hdr_t*)pMsg);
        // Release the OSAL message
        VOID osal_msg_deallocate(pMsg);
      }
      // return unprocessed events
      return (events ^ SYS_EVENT_MSG);
    }
	if (events & SENSOR_CYCLE_TIMER)
	{
		ADCMeasureTask();  //这个是ADC测量函数
		GetTemperature();  //这个是温度计算函数
		return (events ^ SENSOR_CYCLE_TIMER);
	}
	return 0;
}
