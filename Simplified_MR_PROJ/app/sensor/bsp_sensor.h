#ifndef __BSP_SENSOR_H__
#define __BSP_SENSOR_H__

#include "types.h"

//这里是按键的输入管脚(包括泵计数)
#define NTC_10K_AD		 P14    //温度AD端口
#define DESTROY_AD       P15	//药瓶类型判断AD端口

#define SENSOR_CYCLE_TIMER     0x0001


extern uint32_t NTC_ADC_Value;
extern uint32_t DESTROY_ADC_Value;
extern uint8_t percentage_of_repel_medicine;
extern uint8_t percentage_of_fumigate_medicine;


void CalculateMedicineQuantity(void);
void ADCMeasureTask(void);
float GetTemperature(void);
void Sensor_ProcessInit( uint8 task_id );
uint16 Sensor_ProcessEvent( uint8 task_id, uint16 events );




#endif