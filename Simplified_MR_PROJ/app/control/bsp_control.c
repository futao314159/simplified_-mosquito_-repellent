#include "board.h"

#define   FAN_OPEN_TIME       (30*60)
#define   PUMP_INTERVAL       (60)
#define   TIMES_PER_PUMP      (6)


static uint8 Control_TaskID;

static uint32_t PumpTargetCnt = 0;
static uint8_t PumpRunFlag = 0;
static uint8_t StartHeat = 0;

/**
  * @brief  加  
  * @param  无
  * @retval 无
  */
static void HeatControl(void)
{
  if (StartHeat == 1)
  { //开始灭蚊加热
    LOG("Current Temp:%d'C  NTC_ADC:%d\n", (uint16_t)GetTemperature(), NTC_ADC_Value);
    if ((uint16_t)GetTemperature() > ConfigParam.FactorySetting.target_temp)
    {
      CloseFumigateHeat();
      CloseRepelHeat();
    }
    else
    {
      OpenFumigateHeat();
      CloseRepelHeat();
    }
  }
  else
  {
    CloseFumigateHeat();
  }
}

/**
  * @brief  设  
  * @param  LoopCnt:要求泵转的圈数
  * @retval 无
  */
void SetPumpLoop(uint32_t LoopCnt)
{
  if (PumpRunFlag == 0)
  {                                                        //电机处于非运行状态
    PumpTargetCnt = LoopCnt + ConfigParam.PumpCNT.pumpcnt; //后一??参数??先前已经泵的次数(??写进了EEPROM??)
    PumpRunFlag = 1;                                       //该标志位会在电机??完指定的圈数后清??
    hal_gpio_fast_write(PUMP_CONTROL, 1);                  //开泵
  }
}

/**
  * @brief  根据泵电机的转动次数,来动态的改变当前计数值
  * @param  无
  * @retval 无
  */
static void IncPumpCntNum(void)
{
  static uint16_t cnt = 0;

  if (ConfigParam.Pump.is_empty == NOT_EMPTY)
  { //瓶子不为空
    if (ConfigParam.Pump.need_evacuation == NEED_EVACU)
    { //需要排气
      cnt++;
      if (cnt >= ConfigParam.FactorySetting.evacuation_times)
      { //空气排完了
        LOG("evacuation times:%d", cnt);
        cnt = 0;
        ConfigParam.Pump.need_evacuation = NOT_NEED_EVACU;
        /*************************************************************************************/
        osal_snv_write(ConfigParam.Pump.pump_id, PUMP_LENGTH, &(ConfigParam.Pump.need_evacuation));
        /*************************************************************************************/
        PumpTargetCnt -= ConfigParam.FactorySetting.evacuation_times; //
        hal_gpio_fast_write(PUMP_CONTROL, 0);                         //关泵
        PumpRunFlag = 0;
      }
    }
    else
    {                                //不需要排气
      ConfigParam.PumpCNT.pumpcnt++; //不需要排气时这个东东才会加加
      /*************************************************************************************/
      osal_snv_write(ConfigParam.PumpCNT.pumpcnt_id, PUMPCNT_LENGTH, (void*)&(ConfigParam.PumpCNT.pumpcnt));
      /*************************************************************************************/
      if (ConfigParam.PumpCNT.pumpcnt >= TIMES_OF_MEDICINE)
      {                                       //当前药瓶已空
        ConfigParam.Pump.is_empty = IS_EMPTY; //泵了超过规定的次数,将药瓶状态设置为空
        OpenOptionLED();
        /*************************************************************************************/
        osal_snv_write(ConfigParam.Pump.pump_id, PUMP_LENGTH, &(ConfigParam.Pump.need_evacuation));
        /*************************************************************************************/
        hal_gpio_fast_write(PUMP_CONTROL, 0); //关泵
        PumpRunFlag = 0;
      }
      if (ConfigParam.PumpCNT.pumpcnt >= PumpTargetCnt)
      {                                       //泵电机,转动了指定圈数后,电机停止   PumpBottleTargetCnt;这个数得动态的增加
        hal_gpio_fast_write(PUMP_CONTROL, 0); //关泵
        PumpRunFlag = 0;
      }
    }
  }
  if (ConfigParam.Pump.is_empty == IS_EMPTY)
  {                                       //瓶子为空
    hal_gpio_fast_write(PUMP_CONTROL, 0); //关泵
    PumpRunFlag = 0;
  }
}

/**
 *	@brief  烧电阻管脚初始化函数
 *	@param	无
 *	@return	无
 */
static void Destroy_GPIO_Init(void)
{
  hal_gpio_pin_init(DESTROY_CONTROL, OEN);
  hal_gpioretention_register(DESTROY_CONTROL); //enable this pin retention
  hal_gpio_fast_write(DESTROY_CONTROL, 0);
}

/**
 *	@brief  PUMP控制管脚初始化函数
 *	@param	无
 *	@return	无
 */
static void PUMP_Control_GPIO_Init(void)
{
  hal_gpio_pin_init(PUMP_CONTROL, OEN);
  hal_gpioretention_register(PUMP_CONTROL); //enable this pin retention
  hal_gpio_fast_write(PUMP_CONTROL, 0);
}


/*********************************************************************************************************************************/

/**
 *	@brief  熏蒸面积指示(主要就是根据熏蒸面积,点亮相应的LED灯)
 *	@param	无
 *	@return	无
 */
void Fumigate_Area_Indicate(void)
{
  switch (ConfigParam.FumigateSetting.fumi_sett_area)
  {
  case ENUM_5_15_M2_F:
    Indicate5_15m2();
    break;
  case ENUM_15_30_M2_F:
    Indicate15_30m2();
    break;
  case ENUM_30_50_M2_F:
    Indicate30_50m2();
    break;
  case ENUM_50_80_M2_F:
    Indicate50_80m2();
    break;
  case ENUM_80_100_M2_F:
    Indicate80_100m2();
    break;
  case ENUM_100_150_M2_F:
    Indicate100_150m2();
    break;
  case ENUM_150_200_M2_F:
    Indicate150_200m2();
    break;
  default:
    Indicate5_15m2();
    break;
  }
}

uint32_t medicine_quant = 0;

/**
 *	@brief  根据当前灭蚊面积,确定灭蚊时间
 *	@param	无
 *	@return	无
 */
static void Calculate_Fumigate_Time(void)
{
  hal_watchdog_feed(); //喂狗
  switch (ConfigParam.FumigateSetting.fumi_sett_area)
  {                    //下面根据设置选项，选择对应的药剂量
  case ENUM_5_15_M2_F: //房间面积  (0-15m2)
    medicine_quant = (15UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
    break;
  case ENUM_15_30_M2_F: //房价面积  (15-30m2)
    medicine_quant = (30UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
    break;
  case ENUM_30_50_M2_F: //房间面积  (30-50m2)
    medicine_quant = (50UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
    break;
  case ENUM_50_80_M2_F: //房间面积  (50-80m2)
    medicine_quant = (80UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
    break;
  case ENUM_80_100_M2_F: //房间面积  (80-100m2)
    medicine_quant = (100UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
    break;
  case ENUM_100_150_M2_F: //房间面积  (100-150m2)
    medicine_quant = (150UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
    break;
  case ENUM_150_200_M2_F: //房间面积  (150-200m2)
    medicine_quant = (200UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
    break;
  default:
    medicine_quant = (15UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
    break;
  }
  float times = 2.0;

  switch (ConfigParam.Pump.type)
  {
  case NORMAL_MEDICINE:times = 2.0;LOG("NORMAL_MEDICINE\n");break;
  case MEDIAN_MEDICINE:times = 1.5;LOG("MEDIAN_MEDICINE\n");break;
  case ENCHANCE_MEDICINE:times = 1.0;LOG("ENCHANCE_MEDICINE\n");break;
  default:times = 2.0;break;
  }

  switch (ConfigParam.WorkingMode.working_mode)
  {
  case ENUM_FUMIGATE_NORMAL_MODE:
    medicine_quant = medicine_quant * times + 0.9F;
    break;
  case ENUM_FUMIGATE_ENCHANCE_MODE:
    medicine_quant = medicine_quant * 1.2F * times + 0.9F;
    break;
  default:
    medicine_quant = medicine_quant * times + 0.9F;
    break;
  }
  LOG("medicine_quant:%d\n", medicine_quant);
  ConfigParam.FactorySetting.fumigate_time = medicine_quant * PUMP_INTERVAL / (TIMES_PER_PUMP * 60) + 20L; //熏蒸时间是根据药量来算的
  LOG("fumigate_time:%d\n", ConfigParam.FactorySetting.fumigate_time);
  osal_snv_write(ConfigParam.FactorySetting.Factory_Sett_id, FACTORY_SETT_LENGTH, &(ConfigParam.FactorySetting.evacuation_times));
}

/**
 *	@brief  5_15m2面积选择
 *	@param	无
 *	@return	无
 */
void Set_Fumigate_Area5_15m2(void)
{
  ConfigParam.FumigateSetting.fumi_sett_area = ENUM_5_15_M2_F;
  osal_snv_write(ConfigParam.FumigateSetting.fumi_sett_id, FUMIGATE_SETT_LENGTH, &(ConfigParam.FumigateSetting.fumi_house_height.dat_f));
  Calculate_Fumigate_Time();
}

/**
 *	@brief  15_30m2面积选择
 *	@param	无
 *	@return	无
 */
void Set_Fumigate_Area15_30m2(void)
{
  ConfigParam.FumigateSetting.fumi_sett_area = ENUM_15_30_M2_F;
  osal_snv_write(ConfigParam.FumigateSetting.fumi_sett_id, FUMIGATE_SETT_LENGTH, &(ConfigParam.FumigateSetting.fumi_house_height.dat_f));
  Calculate_Fumigate_Time();
}

/**
 *	@brief  30_50m2面积选择
 *	@param	无
 *	@return	无
 */
void Set_Fumigate_Area30_50m2(void)
{
  ConfigParam.FumigateSetting.fumi_sett_area = ENUM_30_50_M2_F;
  osal_snv_write(ConfigParam.FumigateSetting.fumi_sett_id, FUMIGATE_SETT_LENGTH, &(ConfigParam.FumigateSetting.fumi_house_height.dat_f));
  Calculate_Fumigate_Time();
}

/**
 *	@brief  50_80m2面积选择
 *	@param	无
 *	@return	无
 */
void Set_Fumigate_Area50_80m2(void)
{
  ConfigParam.FumigateSetting.fumi_sett_area = ENUM_50_80_M2_F;
  osal_snv_write(ConfigParam.FumigateSetting.fumi_sett_id, FUMIGATE_SETT_LENGTH, &(ConfigParam.FumigateSetting.fumi_house_height.dat_f));
  Calculate_Fumigate_Time();
}

/**
 *	@brief  80_100m2面积选择
 *	@param	无
 *	@return	无
 */
void Set_Fumigate_Area80_100m2(void)
{
  ConfigParam.FumigateSetting.fumi_sett_area = ENUM_80_100_M2_F;
  osal_snv_write(ConfigParam.FumigateSetting.fumi_sett_id, FUMIGATE_SETT_LENGTH, &(ConfigParam.FumigateSetting.fumi_house_height.dat_f));
  Calculate_Fumigate_Time();
}

/**
 *	@brief  100_150m2面积选择
 *	@param	无
 *	@return	无
 */
void Set_Fumigate_Area100_150m2(void)
{
  ConfigParam.FumigateSetting.fumi_sett_area = ENUM_100_150_M2_F;
  osal_snv_write(ConfigParam.FumigateSetting.fumi_sett_id, FUMIGATE_SETT_LENGTH, &(ConfigParam.FumigateSetting.fumi_house_height.dat_f));
  Calculate_Fumigate_Time();
}

/**
 *	@brief  150_200m2面积选择
 *	@param	无
 *	@return	无
 */
void Set_Fumigate_Area150_200m2(void)
{
  ConfigParam.FumigateSetting.fumi_sett_area = ENUM_150_200_M2_F;
  osal_snv_write(ConfigParam.FumigateSetting.fumi_sett_id, FUMIGATE_SETT_LENGTH, &(ConfigParam.FumigateSetting.fumi_house_height.dat_f));
  Calculate_Fumigate_Time();
}
/**********************************************************************************/

// /**
//  *	@brief  5_15m2面积选择
//  *	@param	无
//  *	@return	无
//  */
// void Set_Repel_Area5_15m2(void)
// {
//   ConfigParam.RepelSetting.repel_sett_area = ENUM_5_15_M2_R;
//   osal_snv_write(ConfigParam.RepelSetting.repel_sett_id,REPEL_SETT_LENGTH,&(ConfigParam.RepelSetting.total_repel_time));		
// }

// /**
//  *	@brief  15_30m2面积选择
//  *	@param	无
//  *	@return	无
//  */
// void Set_Repel_Area15_30m2(void)
// {
//   ConfigParam.RepelSetting.repel_sett_area = ENUM_15_30_M2_R;
//   osal_snv_write(ConfigParam.RepelSetting.repel_sett_id,REPEL_SETT_LENGTH,&(ConfigParam.RepelSetting.total_repel_time));
// }

// /**
//  *	@brief  30_50m2面积选择
//  *	@param	无
//  *	@return	无
//  */
// void Set_Repel_Area30_50m2(void)
// {
//   ConfigParam.RepelSetting.repel_sett_area = ENUM_30_50_M2_R;
//   osal_snv_write(ConfigParam.RepelSetting.repel_sett_id,REPEL_SETT_LENGTH,&(ConfigParam.RepelSetting.total_repel_time));
// }



/**
 *	@brief  熏蒸面积选择
 *	@param	无
 *	@return	无
 */
static void Fumigate_Area_Select(void)
{
  if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
  {   //意思是在停机状态下,才可进行选择
    switch (ConfigParam.FumigateSetting.fumi_sett_area)
    {
    case ENUM_5_15_M2_F:
      Set_Fumigate_Area15_30m2();
      // Set_Repel_Area15_30m2();
      break;
    case ENUM_15_30_M2_F:
      Set_Fumigate_Area30_50m2();
      // Set_Repel_Area30_50m2();
      break;
    case ENUM_30_50_M2_F:
      Set_Fumigate_Area50_80m2();
      // Set_Repel_Area30_50m2();
      break;
    case ENUM_50_80_M2_F:
      Set_Fumigate_Area80_100m2();
      // Set_Repel_Area30_50m2();
      break;
    case ENUM_80_100_M2_F:
      if (ConfigParam.Pump.type == NORMAL_MEDICINE)
      {
        Set_Fumigate_Area5_15m2();
        // Set_Repel_Area5_15m2();
      }
      else if (ConfigParam.Pump.type == MEDIAN_MEDICINE)
      {
        Set_Fumigate_Area100_150m2();
        // Set_Repel_Area30_50m2();
      }
      else if (ConfigParam.Pump.type == ENCHANCE_MEDICINE)
      {
        Set_Fumigate_Area100_150m2();
        // Set_Repel_Area30_50m2();
      }
      break;
    case ENUM_100_150_M2_F:
      if (ConfigParam.Pump.type == NORMAL_MEDICINE)
      {
        Set_Fumigate_Area5_15m2();
        // Set_Repel_Area5_15m2();
      }
      else if (ConfigParam.Pump.type == MEDIAN_MEDICINE)
      {
        Set_Fumigate_Area5_15m2();
        // Set_Repel_Area5_15m2();
      }
      else if (ConfigParam.Pump.type == ENCHANCE_MEDICINE)
      {
        Set_Fumigate_Area150_200m2();
        // Set_Repel_Area30_50m2();
      }
      break;
    case ENUM_150_200_M2_F:
      if (ConfigParam.Pump.type == NORMAL_MEDICINE)
      {
        Set_Fumigate_Area5_15m2();
        // Set_Repel_Area5_15m2();
      }
      else if (ConfigParam.Pump.type == MEDIAN_MEDICINE)
      {
        Set_Fumigate_Area5_15m2();
        // Set_Repel_Area5_15m2();
      }
      else if (ConfigParam.Pump.type == ENCHANCE_MEDICINE)
      {
        Set_Fumigate_Area5_15m2();
        // Set_Repel_Area5_15m2();
      }
      break;
    default:
      Set_Fumigate_Area5_15m2();
      // Set_Repel_Area5_15m2();
      break;
    }
    Fumigate_Area_Indicate();
  }
}


// /**
//  *	@brief  驱蚊面积指示(主要就是根据驱蚊面积,点亮相应的LED灯)
//  *	@param	无
//  *	@return	无
//  */
// void Repel_Area_Indicate(void)
// {
//   switch (ConfigParam.RepelSetting.repel_sett_area)
//   {
//   case ENUM_5_15_M2_R:
//     Indicate5_15m2();
//     break;
//   case ENUM_15_30_M2_R:
//     Indicate15_30m2();
//     break;
//   case ENUM_30_50_M2_R:
//     Indicate30_50m2();
//     break;
//   default:
//     Indicate5_15m2();
//     break;
//   }
// }

/*****************************************************************************************************/

/**
 * @brief  将当前系统模式调为增强灭蚊模式
 * @param  无
 * @retval 无
 */
void FumigateEnchanceFunction(void)
{
  if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
  {
    ConfigParam.WorkingMode.working_mode = ENUM_FUMIGATE_ENCHANCE_MODE;
    ConfigParam.WorkingMode.fumigate_buf_mode = ENUM_FUMIGATE_ENCHANCE_MODE;
    osal_snv_write(ConfigParam.WorkingMode.working_mode_id, WORKING_MODE_LENGTH, &(ConfigParam.WorkingMode.working_mode));
  }
}

/**
 * @brief  将当前系统模式调为增强灭蚊模式
 * @param  无
 * @retval 无
 */
void MosFumigateNormalFunction(void)
{
  if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
  {
    ConfigParam.WorkingMode.working_mode = ENUM_FUMIGATE_NORMAL_MODE;
    ConfigParam.WorkingMode.fumigate_buf_mode = ENUM_FUMIGATE_NORMAL_MODE;
    osal_snv_write(ConfigParam.WorkingMode.working_mode_id, WORKING_MODE_LENGTH, &(ConfigParam.WorkingMode.working_mode));
  }
}

/**
 * @brief 将当前系统模式调为空调驱蚊模式
 * @param  无
 * @retval 无
 */
void AirConditionerFunction(void)
{
  if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
  {   //停机模式才可以进行设置
    ConfigParam.WorkingMode.working_mode = ENUM_REPEL_AIR_CONDI_MODE;
    ConfigParam.WorkingMode.repel_buf_mode = ENUM_REPEL_AIR_CONDI_MODE,
      osal_snv_write(ConfigParam.WorkingMode.working_mode_id, WORKING_MODE_LENGTH, &(ConfigParam.WorkingMode.working_mode));
  }
}

/**
 * @brief 将当前系统模式调为母婴驱蚊模式
 * @param  无
 * @retval 无
 */
void MaternalFunction(void)
{
  if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
  {
    ConfigParam.WorkingMode.working_mode = ENUM_REPEL_MATERNAL_MODE;
    ConfigParam.WorkingMode.repel_buf_mode = ENUM_REPEL_MATERNAL_MODE,
      osal_snv_write(ConfigParam.WorkingMode.working_mode_id, WORKING_MODE_LENGTH, &(ConfigParam.WorkingMode.working_mode));
  }
}

/**
 * @brief 将当前系统模式调为增强驱蚊模式
 * @param  无
 * @retval 无
 */
void RepelEnchanceFunction(void)
{
  if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
  {
    ConfigParam.WorkingMode.working_mode = ENUM_REPEL_ENCHANCE_MODE;
    ConfigParam.WorkingMode.repel_buf_mode = ENUM_REPEL_ENCHANCE_MODE,
      osal_snv_write(ConfigParam.WorkingMode.working_mode_id, WORKING_MODE_LENGTH, &(ConfigParam.WorkingMode.working_mode));
  }
}

/**
 * @brief 将当前系统模式调为香薰模式
 * @param  无
 * @retval 无
 */
void AromaFunction(void)
{
  if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
  {
    ConfigParam.WorkingMode.working_mode = ENUM_REPEL_AROMA_MODE;
    //ConfigParam.WorkingMode.repel_buf_mode=ENUM_AROMA_MODE,
    osal_snv_write(ConfigParam.WorkingMode.working_mode_id, WORKING_MODE_LENGTH, &(ConfigParam.WorkingMode.working_mode));
  }
}



/*********************************************************************************************************************************/
/**
 *	@brief  根据当前工作模式点亮相关LED灯
 *	@param	无
 *	@return	无
 */
void Change_Mode_LED(void)
{
  Fumigate_Area_Indicate();
  if (ConfigParam.Pump.is_empty == IS_EMPTY || ConfigParam.RepelSetting.total_repel_time < 10 || notify_flag == 1)
  {
    OpenOptionLED();
  }
  else
  {
    CloseOptionLED();
  }
}


/**
 *	@brief  切换当前面积(灭蚊和驱蚊混合在了一起)
 *	@param	无
 *	@return	无
 */
void Area_Select(void)
{
  Fumigate_Area_Select();
}

/*************************************************************************************************/

/**
 *	@brief  遥控灭蚊开函数
 *	@param	无
 *	@return	无
 */
void RemoteControl_Fumigate_Start(void)
{
  ConfigParam.WorkingStatus.working_status = ENUM_WORKING_READY;
  ConfigParam.WorkingMode.working_mode = ConfigParam.WorkingMode.fumigate_buf_mode;    //这里没有进行存储
  // osal_snv_write(ConfigParam.WorkingMode.working_mode_id,WORKING_MODE_LENGTH,&(ConfigParam.WorkingMode.working_mode));
  Ble_Tx_Action("SET_FUMIGATE_START"); //将命令返回
}

/**
 *	@brief  遥控灭蚊关函数
 *	@param	无
 *	@return	无
 */
void RemoteControl_Fumigate_Stop(void)
{
  ConfigParam.WorkingStatus.working_status = ENUM_WORKING_STOP;
  CloseFumigateHeat();
  StartHeat = 0;           //关闭加热
  Ble_Tx_Action("SET_FUMIGATE_STOP"); //将命令返回
}

/**
 *	@brief  遥控驱蚊开函数
 *	@param	无
 *	@return	无
 */
void RemoteControl_Repel_Start(void)
{
  ConfigParam.WorkingStatus.working_status = ENUM_WORKING_READY;
  ConfigParam.WorkingMode.working_mode = ConfigParam.WorkingMode.repel_buf_mode; //这里没有进行存储
  // osal_snv_write(ConfigParam.WorkingMode.working_mode_id,WORKING_MODE_LENGTH,&(ConfigParam.WorkingMode.working_mode));
  minute_cnt = ConfigParam.RepelSetting.total_repel_time;
  LOG("minute_cnt=%d\n", minute_cnt);
  Ble_Tx_Action("SET_REPEL_START"); //将命令返回
}

/**
 *	@brief  遥控驱蚊关函数
 *	@param	无
 *	@return	无
 */
void RemoteControl_Repel_Stop(void)
{
  ConfigParam.WorkingStatus.working_status = ENUM_WORKING_STOP;
  CloseRepelHeat();
  Ble_Tx_Action("SET_REPEL_STOP"); //将命令返回
}

/**
 *	@brief  遥控香薰开函数
 *	@param	无
 *	@return	无
 */
void RemoteControl_Aroma_Start(void)
{
  ConfigParam.WorkingStatus.working_status = ENUM_WORKING_READY;
  ConfigParam.WorkingMode.working_mode = ENUM_REPEL_AROMA_MODE;    //这里没有进行存储
  osal_snv_write(ConfigParam.WorkingMode.working_mode_id, WORKING_MODE_LENGTH, &(ConfigParam.WorkingMode.working_mode));
  minute_cnt = ConfigParam.RepelSetting.total_repel_time;
  LOG("minute_cnt=%d\n", minute_cnt);
  Ble_Tx_Action("SET_AROMA_START"); //将命令返回	
}

/**
 *	@brief  遥控香薰关函数
 *	@param	无
 *	@return	无
 */
void RemoteControl_Aroma_Stop(void)
{
  ConfigParam.WorkingStatus.working_status = ENUM_WORKING_STOP;
  CloseRepelHeat();
  Ble_Tx_Action("SET_AROMA_STOP"); //将命令返回
}
/*************************************************************************************************/

/**
 *	@brief  按键动作函数,根据输入的键码,执行相应的动作
 *	@param	keycode :键码
 *	@return	无
 */
void KeyAction(uint8_t keycode)
{
  switch (keycode)
  {
  case 0:
    if (LongKeyFlag == 0)
    { //表示此时是短按电容按键后,按键弹起来的结果!!
      Buzzer_Beep_Once();
      Area_Select();

      if (ConfigParam.WorkingMode.working_mode == ENUM_FUMIGATE_NORMAL_MODE
        || ConfigParam.WorkingMode.working_mode == ENUM_FUMIGATE_ENCHANCE_MODE
        )
      {
        RemoteControl_Fumigate_Stop();
      }
      else if (ConfigParam.WorkingMode.working_mode == ENUM_REPEL_AIR_CONDI_MODE
        || ConfigParam.WorkingMode.working_mode == ENUM_REPEL_MATERNAL_MODE
        || ConfigParam.WorkingMode.working_mode == ENUM_REPEL_ENCHANCE_MODE
        )
      {
        RemoteControl_Repel_Stop();
      }
      else if (ConfigParam.WorkingMode.working_mode == ENUM_REPEL_AROMA_MODE)
      {
        RemoteControl_Aroma_Stop();
      }
    }
    break;
  case 1:
    //这里执行泵计数函数
    IncPumpCntNum();
    break;
  case 2: //这里表示长按2s
    RemoteControl_Repel_Start();
    break;
  case 3: //这里表示长按4s
    RemoteControl_Fumigate_Start();
    break;
  default:
    break;
  }
}

static void delay_Xms(uint32_t nCount)
{
  volatile uint32_t tick_cnt = 0;

  tick_cnt = getMcuPrecisionCount();
  nCount = 1.6F * nCount;

  while ((getMcuPrecisionCount() - tick_cnt) < nCount)
  {
    ;
  }
}

uint8_t adc_sample_done = 0;

/**
  * @brief  ADC采样开始函数
  *
  * @param  无
  * @retval 无
  */
static void ADC_Sample_Done(void)
{
  adc_sample_done = 0;
  ADCMeasureTask();
  while (adc_sample_done == 0)
  {
    ;
  }
}

uint8_t notify_flag = 0; //提示??瓶没有安装好标志

/**
  * @brief  判断当前药剂种类,总共有三种药剂    在开启熏蒸之前调用,在进入设置熏蒸面积时使用 简易版的灭蚊器,它的下面电阻值为590R
  *         如果是新瓶子则   对应电压 668.5mV     空瓶子和463.0之间相隔205.5         电压值得重新计算
  *         药瓶上电阻4.7K  对应电压463.0mV       药剂1  低效  和343.8之间相隔 119.2        15.29K
  *         药瓶上电阻10K   对应电压343.8mV         药剂2  中效  和217.2之间相隔 126.6      20.59K
  *         药瓶上电阻22K   对应电压217.2mV         药剂3  高效                            32.59K
  *
  * @param  无
  * @retval 无
  */
void MedicineBottleResistor(void)
{
  uint32_t dat_buff[6];
  uint8_t is_new1 = 0;
  uint8_t i = 0;

  ADC_Sample_Done();    //实时更新药瓶AD值
  dat_buff[0] = DESTROY_ADC_Value;             //这个值会每1s刷新5次
  dat_buff[0] = dat_buff[0] * 8000UL / 4096UL; //实际值就是800mV
  ADC_Sample_Done();    //实时更新药瓶AD值
  dat_buff[0] = DESTROY_ADC_Value;             //这个值会每1s刷新5次
  dat_buff[0] = dat_buff[0] * 8000UL / 4096UL; //实际值就是800mV
  if (5685 < dat_buff[0])
  { //表示可能是新瓶子
    is_new1 = 1;
  }
  else
  { //表示不是新瓶子
    is_new1 = 0;
  }

  if (is_new1 == 1)
  { //当可能为新瓶子时,开始烧电阻
    LOG("new bottle!\n");
    hal_gpio_fast_write(DESTROY_CONTROL, 1);
    hal_watchdog_feed(); //喂狗

    delay_Xms(1000); //烧保险持续300ms
    hal_watchdog_feed(); //喂狗
    hal_gpio_fast_write(DESTROY_CONTROL, 0);
    delay_Xms(200); //等待200ms
    hal_watchdog_feed(); //喂狗
  }
  //上面烧完了电阻
  /***********************************************************************************************/
  for (i = 0; i < 6; ++i)
  {
    LOG("for %d\n", i);
    ADC_Sample_Done();  //实时更新药瓶AD值
    dat_buff[0] = DESTROY_ADC_Value; //
    dat_buff[0] = dat_buff[0] * 8000UL / 4096UL;
    ADC_Sample_Done();  //实时更新药瓶AD值
    dat_buff[0] = DESTROY_ADC_Value; //
    dat_buff[0] = dat_buff[0] * 8000UL / 4096UL;
    if (4130 < dat_buff[0] && dat_buff[0] < 5130)
    { //对应电压463.0mV     药剂1 低效药剂
      LOG("VCC:%d\n", dat_buff[0]);
      CloseOptionLED();
      if (is_new1 == 1)
      { //表示安装了新瓶子
        is_new1 = 0;
        ConfigParam.Pump.is_empty = NOT_EMPTY;
        ConfigParam.Pump.type = NORMAL_MEDICINE;
        ConfigParam.Pump.need_evacuation = NEED_EVACU; //新瓶子需要排空气标志
        LOG("NOT_EMPTY NORMAL_MEDICINE  NEED_EVACU\r\n");
        osal_snv_write(ConfigParam.Pump.pump_id, PUMP_LENGTH, &(ConfigParam.Pump.need_evacuation));

        ConfigParam.PumpCNT.pumpcnt = 0; //泵计数归零
        PumpTargetCnt = 0;
        osal_snv_write(ConfigParam.PumpCNT.pumpcnt_id, PUMPCNT_LENGTH, (void*)&(ConfigParam.PumpCNT.pumpcnt));


        Set_Fumigate_Area5_15m2();  //一旦换了新瓶子,就将面积设为最小
        hal_watchdog_feed();
      }
      notify_flag = 0; //瓶子装好了,不用提示
      goto des;        //准备退出
    }
    else if (2938 < dat_buff[0] && dat_buff[0] < 3938)
    { //对应电压343.8mV     药剂2  中效
      LOG("VCC:%d\n", dat_buff[0]);
      CloseOptionLED();
      if (is_new1 == 1)
      { //表示安装了新瓶子
        is_new1 = 0;
        ConfigParam.Pump.is_empty = NOT_EMPTY;
        ConfigParam.Pump.type = MEDIAN_MEDICINE;
        ConfigParam.Pump.need_evacuation = NEED_EVACU; //新瓶子需要排空气标志
        LOG("NOT_EMPTY MEDIAN_MEDICINE  NEED_EVACU\r\n");
        osal_snv_write(ConfigParam.Pump.pump_id, PUMP_LENGTH, &(ConfigParam.Pump.need_evacuation));

        ConfigParam.PumpCNT.pumpcnt = 0; //泵计数归零
        PumpTargetCnt = 0;
        osal_snv_write(ConfigParam.PumpCNT.pumpcnt_id, PUMPCNT_LENGTH, (void*)&(ConfigParam.PumpCNT.pumpcnt));
        Set_Fumigate_Area5_15m2();  //一旦换了新瓶子,就将面积设为最小
        hal_watchdog_feed(); //
      }
      notify_flag = 0; //瓶子装好了,不用提示
      goto des;        //准备退出
    }
    else if (1672 < dat_buff[0] && dat_buff[0] < 2672)
    { //对应电压217.2mV     药剂1  高效
      LOG("VCC:%d\n", dat_buff[0]);
      CloseOptionLED();
      if (is_new1 == 1)
      { //表示安装了新瓶子
        is_new1 = 0;
        ConfigParam.Pump.is_empty = NOT_EMPTY;
        ConfigParam.Pump.type = ENCHANCE_MEDICINE;
        ConfigParam.Pump.need_evacuation = NEED_EVACU; //新瓶子需要排空气标志
        LOG("NOT_EMPTY ENCHANCE_MEDICINE  NEED_EVACU\r\n");
        osal_snv_write(ConfigParam.Pump.pump_id, PUMP_LENGTH, &(ConfigParam.Pump.need_evacuation));

        ConfigParam.PumpCNT.pumpcnt = 0; //泵计数归零
        PumpTargetCnt = 0;
        osal_snv_write(ConfigParam.PumpCNT.pumpcnt_id, PUMPCNT_LENGTH, (void*)&(ConfigParam.PumpCNT.pumpcnt));
        Set_Fumigate_Area5_15m2();  //一旦换了新瓶子,就将面积设为最小
        hal_watchdog_feed(); //
      }
      notify_flag = 0; //瓶子装好了,不用提示
      goto des;        //准备退出
    }

    hal_watchdog_feed(); //喂狗
    delay_Xms(50); //等待50ms
    hal_watchdog_feed(); //喂狗
  }
  /***********************************************************************************************/
  //能到这一步说明没有装瓶子,或者探针没有接好
  notify_flag = 1; //将药瓶没有安装好标志位置1
  OpenOptionLED();
des:
  Fumigate_Area_Indicate();  //指示当前面积
}

/**
  * @brief  运  
  * @param  无
  * @retval 无
  */
static void RunIndicate(void)
{
  static uint8_t mark = 0;

  hal_watchdog_feed();
  if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
  {
    mark = 0;
    Repel_Run_LED_Close();
    Fumigate_Run_LED_Close();
  }
  else
  {
    if (mark == 0)
    { //这里是开LED
      mark = 1;
      if (ConfigParam.WorkingMode.working_mode == ENUM_REPEL_AIR_CONDI_MODE ||
        ConfigParam.WorkingMode.working_mode == ENUM_REPEL_MATERNAL_MODE ||
        ConfigParam.WorkingMode.working_mode == ENUM_REPEL_ENCHANCE_MODE ||
        ConfigParam.WorkingMode.working_mode == ENUM_REPEL_AROMA_MODE)
      {
        Repel_Run_LED_Open();
        Fumigate_Run_LED_Close();
      }
      else if (ConfigParam.WorkingMode.working_mode == ENUM_FUMIGATE_ENCHANCE_MODE ||
        ConfigParam.WorkingMode.working_mode == ENUM_FUMIGATE_NORMAL_MODE)
      {
        Fumigate_Run_LED_Open();
        Repel_Run_LED_Close();
      }
    }
    else
    { //这里是关LED
      mark = 0;
      Repel_Run_LED_Close();
      Fumigate_Run_LED_Close();
    }
  }
}


WORKING_STEP_Typedef working_step = STEP_ALL; //状态记录用结构体
uint16_t second_cnt = 0;                      //
uint16_t second_cnt_buff = 0;

/**
  * @brief  普  
  * @param  无
  * @retval 无
  */
void FumigateNormal(void)
{
  static uint32_t medicine_quantity = 0;

  if (ConfigParam.WorkingMode.working_mode == ENUM_FUMIGATE_NORMAL_MODE)
  { //如果当前熏蒸模式是 普通模式
    if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_READY)
    { //这里是点下开始键后
      ConfigParam.WorkingStatus.working_status = ENUM_WORKING_START;
      working_step = STEP2; //
      MedicineBottleResistor(); //开始探测药剂种类
      Calculate_Fumigate_Time();  //重新计算灭蚊的时间
      if (ConfigParam.Pump.is_empty == IS_EMPTY || notify_flag == 1)
      { //没药了或者 药瓶没装好
        LOG("bottle uninstall!\n");  //
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_STOP;
        Ble_Tx_Action("SET_FUMIGATE_STOP"); //将命令返回
        OpenOptionLED();
        if (notify_flag == 1)
        {
          Ble_Tx_Action("BOTTLE_UNINSTALL");
        }
      }
      // if (ConfigParam.FactorySetting.target_temp != ConfigParam.FactorySetting.target_temp_backup)
      // { //让目标温度值还原
      //   ConfigParam.FactorySetting.target_temp = ConfigParam.FactorySetting.target_temp_backup;
      //   osal_snv_write(ConfigParam.FactorySetting.Factory_Sett_id, FACTORY_SETT_LENGTH, &(ConfigParam.FactorySetting.evacuation_times));
      // }
      if (ConfigParam.Pump.need_evacuation == NEED_EVACU)
      { //需要排空气
        LOG("evacuation:%d\n", ConfigParam.FactorySetting.evacuation_times);
        SetPumpLoop(ConfigParam.FactorySetting.evacuation_times);
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_WAIT)
    { //这里等待状态
      if (second_cnt > FAN_OPEN_TIME)
      {
        second_cnt--;
      }
      if (working_step == STEP5)
      {
        // if ((second_cnt_buff - second_cnt) == 300)
        // { //过了5min
        //   ConfigParam.FactorySetting.target_temp = TARGET_TEMP - 45;
        //   osal_snv_write(ConfigParam.FactorySetting.Factory_Sett_id, FACTORY_SETT_LENGTH, &(ConfigParam.FactorySetting.evacuation_times));
        // }
        if ((second_cnt_buff - second_cnt) % PUMP_INTERVAL == 0)
        { //每1minute秒喷TIMES_PER_PUMP次
          if (medicine_quantity >= TIMES_PER_PUMP)
          {
            SetPumpLoop(TIMES_PER_PUMP); //TIMES_PER_PUMP次
            medicine_quantity -= TIMES_PER_PUMP;
          }
          else if (medicine_quantity > 0)
          {                                 //到这里说明 TIMES_PER_PUMP> medicine_quantity >0
            SetPumpLoop(medicine_quantity); //有多少喷多少
            medicine_quantity = 0;
          }
        }
        if (second_cnt <= FAN_OPEN_TIME)
        { //熏蒸结束,准???停??
          ConfigParam.WorkingStatus.working_status = ENUM_FAN_WORKING;
        }
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_START)
    {
      if (working_step == STEP2)
      { //这一步可以开风机,加热
        OpenFan();
        StartHeat = 1;
        CloseRepelHeat();
        Buzzer_Beep_Three_Times(); //响三下
        working_step = STEP3;
      }
      if (working_step == STEP3)
      {                                                                            //表示??以开??油泵??(前提??温度到了)
        if ((uint16_t)GetTemperature() > (ConfigParam.FactorySetting.target_temp)) //
        {                                                                          //pid整定完成,并且温度达到了???求 。，
          switch (ConfigParam.FumigateSetting.fumi_sett_area)
          {                    //下面根据设置选项，选择对应的药剂量
          case ENUM_5_15_M2_F: //房间面积  (0-15m2)
            medicine_quantity = (15UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_15_30_M2_F: //房价面积  (15-30m2)
            medicine_quantity = (30UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_30_50_M2_F: //房间面积  (30-50m2)
            medicine_quantity = (50UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_50_80_M2_F: //房间面积  (50-80m2)
            medicine_quantity = (80UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_80_100_M2_F: //房间面积  (80-100m2)
            medicine_quantity = (100UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_100_150_M2_F: //房间面积  (100-150m2)
            medicine_quantity = (150UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_150_200_M2_F: //房间面积  (150-200m2)
            medicine_quantity = (200UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          default:
            medicine_quantity = (15UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          }
          float times = 2.0;

          switch (ConfigParam.Pump.type)
          {
          case NORMAL_MEDICINE:times = 2.0;break;
          case MEDIAN_MEDICINE:times = 1.5;break;
          case ENCHANCE_MEDICINE:times = 1.0;break;
          default:times = 2.0;break;
          }
          medicine_quantity = medicine_quantity * times + 0.9F; //增强模式是普通模式的1.2倍


          if (medicine_quantity >= TIMES_PER_PUMP)
          {
            SetPumpLoop(TIMES_PER_PUMP); //TIMES_PER_PUMP次
            medicine_quantity -= TIMES_PER_PUMP;
          }
          else if (medicine_quantity > 0)
          {                                 //到这里说明 TIMES_PER_PUMP> medicine_quantity >0
            SetPumpLoop(medicine_quantity); //有多少喷多少
            medicine_quantity = 0;
          }
          working_step = STEP4;
        }
        else
        {
          Buzzer_Beep_Three_Times(); //响三下
        }
      }
      if (working_step == STEP4)
      {                                                                               //这里设置一??定时??,用来设定什么时候停??
        second_cnt_buff = second_cnt = ConfigParam.FactorySetting.fumigate_time * 60 + FAN_OPEN_TIME; //需要工作的时间
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_WAIT;                 //进入到监??
        working_step = STEP5;
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_FAN_WORKING)
    {
      working_step = STEP_ALL; //状态???位
      StartHeat = 0;           //关闭加热


      if (second_cnt > 0)
      {
        second_cnt--;
      }
      if (second_cnt == 0)
      {
        CloseFan();
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_STOP;
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
    {
      // if(ConfigParam.FactorySetting.target_temp!=ConfigParam.FactorySetting.target_temp_backup)
      // { //让目标温度值还原
      //   ConfigParam.FactorySetting.target_temp=ConfigParam.FactorySetting.target_temp_backup;
      //   osal_snv_write(ConfigParam.FactorySetting.Factory_Sett_id,FACTORY_SETT_LENGTH,&(ConfigParam.FactorySetting.evacuation_times));
      // }
      second_cnt = 0;          //计时时间归零
      working_step = STEP_ALL; //状态???位
      StartHeat = 0;           //关闭加热
      CloseFan();
    }
    RunIndicate();
  }
}

/**
  * @brief  增  
  * @param  无
  * @retval 无
  */
void FumigateEnchance(void)
{
  static uint32_t medicine_quantity = 0;

  if (ConfigParam.WorkingMode.working_mode == ENUM_FUMIGATE_ENCHANCE_MODE)
  {
    if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_READY)
    {
      ConfigParam.WorkingStatus.working_status = ENUM_WORKING_START;
      working_step = STEP2; //
      MedicineBottleResistor();
      Calculate_Fumigate_Time();  //重新计算灭蚊的时间
      LOG("Enchance1\n");
      if (ConfigParam.Pump.is_empty == IS_EMPTY || notify_flag == 1)
      { //没药了或者 药瓶没装好
        LOG("bottle uninstall!\n");
        OpenOptionLED();
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_STOP;
        Ble_Tx_Action("SET_FUMIGATE_STOP"); //将命令返回
        if (notify_flag == 1)
        {
          Ble_Tx_Action("BOTTLE_UNINSTALL");
        }
      }
      // if (ConfigParam.FactorySetting.target_temp != ConfigParam.FactorySetting.target_temp_backup)
      // { //让目标温度值还原
      //   ConfigParam.FactorySetting.target_temp = ConfigParam.FactorySetting.target_temp_backup;
      //   osal_snv_write(ConfigParam.FactorySetting.Factory_Sett_id, FACTORY_SETT_LENGTH, &(ConfigParam.FactorySetting.evacuation_times));
      // }
      if (ConfigParam.Pump.need_evacuation == NEED_EVACU)
      { //需要排空气
        LOG("evacuation:%d\n", ConfigParam.FactorySetting.evacuation_times);
        SetPumpLoop(ConfigParam.FactorySetting.evacuation_times);
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_WAIT)
    { //
      if (second_cnt > FAN_OPEN_TIME)
      {
        second_cnt--;
      }
      if (working_step == STEP5)
      {
        // if ((second_cnt_buff - second_cnt) == 300)
        // { //过了8min
        //   ConfigParam.FactorySetting.target_temp = TARGET_TEMP - 45;
        //   osal_snv_write(ConfigParam.FactorySetting.Factory_Sett_id, FACTORY_SETT_LENGTH, &(ConfigParam.FactorySetting.evacuation_times));
        // }
        if ((second_cnt_buff - second_cnt) % PUMP_INTERVAL == 0)
        { //每1minute秒喷TIMES_PER_PUMP次
          if (medicine_quantity >= TIMES_PER_PUMP)
          {
            SetPumpLoop(TIMES_PER_PUMP); //TIMES_PER_PUMP次
            medicine_quantity -= TIMES_PER_PUMP;
          }
          else if (medicine_quantity > 0)
          {                                 //到这里说明 TIMES_PER_PUMP> medicine_quantity >0
            SetPumpLoop(medicine_quantity); //有多少喷多少
            medicine_quantity = 0;
          }
        }
        if (second_cnt <= FAN_OPEN_TIME)
        { //加热结束,但还得开启风扇30min
          ConfigParam.WorkingStatus.working_status = ENUM_FAN_WORKING;
        }
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_START)
    {
      if (working_step == STEP2)
      { //这一步可以开风机,加热
        OpenFan();
        StartHeat = 1;
        CloseRepelHeat();
        Buzzer_Beep_Three_Times(); //响三下
        working_step = STEP3;
      }
      if (working_step == STEP3)
      {                                                                            //表示??以开??油泵??
        if ((uint16_t)GetTemperature() > (ConfigParam.FactorySetting.target_temp)) //
        {                                                                          //pid整定完成,并且温度达到了???求
          switch (ConfigParam.FumigateSetting.fumi_sett_area)
          {                    //下面根据设置选项，选择对应的药剂量
          case ENUM_5_15_M2_F: //房间面积  (0-15m2)
            medicine_quantity = (15UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_15_30_M2_F: //房价面积  (15-30m2)
            medicine_quantity = (30UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_30_50_M2_F: //房间面积  (30-50m2)
            medicine_quantity = (50UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_50_80_M2_F: //房间面积  (50-80m2)
            medicine_quantity = (80UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_80_100_M2_F: //房间面积  (80-100m2)
            medicine_quantity = (100UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_100_150_M2_F: //房间面积  (100-150m2)
            medicine_quantity = (150UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          case ENUM_150_200_M2_F: //房间面积  (150-200m2)
            medicine_quantity = (200UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          default:
            medicine_quantity = (15UL * ConfigParam.FumigateSetting.fumi_house_height.dat_f) * ConfigParam.FactorySetting.timers_per10m2.dat_f / 10 + 0.9F;
            break;
          }
          float times = 2.0;

          switch (ConfigParam.Pump.type)
          {
          case NORMAL_MEDICINE:times = 2.0;break;
          case MEDIAN_MEDICINE:times = 1.5;break;
          case ENCHANCE_MEDICINE:times = 1.0;break;
          default:times = 2.0;break;
          }

          medicine_quantity = medicine_quantity * 1.2F * times + 0.9F; //增强模式是普通模式的1.2倍
          if (medicine_quantity >= TIMES_PER_PUMP)
          {
            SetPumpLoop(TIMES_PER_PUMP); //TIMES_PER_PUMP次
            medicine_quantity -= TIMES_PER_PUMP;
          }
          else if (medicine_quantity > 0)
          {                                 //到这里说明 TIMES_PER_PUMP> medicine_quantity >0
            SetPumpLoop(medicine_quantity); //有多少喷多少
            medicine_quantity = 0;
          }
          working_step = STEP4;
        }
        else
        {
          Buzzer_Beep_Three_Times(); //响三下
        }
      }
      if (working_step == STEP4)
      {                                                                               //这里设置一??定时??,用来设定什么时候停??
        second_cnt_buff = second_cnt = ConfigParam.FactorySetting.fumigate_time * 60 + FAN_OPEN_TIME; //需要工作的时间(正式的熏蒸时??)
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_WAIT;                 //进入到监??
        working_step = STEP5;
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_FAN_WORKING)
    {
      working_step = STEP_ALL; //状态???位
      StartHeat = 0;           //关闭加热


      if (second_cnt > 0)
      {
        second_cnt--;
      }
      if (second_cnt == 0)
      {
        CloseFan();
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_STOP;
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
    {
      // if (ConfigParam.FactorySetting.target_temp != ConfigParam.FactorySetting.target_temp_backup)
      // { //让目标温度值还原
      //   ConfigParam.FactorySetting.target_temp = ConfigParam.FactorySetting.target_temp_backup;
      //   osal_snv_write(ConfigParam.FactorySetting.Factory_Sett_id, FACTORY_SETT_LENGTH, &(ConfigParam.FactorySetting.evacuation_times));
      // }
      second_cnt = 0;          //计时时间归零
      working_step = STEP_ALL; //状态???位
      StartHeat = 0;           //关闭加热
      CloseFan();
    }
    RunIndicate();
  }
}

uint32_t minute_cnt = 0;
uint32_t minute = 0;

void RepelAirCondition(void)
{
  if (ConfigParam.WorkingMode.working_mode == ENUM_REPEL_AIR_CONDI_MODE)
  {
    if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_READY)
    {
      OpenRepelHeat();
      CloseFan();
      CloseFumigateHeat();
      StartHeat = 0;           //关闭加热
      second_cnt = ConfigParam.FactorySetting.InitRepelTime * 60;
      working_step = STEP1;
      ConfigParam.WorkingStatus.working_status = ENUM_WORKING_WAIT;
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_WAIT)
    {
      if (second_cnt > 0)
      {
        second_cnt--;
      }
      if (working_step == STEP1)
      {
        if (second_cnt == 0)
        {
          ConfigParam.WorkingStatus.working_status = ENUM_WORKING_START;
          working_step = STEP2; //
        }
        if (ConfigParam.RepelSetting.total_repel_time > 0)
        {                                              //如果还有驱蚊药剂的话
          ConfigParam.RepelSetting.total_repel_time--; //每隔1s药剂总时间,得减1
          minute = minute_cnt - ConfigParam.RepelSetting.total_repel_time;
          if (ConfigParam.RepelSetting.total_repel_time % 1800 == 0)
          { //每个30min对药剂时间进行存储
            osal_snv_write(ConfigParam.RepelSetting.repel_sett_id, REPEL_SETT_LENGTH, &(ConfigParam.RepelSetting.repel_sett_area));
          }
        }
        else
        { //这里是驱蚊药水用完了的情况
          ConfigParam.WorkingStatus.working_status = ENUM_WORKING_STOP;
          OpenOptionLED();
        }
      }
      if (working_step == STEP3)
      { //在这里等待驱蚊开??(这???时间里驱蚊??关闭??)
        if (second_cnt == 0)
        { //再???开??驱蚊
          ConfigParam.WorkingStatus.working_status = ENUM_WORKING_START;
          working_step = STEP4;
        }
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_START)
    { //
      if (working_step == STEP2)
      { //关闭驱蚊加热
        switch (ConfigParam.FumigateSetting.fumi_sett_area)
        {
        case ENUM_30_50_M2_F:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.air_condi_close_time * 60UL / 100;
          break;
        case ENUM_15_30_M2_F:
          second_cnt = ConfigParam.FactorySetting.area15_30m2_factory * ConfigParam.FactorySetting.air_condi_close_time * 60UL / 100;
          break;
        case ENUM_5_15_M2_F:
          second_cnt = ConfigParam.FactorySetting.area5_15m2_factory * ConfigParam.FactorySetting.air_condi_close_time * 60UL / 100;
          break;
        default:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.air_condi_close_time * 60UL / 100;
          break;
        }
        CloseRepelHeat();
        CloseFan();
        CloseFumigateHeat();
        StartHeat = 0;           //关闭加热
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_WAIT;
        working_step = STEP3;
      }
      if (working_step == STEP4)
      { //开驱蚊加热
        switch (ConfigParam.FumigateSetting.fumi_sett_area)
        {
        case ENUM_30_50_M2_F:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.air_condi_open_time * 60UL / 100;
          break;
        case ENUM_15_30_M2_F:
          second_cnt = ConfigParam.FactorySetting.area15_30m2_factory * ConfigParam.FactorySetting.air_condi_open_time * 60UL / 100;
          break;
        case ENUM_5_15_M2_F:
          second_cnt = ConfigParam.FactorySetting.area5_15m2_factory * ConfigParam.FactorySetting.air_condi_open_time * 60UL / 100;
          break;
        default:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.air_condi_open_time * 60UL / 100;
          break;
        }
        OpenRepelHeat();
        CloseFan();
        CloseFumigateHeat();
        StartHeat = 0;           //关闭加热
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_WAIT;
        working_step = STEP1;
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
    {
      second_cnt = 0;          //计时时间??
      working_step = STEP_ALL; //状态???位
      CloseRepelHeat();
      CloseFan();
      CloseFumigateHeat();
      StartHeat = 0;           //关闭加热
    }
    RunIndicate();
  }
}

/**
  * @brief  母  
  * @param  无
  * @retval 无
  */
void RepelMaternal(void)
{
  if (ConfigParam.WorkingMode.working_mode == ENUM_REPEL_MATERNAL_MODE)
  {
    if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_READY)
    {
      OpenRepelHeat();
      CloseFan();
      CloseFumigateHeat();
      StartHeat = 0;           //关闭加热
      second_cnt = ConfigParam.FactorySetting.InitRepelTime * 60;
      working_step = STEP1;
      ConfigParam.WorkingStatus.working_status = ENUM_WORKING_WAIT;
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_WAIT)
    {
      if (second_cnt > 0)
      {
        second_cnt--;
      }
      if (working_step == STEP1)
      {
        if (second_cnt == 0)
        {
          ConfigParam.WorkingStatus.working_status = ENUM_WORKING_START;
          working_step = STEP2; //
        }
        if (ConfigParam.RepelSetting.total_repel_time > 0)
        {                                              //如果还有驱蚊药剂的话
          ConfigParam.RepelSetting.total_repel_time--; //每隔1s药剂总时间,得减1
          minute = minute_cnt - ConfigParam.RepelSetting.total_repel_time;
          if (ConfigParam.RepelSetting.total_repel_time % 1800 == 0)
          { //每个30min对药剂时间进行存储
            osal_snv_write(ConfigParam.RepelSetting.repel_sett_id, REPEL_SETT_LENGTH, &(ConfigParam.RepelSetting.repel_sett_area));
          }
        }
        else
        { //这里是驱蚊药水用完了的情况
          ConfigParam.WorkingStatus.working_status = ENUM_WORKING_STOP;
          OpenOptionLED();
        }
      }
      if (working_step == STEP3)
      {
        if (second_cnt == 0)
        {
          ConfigParam.WorkingStatus.working_status = ENUM_WORKING_START;
          working_step = STEP4;
        }
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_START)
    {
      if (working_step == STEP2)
      { //关加热时间
        switch (ConfigParam.FumigateSetting.fumi_sett_area)
        { //根据当前的面??来???算具体的驱蚊开关时??(这里表示关闭的时??)  这里的智能???测没有做
        case ENUM_30_50_M2_F:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.maternal_close_time * 60UL / 100;
          break;
        case ENUM_15_30_M2_F:
          second_cnt = ConfigParam.FactorySetting.area15_30m2_factory * ConfigParam.FactorySetting.maternal_close_time * 60UL / 100;
          break;
        case ENUM_5_15_M2_F:
          second_cnt = ConfigParam.FactorySetting.area5_15m2_factory * ConfigParam.FactorySetting.maternal_close_time * 60UL / 100;
          break;
        default:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.maternal_close_time * 60UL / 100;
          break;
        }
        CloseRepelHeat();
        CloseFan();
        CloseFumigateHeat();
        StartHeat = 0;           //关闭加热
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_WAIT;
        working_step = STEP3;
      }
      if (working_step == STEP4)
      { ////开加热时间
        switch (ConfigParam.FumigateSetting.fumi_sett_area)
        {
        case ENUM_30_50_M2_F:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.maternal_open_time * 60UL / 100;
          break;
        case ENUM_15_30_M2_F:
          second_cnt = ConfigParam.FactorySetting.area15_30m2_factory * ConfigParam.FactorySetting.maternal_open_time * 60UL / 100;
          break;
        case ENUM_5_15_M2_F:
          second_cnt = ConfigParam.FactorySetting.area5_15m2_factory * ConfigParam.FactorySetting.maternal_open_time * 60UL / 100;
          break;
        default:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.maternal_open_time * 60UL / 100;
          break;
        }
        OpenRepelHeat();      //   
        CloseFan();
        CloseFumigateHeat();
        StartHeat = 0;           //关闭加热
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_WAIT;
        working_step = STEP1;
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
    {
      second_cnt = 0;          //计时时间归零
      working_step = STEP_ALL; //状态???位
      CloseRepelHeat();
      CloseFan();
      CloseFumigateHeat();
      StartHeat = 0;           //关闭加热
    }
    RunIndicate();
  }
}

/**
  * @brief  增  
  * @param  无
  * @retval 无
  */
void RepelEnchance(void)
{
  if (ConfigParam.WorkingMode.working_mode == ENUM_REPEL_ENCHANCE_MODE)
  {
    if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_READY)
    {
      OpenRepelHeat();      //   
      CloseFan();
      CloseFumigateHeat();
      StartHeat = 0;           //关闭加热                                     
      if (ConfigParam.RepelSetting.total_repel_time > 0)
      {  //如果还有驱蚊药剂的话
        ConfigParam.RepelSetting.total_repel_time--; //每隔1s药剂总时间,得减1
        minute = minute_cnt - ConfigParam.RepelSetting.total_repel_time;
        if (ConfigParam.RepelSetting.total_repel_time % 1800 == 0)
        { //每个隔30min对药剂时间进行存储  30*60=1800s
          osal_snv_write(ConfigParam.RepelSetting.repel_sett_id, REPEL_SETT_LENGTH, &(ConfigParam.RepelSetting.repel_sett_area));
        }
      }
      else    //这里是total_repel_time小于等于0的情况下
      { //这里是驱蚊药水用完了的情况
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_STOP;
        OpenOptionLED();
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
    {
      second_cnt = 0;          //计时时间归零
      working_step = STEP_ALL; //状态???位
      CloseRepelHeat();
      CloseFan();
      CloseFumigateHeat();
      StartHeat = 0;           //关闭加热
    }
    RunIndicate();
  }
}

/**
  * @brief  主  
  * @param  无
  * @retval 无
  */
void RepelAroma(void)
{
  if (ConfigParam.WorkingMode.working_mode == ENUM_REPEL_AROMA_MODE)
  {
    if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_READY)
    {
      OpenRepelHeat();
      CloseFan();
      CloseFumigateHeat();
      StartHeat = 0;           //关闭加热
      second_cnt = ConfigParam.FactorySetting.InitRepelTime * 60;  //这里按秒来算
      working_step = STEP1;
      ConfigParam.WorkingStatus.working_status = ENUM_WORKING_WAIT;
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_WAIT)
    {
      if (second_cnt > 0)
      {
        second_cnt--;
      }
      if (working_step == STEP1)
      {
        if (second_cnt == 0)
        {
          ConfigParam.WorkingStatus.working_status = ENUM_WORKING_START;
          working_step = STEP2;
        }
        if (ConfigParam.RepelSetting.total_repel_time > 0)
        {                                              //如果还有驱蚊药剂的话
          ConfigParam.RepelSetting.total_repel_time--; //每隔1s药剂总时间,得减1
          minute = minute_cnt - ConfigParam.RepelSetting.total_repel_time;
          if (ConfigParam.RepelSetting.total_repel_time % 1800 == 0)
          { //每个30min对药剂时间进行存储
            osal_snv_write(ConfigParam.RepelSetting.repel_sett_id, REPEL_SETT_LENGTH, &(ConfigParam.RepelSetting.repel_sett_area));
          }
        }
        else
        { //这里是驱蚊药水用完了的情况
          ConfigParam.WorkingStatus.working_status = ENUM_WORKING_STOP;
          OpenOptionLED();
        }
      }
      if (working_step == STEP3)
      {
        if (second_cnt == 0)
        {
          ConfigParam.WorkingStatus.working_status = ENUM_WORKING_START;
          working_step = STEP4;
        }
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_START)
    {
      if (working_step == STEP2)
      { //表示驱蚊关的时间
        switch (ConfigParam.FumigateSetting.fumi_sett_area)
        {
        case ENUM_30_50_M2_F:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.aroma_close_time * 60UL / 100;
          break;
        case ENUM_15_30_M2_F:
          second_cnt = ConfigParam.FactorySetting.area15_30m2_factory * ConfigParam.FactorySetting.aroma_close_time * 60UL / 100;
          break;
        case ENUM_5_15_M2_F:
          second_cnt = ConfigParam.FactorySetting.area5_15m2_factory * ConfigParam.FactorySetting.aroma_close_time * 60UL / 100;
          break;
        default:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.aroma_close_time * 60UL / 100;
          break;
        }
        CloseRepelHeat();
        CloseFan();
        CloseFumigateHeat();
        StartHeat = 0;           //关闭加热
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_WAIT;
        working_step = STEP3;
      }
      if (working_step == STEP4)
      { //表示驱蚊开的时间
        switch (ConfigParam.FumigateSetting.fumi_sett_area)
        {
        case ENUM_30_50_M2_F:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.aroma_open_time * 60UL / 100;
          break;
        case ENUM_15_30_M2_F:
          second_cnt = ConfigParam.FactorySetting.area15_30m2_factory * ConfigParam.FactorySetting.aroma_open_time * 60UL / 100;
          break;
        case ENUM_5_15_M2_F:
          second_cnt = ConfigParam.FactorySetting.area5_15m2_factory * ConfigParam.FactorySetting.aroma_open_time * 60UL / 100;
          break;
        default:
          second_cnt = ConfigParam.FactorySetting.area30_50m2_factory * ConfigParam.FactorySetting.aroma_open_time * 60UL / 100;
          break;
        }
        OpenRepelHeat();
        CloseFan();
        CloseFumigateHeat();
        StartHeat = 0;           //关闭加热
        ConfigParam.WorkingStatus.working_status = ENUM_WORKING_WAIT;
        working_step = STEP1;
      }
    }
    else if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
    {
      second_cnt = 0;          //计时时间归零
      working_step = STEP_ALL; //状态???位
      CloseRepelHeat();
      CloseFan();
      CloseFumigateHeat();
      StartHeat = 0;           //关闭加热
    }
    RunIndicate();
  }
}

/**
  * @brief  根  
  * @param  无
  * @retval 无
  */
void ReportRepelAromaTime(void)
{
  static uint16_t cnt = 0;

  if (ConfigParam.WorkingMode.working_mode == ENUM_REPEL_AIR_CONDI_MODE
    || ConfigParam.WorkingMode.working_mode == ENUM_REPEL_MATERNAL_MODE
    || ConfigParam.WorkingMode.working_mode == ENUM_REPEL_ENCHANCE_MODE
    )
  {
    if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
    {
      cnt = 0;
      minute = 0;
    }
    else
    {
      if (cnt % 120 == 0)
      { //2s上报一次当前已工作的时间
        Repel_time_send();
      }
      cnt++;
    }
  }
  else if (ConfigParam.WorkingMode.working_mode == ENUM_REPEL_AROMA_MODE)
  {
    if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
    {
      cnt = 0;
      minute = 0;
    }
    else
    {
      if (cnt % 120 == 0)
      { //2s上报一次当前已工作的时间
        Aroma_time_send();
      }
      cnt++;
    }
  }
}



static void ControlAction(void)
{
  switch (ConfigParam.WorkingMode.working_mode)
  {
  case ENUM_FUMIGATE_NORMAL_MODE:
    FumigateNormal();
    break;
  case ENUM_FUMIGATE_ENCHANCE_MODE:
    FumigateEnchance();
    break;
  case ENUM_REPEL_AIR_CONDI_MODE:
    RepelAirCondition();
    ReportRepelAromaTime();
    break;
  case ENUM_REPEL_MATERNAL_MODE:
    RepelMaternal();
    ReportRepelAromaTime();
    break;
  case ENUM_REPEL_ENCHANCE_MODE:
    RepelEnchance();
    ReportRepelAromaTime();
    break;
  case ENUM_REPEL_AROMA_MODE:
    RepelAroma();
    ReportRepelAromaTime();
    break;
  default:
    break;
  }
  CalculateMedicineQuantity();
}

/**
 * @brief 控制线程初始化
 * @param task_id 线程的id号
 * @retval 无
 */
void Control_ProcessInit(uint8 task_id)
{
  Control_TaskID = task_id;
  LOG("Control task start...\n");
  Destroy_GPIO_Init();
  PUMP_Control_GPIO_Init();
  InitHC164();
  Change_Mode_LED();
  osal_start_reload_timer(Control_TaskID, CONTROL_CYCLE_TIMER, 1000); //控制线程每1s干一次
}


static void BUP_ProcessOSALMsg(osal_event_hdr_t* pMsg)
{
  switch (pMsg->event)
  {
  default:
    // do nothing
    break;
  }
}

/**
 * @brief 控制线程
 * @param task_id 线程的id号
 * @param events 任务的事件参数
 * @retval 我也不知道返回啥??
 */
uint16 Control_ProcessEvent(uint8 task_id, uint16 events)
{
  VOID task_id; // OSAL required parameter that isn't used in this function

  if (events & SYS_EVENT_MSG)
  {
    uint8* pMsg;

    if ((pMsg = osal_msg_receive(Control_TaskID)) != NULL)
    {
      BUP_ProcessOSALMsg((osal_event_hdr_t*)pMsg);
      // Release the OSAL message
      VOID osal_msg_deallocate(pMsg);
    }
    // return unprocessed events
    return (events ^ SYS_EVENT_MSG);
  }
  if (events & CONTROL_CYCLE_TIMER)
  {
    ControlAction();
    HeatControl();
    return (events ^ CONTROL_CYCLE_TIMER);
  }
  return 0;
}