#ifndef __CONTROL_H__
#define __CONTROL_H__

#include "types.h"

typedef enum
{
    STEP1,
    STEP2,
    STEP3,
    STEP4,
    STEP5,
    STEP6,
    STEP7,
    STEP8,
    STEP_ALL,
}WORKING_STEP_Typedef;    //工作步骤结构体

//这里是泵控制管脚
#define PUMP_CONTROL    P20     //泵控制GPIO
#define DESTROY_CONTROL P18     //自毁GPIO
/*****************************************************************************************************/
#define CONTROL_CYCLE_TIMER     0x0001

void RemoteControl_Repel_Start(void);
void RemoteControl_Repel_Stop(void);

void RemoteControl_Fumigate_Start(void);
void RemoteControl_Fumigate_Stop(void);

void RemoteControl_Aroma_Start(void);
void RemoteControl_Aroma_Stop(void);

void MedicineBottleResistor(void);

void AromaFunction(void);
void RepelEnchanceFunction(void);
void MaternalFunction(void);
void AirConditionerFunction(void);

void MosFumigateNormalFunction(void);
void FumigateEnchanceFunction(void);


void Set_Fumigate_Area5_15m2(void);
void Set_Fumigate_Area15_30m2(void);
void Set_Fumigate_Area30_50m2(void);
void Set_Fumigate_Area50_80m2(void);
void Set_Fumigate_Area80_100m2(void);
void Set_Fumigate_Area100_150m2(void);
void Set_Fumigate_Area150_200m2(void);


// void Set_Repel_Area5_15m2(void);
// void Set_Repel_Area15_30m2(void);
// void Set_Repel_Area30_50m2(void);

void Fumigate_Area_Indicate(void);
// void Repel_Area_Indicate(void);

void KeyAction(uint8_t keycode);
void SetPumpLoop(uint32_t LoopCnt);
void Control_ProcessInit( uint8 task_id );
uint16 Control_ProcessEvent( uint8 task_id, uint16 events);

extern uint8_t adc_sample_done;
extern uint32_t medicine_quant;
extern uint32_t minute;
extern uint32_t minute_cnt;
extern uint8_t notify_flag; //提示??瓶没有安装好标志

#endif 