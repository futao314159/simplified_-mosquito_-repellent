#include "board.h"

// The order in this table must be identical to the task initialization calls below in osalInitTask.
const pTaskEventHandlerFn tasksArr[] =
{
    LL_ProcessEvent,                                                  // task 0
    HCI_ProcessEvent,                                                 // task 1
    L2CAP_ProcessEvent,                                               // task 2
    GAP_ProcessEvent,                                                 // task 3
    GATT_ProcessEvent,                                                // task 4
    SM_ProcessEvent,                                                  // task 5
    GAPRole_ProcessEvent,                                             // task 6
  //GAPBondMgr_ProcessEvent,                                         //chendy add for bonding test
    GATTServApp_ProcessEvent,                                         // task 7
    ble_ProcessEvent,                                             // task 8
    Control_ProcessEvent,                                             // task 9
    Key_ProcessEvent,                                                 // task 10
    Sensor_ProcessEvent,                                              // task 11
    Buzzer_ProcessEvent,                                              // task 12
    // Watchdog_ProcessEvent,                                            // task 13
};

const uint8 tasksCnt = sizeof(tasksArr) / sizeof(tasksArr[0]);
uint16* tasksEvents;


/*********************************************************************
 * @fn      osalInitTasks
 *
 * @brief   This function invokes the initialization function for each task.
 *
 * @param   void
 *
 * @return  none
 */
void osalInitTasks(void)
{
  uint8 taskID = 0;

  tasksEvents = (uint16*)osal_mem_alloc(sizeof(uint16) * tasksCnt);
  osal_memset(tasksEvents, 0, (sizeof(uint16) * tasksCnt));

  LL_Init(taskID++);
  HCI_Init(taskID++);
  L2CAP_Init(taskID++);
  GAP_Init(taskID++);
  GATT_Init(taskID++);
  SM_Init(taskID++);
  GAPRole_Init(taskID++);
  GATTServApp_Init(taskID++);

  /* Application */
  ble_Init(taskID++);
  Control_ProcessInit(taskID++);
  Key_ProcessInit(taskID++);
  Sensor_ProcessInit(taskID++);
  Buzzer_ProcessInit(taskID++);
  // Watchdog_Init(taskID);   //这里的看门狗是2000ms,然后每隔1000ms就会喂一次看门狗
}

/*********************************************************************
*********************************************************************/
