#include "board.h"
/**************************************************************************************************
  Filename:       bleuart.c
  Revised:        
  Revision:       

  Description:    This file contains the ble uart rawpass application
                  

**************************************************************************************************/




/*********************************************************************
 * MACROS
 */

// #define AT_UART   1

/*********************************************************************
 * CONSTANTS
 */

// How often to perform periodic event
#define BUP_PERIODIC_EVT_PERIOD                   5000

#define DEVINFO_SYSTEM_ID_LEN             8
#define DEVINFO_SYSTEM_ID                 0
 

#define DEFAULT_DISCOVERABLE_MODE             GAP_ADTYPE_FLAGS_GENERAL

// Minimum connection interval (units of 1.25ms, 80=100ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     8

// Maximum connection interval (units of 1.25ms, 800=1000ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     10

// Slave latency to use if automatic parameter update request is enabled
#define DEFAULT_DESIRED_SLAVE_LATENCY         0

// Supervision timeout value (units of 10ms, 1000=10s) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_CONN_TIMEOUT          600

// Whether to enable automatic parameter update request when a connection is formed
#define DEFAULT_ENABLE_UPDATE_REQUEST         TRUE

// Connection Pause Peripheral time value (in seconds)
#define DEFAULT_CONN_PAUSE_PERIPHERAL         6


// Length of bd addr as a string
#define B_ADDR_STR_LEN                        15

//add bonding chendy 20200605

// Default passcode
#define DEFAULT_PASSCODE                      0

// Default GAP pairing mode
//#define DEFAULT_PAIRING_MODE                  GAPBOND_PAIRING_MODE_INITIATE
#define DEFAULT_PAIRING_MODE                  GAPBOND_PAIRING_MODE_WAIT_FOR_REQ

// Default MITM mode (TRUE to require passcode or OOB when pairing)
#define DEFAULT_MITM_MODE                     TRUE

// Default bonding mode, TRUE to bond
#define DEFAULT_BONDING_MODE                  TRUE

// Default GAP bonding I/O capabilities
#define DEFAULT_IO_CAPABILITIES               GAPBOND_IO_CAP_NO_INPUT_NO_OUTPUT//GAPBOND_IO_CAP_DISPLAY_ONLY // GAPBOND_IO_CAP_NO_INPUT_NO_OUTPUT

//广播间隔
uint16 advInt[6] ={80,160,320,800,1600,3200};   // actual time = advInt * 625us				广播间隔
uint8  advint = 2;								//默认广播间隔200ms



uint8 bleuart_TaskID;   // Task ID for internal task/event processing

uint16 gapConnHandle;

static gaprole_States_t gapProfileState = GAPROLE_INIT;

// GAP - SCAN RSP data (max size = 31 bytes)
static uint8 scanRspData[] =
{
    // complete name
    21,   // length of this data    出去它本身
    GAP_ADTYPE_LOCAL_NAME_COMPLETE,
    'M','o','s','q','u','i','t','o','_','R',
	'e','p','e','l','l','e','n','t',' ',' ',

// connection interval range
//    0x05,   // length of this data
//    GAP_ADTYPE_SLAVE_CONN_INTERVAL_RANGE,
//    LO_UINT16( DEFAULT_DESIRED_MIN_CONN_INTERVAL ),   // 100ms
//    HI_UINT16( DEFAULT_DESIRED_MIN_CONN_INTERVAL ),
//    LO_UINT16( DEFAULT_DESIRED_MAX_CONN_INTERVAL ),   // 1s
//    HI_UINT16( DEFAULT_DESIRED_MAX_CONN_INTERVAL ),

    // Tx power level
//    0x02,   // length of this data
//    GAP_ADTYPE_POWER_LEVEL,
//    5       // 0dBm
};



// advert data for bleuart
static uint8 advertData[] =
{	
    0x02,   // length of this data
    GAP_ADTYPE_FLAGS,
    DEFAULT_DISCOVERABLE_MODE | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,
	
    0x03,
    0x03,											//Complete list of 16-bit UUIDs
    0xf0, 0xff,
	
	0x09,
	0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,		//reserved data
};


// GAP GATT Attributes
static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "Uart-Test           ";

uint8*scanR=scanRspData;
uint8*advertdata=advertData;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void bleuart_StateNotificationCB( gaprole_States_t newState );

/*********************************************************************
 * PROFILE CALLBACKS
 */

// GAP Role Callbacks
static gapRolesCBs_t bleuart_PeripheralCBs =
{
    bleuart_StateNotificationCB,  // Profile State Change Callbacks
    NULL                            // When a valid RSSI is read from controller (not used by application)
};


static void BleUart_ProcessPasscodeCB(uint8 *deviceAddr, uint16 connectionHandle, uint8 uiInputs, uint8 uiOutputs);
static void BleUart_ProcessPairStateCB(uint16 connHandle, uint8 state, uint8 status);

static gapBondCBs_t BleUart_BondMgrCBs =
{
  (pfnPasscodeCB_t) BleUart_ProcessPasscodeCB, // Passcode callback
   BleUart_ProcessPairStateCB                   // Pairing / Bonding state Callback
};

static void BleUart_ProcessPasscodeCB(uint8 *deviceAddr, uint16 connectionHandle, uint8 uiInputs, uint8 uiOutputs)
{
	uint32 passcode; 
// uint8 str[7]; 
	
	//LOG("Entry passcode process\n");
 // create pass code
 #if 0 
  LL_Rand(((uint8 *) &passcode), sizeof( uint32 )); 
passcode %= 1000000; 
#else 
  passcode = 456890; 
  #endif 
//LOG("passcode is=%d\n",passcode);	
  //display passcode or log passcode
 if(uiOutputs != 0) 
  { 
   //LOG("passcode is=%d\n",passcode); 
  } 
  //pass code rsp
GAPBondMgr_PasscodeRsp(connectionHandle, SUCCESS, passcode); 
} 



static void BleUart_ProcessPairStateCB(uint16 connHandle, uint8 state, uint8 status)
{
	

  //paring state 
  if(state == GAPBOND_PAIRING_STATE_STARTED) 
  { 
  //LOG("Pairing started"); 
 
  } 
   //
  else if(state == GAPBOND_PAIRING_STATE_COMPLETE) 
  { 
  //
  if(status == SUCCESS) 
  { 
  //LOG("Pairing success"); 
  
  } 
  //
  else if(status == SMP_PAIRING_FAILED_UNSPECIFIED) 
  { 
  //LOG("Paired device"); 

  } 
  //
  else 
  { 
  //LOG("Pairing fail"); 
 
  } 
 
  } 
  else if (state == GAPBOND_PAIRING_STATE_BONDED) 
  { 
   if (status == SUCCESS) 
   { 
      //LOG("Bonding success"); 
   } 
  } 

}	
/*********************************************************************
 * PUBLIC FUNCTIONS     
 */

/**
 * @brief   这里表示蓝牙接受到了数据
 * @param   bleuart_Evt_t 数据指针
 * @retval  
 */
void on_bleuartServiceEvt(bleuart_Evt_t* pev)
{
    //LOG("on_bleuartServiceEvt:%d\n", pev->ev);
    switch(pev->ev)
    {  //根据事件来    不能为了别人而委屈了自己
      case bleuart_EVT_TX_NOTI_DISABLED:
        BUP_disconnect_handler();
        break;
      case bleuart_EVT_TX_NOTI_ENABLED :
        BUP_connect_handler();
        osal_set_event(bleuart_TaskID,BUP_OSAL_EVT_NOTIFY_DATA);
        break;
      case bleuart_EVT_BLE_DATA_RECIEVED:
        Receive_data_from_ble( (uint8_t*)pev->data, (uint8_t)pev->param);
        //LOG("%s,%d\n",pev->data,pev->param);
        break;
      default:
        break;
    }
}

void on_BUP_Evt(BUP_Evt_t* pev)
{
    switch(pev->ev)
    {
      
    }
}

/**
 * @brief 蓝牙通讯初始化函数
 * @param  task_id 任务的id
 * @retval 无
 */
void ble_Init( uint8 task_id )
{
  bleuart_TaskID = task_id;

  // Setup the GAP  //这个不知道是干嘛的????
  VOID GAP_SetParamValue( TGAP_CONN_PAUSE_PERIPHERAL, DEFAULT_CONN_PAUSE_PERIPHERAL );
      
  // Setup the GAP Peripheral Role Profile
  {
    // device starts advertising upon initialization
    uint8 initial_advertising_enable = TRUE;
    
    uint8 enable_update_request = DEFAULT_ENABLE_UPDATE_REQUEST;
    uint8 advChnMap = GAP_ADVCHAN_37 | GAP_ADVCHAN_38 | GAP_ADVCHAN_39; 
            
    // By setting this to zero, the device will go into the waiting state after
    // being discoverable for 30.72 second, and will not being advertising again
    // until the enabler is set back to TRUE
    uint16 gapRole_AdvertOffTime = 0;
        
    uint16 desired_min_interval = DEFAULT_DESIRED_MIN_CONN_INTERVAL;
    uint16 desired_max_interval = DEFAULT_DESIRED_MAX_CONN_INTERVAL;
    uint16 desired_slave_latency = DEFAULT_DESIRED_SLAVE_LATENCY;
    uint16 desired_conn_timeout = DEFAULT_DESIRED_CONN_TIMEOUT;
        
    uint8 peerPublicAddr[] = {
      0x01,
      0x02,
      0x03,
      0x04,
      0x05,
      0x06
    };
	
/***************************************************************************************************************/

/***************************************************************************************************************/		
    GAPRole_SetParameter(GAPROLE_ADV_DIRECT_ADDR, sizeof(peerPublicAddr), peerPublicAddr);
    // set adv channel map
    GAPRole_SetParameter(GAPROLE_ADV_CHANNEL_MAP, sizeof(uint8), &advChnMap);        
    
    // Set the GAP Role Parameters
    GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &initial_advertising_enable );
    GAPRole_SetParameter( GAPROLE_ADVERT_OFF_TIME, sizeof( uint16 ), &gapRole_AdvertOffTime );
    
    GAPRole_SetParameter( GAPROLE_SCAN_RSP_DATA, sizeof ( scanRspData ), scanRspData );
    GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertData ), advertData );
    
    GAPRole_SetParameter( GAPROLE_PARAM_UPDATE_ENABLE, sizeof( uint8 ), &enable_update_request );
    GAPRole_SetParameter( GAPROLE_MIN_CONN_INTERVAL, sizeof( uint16 ), &desired_min_interval );
    GAPRole_SetParameter( GAPROLE_MAX_CONN_INTERVAL, sizeof( uint16 ), &desired_max_interval );
    GAPRole_SetParameter( GAPROLE_SLAVE_LATENCY, sizeof( uint16 ), &desired_slave_latency );
    GAPRole_SetParameter( GAPROLE_TIMEOUT_MULTIPLIER, sizeof( uint16 ), &desired_conn_timeout );
  }
    
  // Set the GAP Characteristics
  osal_memcpy(attDeviceName,&scanRspData[2],20);
  GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, attDeviceName );
    
  // Set advertising interval
  {
    GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MIN, advInt[advint] );
    GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MAX, advInt[advint] );
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, advInt[advint] );
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, advInt[advint] );
  }

  bleuart_AddService(on_bleuartServiceEvt);		//这个主要是对蓝牙接收到的数据进行处理
	
  // Setup a delayed profile startup
  osal_set_event( bleuart_TaskID, BUP_OSAL_EVT_START_DEVICE );  //到时候bleuart_ProcessEvent函数会执行
}


static void BUP_ProcessOSALMsg( osal_event_hdr_t* pMsg )
{
    switch ( pMsg->event )
    {
		default:
        // do nothing
        break;
    }
}

/*********************************************************************
 * @fn      SimpleBLEPeripheral_ProcessEvent
 *
 * @brief   Simple BLE Peripheral Application Task event processor.  This function
 *          is called to process all events for the task.  Events
 *          include timers, messages and any other user defined events.
 *
 * @param   task_id  - The OSAL assigned task ID.
 * @param   events - events to process.  This is a bit map and can
 *                   contain more than one event.
 *
 * @return  events not processed
 */
uint16 ble_ProcessEvent( uint8 task_id, uint16 events )
{
    VOID task_id; // OSAL required parameter that isn't used in this function
    
    if (events & SYS_EVENT_MSG)
    {
      uint8* pMsg;

      if ((pMsg = osal_msg_receive(bleuart_TaskID)) != NULL)
      {
        BUP_ProcessOSALMsg((osal_event_hdr_t*)pMsg);
        // Release the OSAL message
        VOID osal_msg_deallocate(pMsg);
      }
      // return unprocessed events
      return (events ^ SYS_EVENT_MSG);
    }

    if ( events & BUP_OSAL_EVT_START_DEVICE )
    {
      // Start the Device
      VOID GAPRole_StartDevice( &bleuart_PeripheralCBs );
      
      return ( events ^ BUP_OSAL_EVT_START_DEVICE );
    }

    // enable adv
    if ( events & BUP_OSAL_EVT_RESET_ADV )
    {
      uint8 initial_advertising_enable = TRUE;
      
      GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &initial_advertising_enable );	
      
      return ( events ^ BUP_OSAL_EVT_RESET_ADV );
    }
    if(events & BUP_OSAL_EVT_NOTIFY_DATA)
    {
      //LOG("BUP_OSAL_EVT_NOTIFY_DATA\n");
      return ( events ^ BUP_OSAL_EVT_NOTIFY_DATA );
    }

    if( events & BUP_OSAL_EVT_BLE_TIMER)
    {
      //LOG("Ble_Rx_Action !\n");
      Ble_Rx_Action();    
      return ( events ^ BUP_OSAL_EVT_BLE_TIMER );
    }
    
    if( events & BUP_OSAL_EVT_UART_TO_TIMER)
    { //这个就是将数据从一个地方复制到另一个地方
      //LOG("Transfer_data_to_ble_Txbuffer !\n");
      Transfer_data_to_ble_Txbuffer();  //这里最终会触发蓝牙发送函数
      return ( events ^ BUP_OSAL_EVT_UART_TO_TIMER);
    }  
    if( events & BUP_OSAL_EVT_UARTRX_TIMER)
    { //这个就是蓝牙发送函数
      //LOG("Ble_data_send !\n");
      Ble_data_send();
      return ( events ^ BUP_OSAL_EVT_UARTRX_TIMER );
    }
    if( events & BUP_OSAL_EVT_AT)
    {
      //LOG("Hello\n");
      return ( events ^ BUP_OSAL_EVT_AT);
    }
    return 0;
}




/*********************************************************************
 * @fn      peripheralStateNotificationCB
 *
 * @brief   Notification from the profile of a state change.
 *
 * @param   newState - new state
 *
 * @return  none
 */
static void bleuart_StateNotificationCB( gaprole_States_t newState )
{
    switch ( newState )
    {
        case GAPROLE_STARTED:
        {
            uint8 ownAddress[B_ADDR_LEN];
            uint8 systemId[DEVINFO_SYSTEM_ID_LEN];
        
            GAPRole_GetParameter(GAPROLE_BD_ADDR, ownAddress);
            //LOG("MAC ADDR:\n");
            for (uint8 i = 0; i < B_ADDR_LEN; i++)
            {
              //LOG("%x ", ownAddress[B_ADDR_LEN - 1 - i]);
            }
            //LOG("\n");
            // use 6 bytes of device address for 8 bytes of system ID value
            systemId[0] = ownAddress[0];
            systemId[1] = ownAddress[1];
            systemId[2] = ownAddress[2];
        
            // set middle bytes to zero
            systemId[4] = 0x00;
            systemId[3] = 0x00;
        
            // shift three bytes up
            systemId[7] = ownAddress[5];
            systemId[6] = ownAddress[4];
            systemId[5] = ownAddress[3];
        
            DevInfo_SetParameter(DEVINFO_SYSTEM_ID, DEVINFO_SYSTEM_ID_LEN, systemId);
        }
        break;

        case GAPROLE_ADVERTISING:
          BUP_disconnect_handler();
				  //hal_gpio_write(UART_INDICATE_LED,0);   //这里可以发挥一下
          LOG("advertising!\n");
          break;

        case GAPROLE_CONNECTED:
          GAPRole_GetParameter( GAPROLE_CONNHANDLE, &gapConnHandle );
				  //hal_gpio_write(UART_INDICATE_LED,1);      //这里可以发挥一下
          LOG("connected handle[%d]!\n", gapConnHandle);
          break;
        
        case GAPROLE_CONNECTED_ADV:
            break;      
        case GAPROLE_WAITING:
            break;
        
        case GAPROLE_WAITING_AFTER_TIMEOUT:
            break;
        
        case GAPROLE_ERROR:
            break;
        
        default:
            break;        
    }  
    gapProfileState = newState;
     
    VOID gapProfileState;     
}


uint16_t ble_conn_interval(void)
{
    uint16_t interval, latency;
    GAPRole_GetParameter(GAPROLE_CONNECTION_INTERVAL, &interval);
    GAPRole_GetParameter(GAPROLE_CONNECTION_LATENCY, &latency);
    return ((1+latency)*interval*5/4);
}


/*********************************************************************
*********************************************************************/
