#include "board.h"


//这里是命令枚举！！
BLE_TX_CMD ble_tx_command;    //根据指令,向蓝牙APP返回相应的数据字符串


BLE_Data_Buffer_Typedef Ble_Buffer;  //这里是蓝牙通讯数据缓冲区

/**
 * @brief  触发函数,将数据从发送缓冲区,搬运至蓝牙发送区
 * @param  无
 * @retval 无
 */
static void uartrx_timeout_timer_start(void)
{
  osal_start_timerEx(bleuart_TaskID, BUP_OSAL_EVT_UART_TO_TIMER, 50);
}

/**
 * @brief  触发函数,将数据从发送缓冲区,搬运至蓝牙发送区
 * @param  无
 * @retval 无
 */
static void uartrx_timeout_timer_stop(void)
{
  osal_stop_timerEx(bleuart_TaskID, BUP_OSAL_EVT_UART_TO_TIMER);
}

/**
  * @brief  将字符串中连续的数字串转换成数字
  * @param  p 指向待钻换的数字串
  * @retval 转换完成的数字
  */
static int skip_atoi(const char *s)
{
  register int i = 0;
  while (((unsigned)((*s) - '0') < 10))
  {
    i = i * 10 + *(s++) - '0';
  }
  return i;
}

/**
  * @brief  向APP发送当前是什么状态   那个定时驱蚊得改一改
  * @param  无
  * @retval 无
  */
static void State_send(void)
{
  switch (ConfigParam.WorkingMode.working_mode)
  {
	case ENUM_FUMIGATE_NORMAL_MODE:
	case ENUM_FUMIGATE_ENCHANCE_MODE:
		if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
		{
			Ble_Tx_Action("SET_FUMIGATE_STOP");
		}
		else
		{
			Ble_Tx_Action("SET_FUMIGATE_START");
		}
		break;
	case ENUM_REPEL_AIR_CONDI_MODE:
	case ENUM_REPEL_ENCHANCE_MODE:
	case ENUM_REPEL_MATERNAL_MODE:
		if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
		{
			Ble_Tx_Action("SET_REPEL_STOP");
		}
		else
		{
			Ble_Tx_Action("SET_REPEL_START");
		}
		break;
	case ENUM_REPEL_AROMA_MODE:
		if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
		{
			Ble_Tx_Action("SET_AROMA_STOP");
		}
		else
		{
			Ble_Tx_Action("SET_AROMA_START");
		}
		break;
  }
}

/**
  * @brief  设置驱蚊出厂设置参数
  * @param  p:指向接收到的字符串
  * @retval 无
  */
static void Repel_factory_sett_set(const char *p)				//关于驱蚊设置这部分,还得添加进去
{
	if(ConfigParam.WorkingStatus.working_status==ENUM_WORKING_STOP)
	{
		ConfigParam.FactorySetting.InitRepelTime = skip_atoi(p);
		ConfigParam.FactorySetting.air_condi_open_time = skip_atoi(p+4);
		ConfigParam.FactorySetting.air_condi_close_time = skip_atoi(p + 8);
		ConfigParam.FactorySetting.maternal_open_time = skip_atoi(p+12);
		ConfigParam.FactorySetting.maternal_close_time = skip_atoi(p + 16);
		ConfigParam.FactorySetting.aroma_open_time = skip_atoi(p+20);
		ConfigParam.FactorySetting.aroma_close_time = skip_atoi(p + 24);
		ConfigParam.FactorySetting.area5_15m2_factory = skip_atoi(p+28);
		ConfigParam.FactorySetting.area15_30m2_factory = skip_atoi(p + 32);
		ConfigParam.FactorySetting.area30_50m2_factory = skip_atoi(p + 36);
		osal_snv_write(ConfigParam.FactorySetting.Factory_Sett_id,FACTORY_SETT_LENGTH,&(ConfigParam.FactorySetting.evacuation_times));	
	}		
}

/**
  * @brief  向APP发送驱蚊出厂设置中参数
  * @param  无
  * @retval 无
  */
static void Repel_Factory_setting_send(void)
{
  	BLE_Data_Buffer_Typedef* pctx = & Ble_Buffer;

	memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
  	sprintf(pctx->ble_tx_buffer, "REPEL_FACTORY_SETT/%03d/%03d_%03d/%03d_%03d/%03d_%03d/%03d_%03d_%03d",
               ConfigParam.FactorySetting.InitRepelTime,
               ConfigParam.FactorySetting.air_condi_open_time, ConfigParam.FactorySetting.air_condi_close_time,
               ConfigParam.FactorySetting.maternal_open_time, ConfigParam.FactorySetting.maternal_close_time,
               ConfigParam.FactorySetting.aroma_open_time, ConfigParam.FactorySetting.aroma_close_time,
               ConfigParam.FactorySetting.area5_15m2_factory,ConfigParam.FactorySetting.area15_30m2_factory,ConfigParam.FactorySetting.area30_50m2_factory
               );
	pctx->ble_tx_buffer_size=osal_strlen(pctx->ble_tx_buffer)+1;
	if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
	{  //数据超过缓冲区的大小
		return;
	}		
	uartrx_timeout_timer_stop();
	uartrx_timeout_timer_start();
}

/**
  * @brief  设置灭蚊出厂设置参数
  * @param  p:指向接收到的字符串
  * @retval 无
  */
static void Fumigate_factory_sett_set(const char *p)
{
	float dat1=0;
	float dat2=0;

	if(ConfigParam.WorkingStatus.working_status==ENUM_WORKING_STOP)
	{
		ConfigParam.FactorySetting.evacuation_times = skip_atoi(p);
		if(ConfigParam.FactorySetting.target_temp_backup != skip_atoi(p+4))
		{
			ConfigParam.FactorySetting.target_temp = ConfigParam.FactorySetting.target_temp_backup=skip_atoi(p+4);
		}
		dat1 = skip_atoi(p + 8);  //整数部分
		dat2 = skip_atoi(p + 11); //小数部分
		dat1=dat1*10+dat2;
		dat1=dat1/10;
		ConfigParam.FactorySetting.timers_per10m2.dat_f=dat1;   //次数/10m2
		ConfigParam.FactorySetting.fumigate_time= skip_atoi(p+13);
		osal_snv_write(ConfigParam.FactorySetting.Factory_Sett_id,FACTORY_SETT_LENGTH,&(ConfigParam.FactorySetting.evacuation_times));
	}
}

/**
  * @brief  向APP发送灭蚊出厂设置中的参数
  * @param  无
  * @retval 无
  */
static void Fumigate_Factory_setting_send(void)
{
  	BLE_Data_Buffer_Typedef* pctx = & Ble_Buffer;

	memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
  	sprintf(pctx->ble_tx_buffer, "FUMIGATE_FACTORY_SETT/%03d/%03d_%03d/%04.1f/%03d",
                ConfigParam.FactorySetting.evacuation_times,
                ConfigParam.FactorySetting.target_temp,(uint16_t)GetTemperature(),
                ConfigParam.FactorySetting.timers_per10m2.dat_f,
                ConfigParam.FactorySetting.fumigate_time
                );
	pctx->ble_tx_buffer_size=osal_strlen(pctx->ble_tx_buffer)+1;
	if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
	{  //数据超过缓冲区的大小
		return;
	}		
	uartrx_timeout_timer_stop();
	uartrx_timeout_timer_start();
}

/**
  * @brief  向APP发送当前已经泵了多少次
  * @param  无
  * @retval 无
  */
static void Pump_Num_send(void)
{
  	BLE_Data_Buffer_Typedef* pctx = & Ble_Buffer;

	memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
  	sprintf(pctx->ble_tx_buffer, "PUMP_%03d",ConfigParam.PumpCNT.pumpcnt);
	pctx->ble_tx_buffer_size=osal_strlen(pctx->ble_tx_buffer)+1;
	if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
	{  //数据超过缓冲区的大小
		return;
	}		
	uartrx_timeout_timer_stop();
	uartrx_timeout_timer_start();
}

/**
  * @brief  向APP发送当前剩余药量
  * @param  无
  * @retval 无
  */
void Remains_send(void)
{
  	BLE_Data_Buffer_Typedef* pctx = & Ble_Buffer;

	//memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
  	sprintf(pctx->ble_tx_buffer,"REMAINS_%03d_%03d",percentage_of_repel_medicine,percentage_of_fumigate_medicine);
	pctx->ble_tx_buffer_size=osal_strlen(pctx->ble_tx_buffer)+1;
	if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
	{  //数据超过缓冲区的大小
		return;
	}		
	uartrx_timeout_timer_stop();
	uartrx_timeout_timer_start();
}

/**
  * @brief  向APP发送当前已香薰用时
  * @param  无
  * @retval 无
  */
void Aroma_time_send(void)
{
  	BLE_Data_Buffer_Typedef* pctx = & Ble_Buffer;

	//memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
  	sprintf(pctx->ble_tx_buffer,"AROMA_TIME_%04d",(uint32_t)(minute/60));
	pctx->ble_tx_buffer_size=osal_strlen(pctx->ble_tx_buffer)+1;
	if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
	{  //数据超过缓冲区的大小
		return;
	}		
	uartrx_timeout_timer_stop();
	uartrx_timeout_timer_start();
}

/**
  * @brief  向APP发送当前已驱蚊用时
  * @param  无
  * @retval 无
  */
void Repel_time_send(void)
{
  	BLE_Data_Buffer_Typedef* pctx = & Ble_Buffer;

	//memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
  	sprintf(pctx->ble_tx_buffer,"REPEL_TIME_%04d",(uint32_t)(minute/60));
	pctx->ble_tx_buffer_size=osal_strlen(pctx->ble_tx_buffer)+1;
	if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
	{  //数据超过缓冲区的大小
		return;
	}		
	uartrx_timeout_timer_stop();
	uartrx_timeout_timer_start();
}


/**
  * @brief  向APP发送当前的灭蚊设置
  * @param  无
  * @retval 无
  */
static void Fumigate_setting_send(void)
{
  	BLE_Data_Buffer_Typedef* pctx = & Ble_Buffer;
	const char *buff1;
	const char *buff2;
	const char *buff3;
	const char *buff4;

	switch (ConfigParam.WorkingMode.fumigate_buf_mode)
	{
		case ENUM_FUMIGATE_ENCHANCE_MODE: buff1="MODE_FUMIGATE_ENCHANCE";break;
		case ENUM_FUMIGATE_NORMAL_MODE: buff1="MODE_FUMIGATE_NORMAL";break;
		default: buff1="MODE_FUMIGATE_NORMAL";break;
	}
	MedicineBottleResistor(); //先从事相关工作,吸取经验
	switch (ConfigParam.Pump.type)
	{
		case NORMAL_MEDICINE: buff2="NORMAL_MEDICINE";break;
		case MEDIAN_MEDICINE: buff2="MEDIAN_MEDICINE";break;
		case ENCHANCE_MEDICINE: buff2="ENCHANCE_MEDICINE";break;
		default:buff2="NORMAL_MEDICINE";break;
	}
	switch (ConfigParam.FumigateSetting.fumi_sett_area)
	{
		case ENUM_5_15_M2_F: buff3="FUMIGATE_5_15_M2";break;
		case ENUM_15_30_M2_F: buff3="FUMIGATE_15_30_M2";break;
		case ENUM_30_50_M2_F: buff3="FUMIGATE_30_50_M2";break;
		case ENUM_50_80_M2_F: buff3="FUMIGATE_50_80_M2";break;
		case ENUM_80_100_M2_F: buff3="FUMIGATE_80_100_M2";break;
		case ENUM_100_150_M2_F: buff3="FUMIGATE_100_150_M2";break;
		case ENUM_150_200_M2_F: buff3="FUMIGATE_150_200_M2";break;
		default:buff3="FUMIGATE_5_15_M2";break; 
	}

    if (ConfigParam.WorkingMode.working_mode == ENUM_FUMIGATE_NORMAL_MODE || ConfigParam.WorkingMode.working_mode == ENUM_FUMIGATE_ENCHANCE_MODE)
    {
		if ( ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP )
		{	//处于开始工作状态
			buff4="SET_FUMIGATE_STOP";
		}
		else
		{
			buff4="SET_FUMIGATE_START";
		}
	}
	else
	{	//当不处于灭蚊状态时,那么灭蚊就应该停止
		buff4="SET_FUMIGATE_STOP";
	}

	memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
	sprintf(pctx->ble_tx_buffer, "FUMIGATE_SETT/%03d/%s/%04.1f/%s/%s/REMAINS_%03d_%03d/%s",
					ConfigParam.FactorySetting.fumigate_time,
					buff1,
					ConfigParam.FumigateSetting.fumi_house_height.dat_f,
					buff2,
					buff3,
					percentage_of_repel_medicine,
					percentage_of_fumigate_medicine,
					buff4
					);
	pctx->ble_tx_buffer_size=osal_strlen(pctx->ble_tx_buffer)+1;
	if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
	{  //数据超过缓冲区的大小
		return;
	}		
	uartrx_timeout_timer_stop();
	uartrx_timeout_timer_start();
}

/**
  * @brief  设置灭蚊设置参数    
  * @param  p:指向接收到的字符串
  * @retval 无
  */
static void Fumigate_sett_set(const char *p)
{
  float dat1=0;
  float dat2=0;
  //SET_FUMIGATE_SETT/MODE_FUMIGATE_ENCHANCE/02.3/FUMIGATE_5_15_M2
  if(ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
  {
    if(strstr(p,"MODE_FUMIGATE_ENCHANCE"))
    {
      FumigateEnchanceFunction();  //这种东东得由专门的函数来完成

      dat1=skip_atoi( strstr(p,"MODE_FUMIGATE_ENCHANCE/")+strlen("MODE_FUMIGATE_ENCHANCE/") );
      dat2=skip_atoi( strstr(p,"MODE_FUMIGATE_ENCHANCE/")+strlen("MODE_FUMIGATE_ENCHANCE/___") );
      dat1=dat1*10+dat2;
      dat1=dat1/10;
      ConfigParam.FumigateSetting.fumi_house_height.dat_f=dat1;
    }
    else if(strstr(p,"MODE_FUMIGATE_NORMAL"))
    {
      MosFumigateNormalFunction();

      dat1=skip_atoi( strstr(p,"MODE_FUMIGATE_NORMAL/")+strlen("MODE_FUMIGATE_NORMAL/") );
      dat2=skip_atoi( strstr(p,"MODE_FUMIGATE_NORMAL/")+strlen("MODE_FUMIGATE_NORMAL/___") );
      dat1=dat1*10+dat2;
      dat1=dat1/10;
      ConfigParam.FumigateSetting.fumi_house_height.dat_f=dat1;
    }
	
    if(strstr(p,"FUMIGATE_5_15_M2"))
    {
      Set_Fumigate_Area5_15m2();
    }
    else if(strstr(p,"FUMIGATE_15_30_M2"))
    {
      Set_Fumigate_Area15_30m2();
    }
    else if(strstr(p,"FUMIGATE_30_50_M2"))
    {
      Set_Fumigate_Area30_50m2();
    }
    else if(strstr(p,"FUMIGATE_50_80_M2"))
    {
      Set_Fumigate_Area50_80m2();
    }
    else if(strstr(p,"FUMIGATE_80_100_M2"))
    {
      Set_Fumigate_Area80_100m2();
    }
    else if(strstr(p,"FUMIGATE_100_150_M2"))
    {
      Set_Fumigate_Area100_150m2();
    }
    else if(strstr(p,"FUMIGATE_150_200_M2"))
    {
      Set_Fumigate_Area150_200m2();
    }
	Fumigate_Area_Indicate();
  }
}

/**
  * @brief  向APP发送当前的驱蚊设置
  * @param  无
  * @retval 无
  */
static void Send_repel_sett(void)
{
  BLE_Data_Buffer_Typedef* pctx = & Ble_Buffer;

  const char *buff1;
  const char *buff2;
  const char *buff4;

  switch (ConfigParam.FumigateSetting.fumi_sett_area)
  {
    case ENUM_5_15_M2_F:  buff1="REPEL_5_15_M2";break;
    case ENUM_15_30_M2_F: buff1="REPEL_15_30_M2";break;
    case ENUM_30_50_M2_F: buff1="REPEL_30_50_M2";break;
    default: buff1="REPEL_5_15_M2";break;
  }
  switch (ConfigParam.WorkingMode.repel_buf_mode)
  {
    case ENUM_REPEL_AIR_CONDI_MODE: buff2="MODE_REPEL_AIR";break;
    case ENUM_REPEL_MATERNAL_MODE:  buff2="MODE_REPEL_MATERNAL";break;
    case ENUM_REPEL_ENCHANCE_MODE:  buff2="MODE_REPEL_ENCHANCE";break;
    default:buff2="MODE_REPEL_ENCHANCE";break;   //默认是增强驱蚊模式
  }

  if (	ConfigParam.WorkingMode.working_mode == ENUM_REPEL_AIR_CONDI_MODE ||
		ConfigParam.WorkingMode.working_mode == ENUM_REPEL_MATERNAL_MODE ||
		ConfigParam.WorkingMode.working_mode == ENUM_REPEL_ENCHANCE_MODE )
    {
		if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
		{
			buff4="SET_REPEL_STOP";
		}
		else
		{
			buff4="SET_REPEL_START";
		}
	}
	else
	{	//当不处于驱蚊状态时,那么驱蚊就应该停止
		buff4="SET_REPEL_STOP";
	}


  memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
  sprintf(pctx->ble_tx_buffer, "REPEL_SETT/%s/%s/REPEL_TIME_%04d/REMAINS_%03d_%03d/%s",
                buff1,
                buff2,
                (uint32_t)(minute/60),
                percentage_of_repel_medicine,
                percentage_of_fumigate_medicine,
                buff4
                );
	pctx->ble_tx_buffer_size=osal_strlen(pctx->ble_tx_buffer)+1;
	if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
	{  //数据超过缓冲区的大小
		return;
	}		
	uartrx_timeout_timer_stop();
	uartrx_timeout_timer_start();
}

/**
  * @brief  设置驱蚊设置				找到自己的组织！！！
  * @param  p:指向接收到的字符串
  * @retval 无
  */
static void Repel_sett_set(const char *p)   
{
  if(ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
  {
    if(strstr(p,"MODE_REPEL_AIR"))
    {
      AirConditionerFunction();   //这里不用更改模式,但如果涉及到开启或者关闭就得切换到相应的模式
    }
    else if(strstr(p,"MODE_REPEL_MATERNAL"))
    {
      MaternalFunction();
    }
    else if(strstr(p,"MODE_REPEL_ENCHANCE"))
    {
      RepelEnchanceFunction();
    }

    if(strstr(p,"REPEL_5_15_M2"))
    {
      Set_Fumigate_Area5_15m2();
    }
    else if(strstr(p,"REPEL_15_30_M2"))
    {
      Set_Fumigate_Area15_30m2();
    }
    else if(strstr(p,"REPEL_30_50_M2"))    //上面的只能4选一
    {
      Set_Fumigate_Area30_50m2();
    }
	Fumigate_Area_Indicate();
  }
}

/**
  * @brief  读取香薰相关的设置
  * @param  无  
  * @retval 无
  */
void Send_aroma_sett(void)
{
  BLE_Data_Buffer_Typedef* pctx = & Ble_Buffer;
	
  const char *buff1;
  const char *buff4;

  switch (ConfigParam.FumigateSetting.fumi_sett_area)
  {
    case ENUM_5_15_M2_F:  buff1="AROMA_5_15_M2";break;
    case ENUM_15_30_M2_F: buff1="AROMA_15_30_M2";break;
    case ENUM_30_50_M2_F: buff1="AROMA_30_50_M2";break;
    default: buff1="AROMA_5_15_M2";break;
  }

    if (ConfigParam.WorkingMode.working_mode == ENUM_REPEL_AROMA_MODE )
    {
		if (ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
		{
			buff4="SET_AROMA_STOP";
		}
		else
		{
			buff4="SET_AROMA_START";
		}
	}
	else
	{	//当不处于香薰状态时,那么香薰就应该停止
		buff4="SET_AROMA_STOP";
	}


  //memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
  sprintf(pctx->ble_tx_buffer, "AROMA_SETT/%s/AROMA_TIME_%04d/REMAINS_%03d_%03d/%s",
                buff1,
                (uint32_t)(minute/60),
                percentage_of_repel_medicine,
                percentage_of_fumigate_medicine,
                buff4
                );
	pctx->ble_tx_buffer_size=osal_strlen(pctx->ble_tx_buffer)+1;
	if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
	{  //数据超过缓冲区的大小
		return;
	}		
	uartrx_timeout_timer_stop();
	uartrx_timeout_timer_start();
}

/**
  * @brief  转换为相应的香薰模式
  * @param  p:指向接收到的字符串
  * @retval 无
  */
static void Aroma_sett_set(const char *p)
{
  if(ConfigParam.WorkingStatus.working_status == ENUM_WORKING_STOP)
  {
    AromaFunction();

    if(strstr(p,"AROMA_5_15_M2"))
    {
      Set_Fumigate_Area5_15m2();
    }
    else if(strstr(p,"AROMA_15_30_M2"))
    {
      Set_Fumigate_Area15_30m2();
    }
    else if(strstr(p,"AROMA_30_50_M2"))    //上面的只能4选一
    {
      Set_Fumigate_Area30_50m2();
    }
	Fumigate_Area_Indicate();
  }
}


/**
 * @brief 	对接收数据区域的数据进行解析
 * @param	无
 * @retval 	无
 */
int Ble_Rx_Action(void)
{
  BLE_Data_Buffer_Typedef* pctx = &Ble_Buffer;

  if(pctx->ble_rx_state != BUP_RX_ST_IDLE && pctx->ble_rx_size)
  {	
    pctx->ble_rx_state = BUP_RX_ST_SENDING;  //表示正在接受!! !!
	/*这里可以写,真正的数据处理函数*/

	LOG("RX--%s\n",pctx->ble_rx_buf);

	Buzzer_Beep_Once();
	if(strstr(pctx->ble_rx_buf,"PUMP_TEST"))
	{
		SetPumpLoop(5);  //泵5次
		Ble_Tx_Action("TRUE");
	}
	else if(strstr(pctx->ble_rx_buf,"SET_FUMIGATE_START"))
	{	//这里是开启灭蚊  
		RemoteControl_Fumigate_Start();
	}
	else if(strstr(pctx->ble_rx_buf,"SET_FUMIGATE_STOP"))
	{	//这里是关闭灭蚊
		RemoteControl_Fumigate_Stop();
	}
	else if(strstr(pctx->ble_rx_buf,"SET_REPEL_START"))
	{	//这里是开启驱蚊 
		RemoteControl_Repel_Start();
	}
	else if(strstr(pctx->ble_rx_buf,"SET_REPEL_STOP"))
	{	//这里是关闭驱蚊
		RemoteControl_Repel_Stop();
	}
	else if(strstr(pctx->ble_rx_buf,"SET_AROMA_START"))
	{	//这里是开启香薰  
		RemoteControl_Aroma_Start();
	}
	else if(strstr(pctx->ble_rx_buf,"SET_AROMA_STOP"))
	{	//这里是关闭香薰  
		RemoteControl_Aroma_Stop();
	}
	else if(strstr(pctx->ble_rx_buf,"READ_STATE_CMD"))
	{	//读取当前的工作状态 
		State_send();
	}
	else if(strstr(pctx->ble_rx_buf,"READ_REPEL_FACTORY_SETT"))
	{	//读取当前的驱蚊出厂设置
		Repel_Factory_setting_send();
	}
	else if (strstr(pctx->ble_rx_buf, "SET_REPEL_FACTORY_SETT/"))		//这里还得进行,简化
	{ 	//设置驱蚊出厂设置参数		设置完成后得进行返回操作
  		memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
		strncpy(pctx->ble_tx_buffer,strstr(pctx->ble_rx_buf,"SET_REPEL_FACTORY_SETT/"),strlen(pctx->ble_rx_buf)+1); 
		Repel_factory_sett_set(strstr(pctx->ble_rx_buf, "SET_REPEL_FACTORY_SETT/") + strlen("SET_REPEL_FACTORY_SETT/"));
		pctx->ble_tx_buffer_size=strlen(pctx->ble_tx_buffer)+1;
		if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
		{  //数据超过缓冲区的大小
			return 0;
		}		
		uartrx_timeout_timer_stop();
		uartrx_timeout_timer_start();
	}
	else if(strstr(pctx->ble_rx_buf,"READ_FUMIGATE_FACTORY_SETT"))
	{	//读取当前的灭蚊出厂设置
		Fumigate_Factory_setting_send();   //这个可以
	}
	else if (strstr(pctx->ble_rx_buf, "SET_FUMIGATE_FACTORY_SETT/"))
	{ //设置灭蚊出厂设置参数		参数最后还是会返回的
  		memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
		strncpy(pctx->ble_tx_buffer,strstr(pctx->ble_rx_buf,"SET_FUMIGATE_FACTORY_SETT/"),strlen(pctx->ble_rx_buf)+1); 
		Fumigate_factory_sett_set(strstr(pctx->ble_rx_buf, "SET_FUMIGATE_FACTORY_SETT/") + strlen("SET_FUMIGATE_FACTORY_SETT/"));
		pctx->ble_tx_buffer_size=strlen(pctx->ble_tx_buffer)+1;
		if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
		{  //数据超过缓冲区的大小
			return 0;
		}		
		uartrx_timeout_timer_stop();
		uartrx_timeout_timer_start();
	}
	else if (strstr(pctx->ble_rx_buf, "READ_PUMP_NUM"))
	{ //读取当前泵了多少次 
		Pump_Num_send();
	}
	else if (strstr(pctx->ble_rx_buf, "READ_FUMIGATE_SETT"))
	{ //读取灭蚊设置 
		Fumigate_setting_send();
	}
	else if (strstr(pctx->ble_rx_buf, "SET_FUMIGATE_SETT/"))
	{ //设置灭蚊参数		 SET_FUMIGATE_SETT/MODE_FUMIGATE_ENCHANCE/02.9/ FUMIGATE_AUTO_M2 
							
  		memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
		strncpy(pctx->ble_tx_buffer,strstr(pctx->ble_rx_buf,"SET_FUMIGATE_SETT/"),strlen(pctx->ble_rx_buf)+1); 
		Fumigate_sett_set(strstr(pctx->ble_rx_buf, "SET_FUMIGATE_SETT/") + strlen("SET_FUMIGATE_SETT/"));
		pctx->ble_tx_buffer_size=strlen(pctx->ble_tx_buffer)+1;
		if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
		{  //数据超过缓冲区的大小
			return 0;
		}		
		uartrx_timeout_timer_stop();
		uartrx_timeout_timer_start();
	}
	else if (strstr(pctx->ble_rx_buf, "READ_REPEL_SETT"))
	{ //读取灭蚊设置
		Send_repel_sett();
	}
	else if (strstr(pctx->ble_rx_buf, "REPEL_MEDICINE_RESET"))
	{ //将驱蚊药剂量重置 
		ConfigParam.RepelSetting.total_repel_time=HOURS_OF_REPEL*3600L,
		osal_snv_write(ConfigParam.RepelSetting.repel_sett_id,REPEL_SETT_LENGTH,&(ConfigParam.RepelSetting.repel_sett_area));
		Ble_Tx_Action("REPEL_MEDICINE_RESET"); //将命令返回
  		CalculateMedicineQuantity();
		  CloseOptionLED();
	}
	else if (strstr(pctx->ble_rx_buf, "SET_REPEL_SETT/"))
	{ //对驱蚊设置进行设置
  		memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
		strncpy(pctx->ble_tx_buffer,strstr(pctx->ble_rx_buf,"SET_REPEL_SETT/"),strlen(pctx->ble_rx_buf)+1); 
		Repel_sett_set(strstr(pctx->ble_rx_buf, "SET_REPEL_SETT/") + strlen("SET_REPEL_SETT/"));
		pctx->ble_tx_buffer_size=strlen(pctx->ble_tx_buffer)+1;
		if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
		{  //数据超过缓冲区的大小
			return 0;
		}		
		uartrx_timeout_timer_stop();
		uartrx_timeout_timer_start();
	} 
	else if (strstr(pctx->ble_rx_buf, "READ_AROMA_SETT"))
	{ //读取香薰设置
		Send_aroma_sett();
	}  
	else if (strstr(pctx->ble_rx_buf, "AROMA_MEDICINE_RESET"))
	{ //读取灭蚊设置
		ConfigParam.RepelSetting.total_repel_time=HOURS_OF_REPEL*3600L,
		osal_snv_write(ConfigParam.RepelSetting.repel_sett_id,REPEL_SETT_LENGTH,&(ConfigParam.RepelSetting.repel_sett_area));
		Ble_Tx_Action("AROMA_MEDICINE_RESET"); //将命令返回
  		CalculateMedicineQuantity();
				  CloseOptionLED();
	}
	else if (strstr(pctx->ble_rx_buf, "SET_AROMA_SETT/"))
	{ //对香薰设置进行设置
  		memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
		strncpy(pctx->ble_tx_buffer,strstr(pctx->ble_rx_buf,"SET_AROMA_SETT/"),strlen(pctx->ble_rx_buf)+1); 
		Aroma_sett_set(strstr(pctx->ble_rx_buf, "SET_AROMA_SETT/") + strlen("SET_AROMA_SETT/"));
		pctx->ble_tx_buffer_size=strlen(pctx->ble_tx_buffer)+1;
		if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
		{  //数据超过缓冲区的大小
			return 0;
		}		
		uartrx_timeout_timer_stop();
		uartrx_timeout_timer_start();
	}  

	memset(pctx->ble_rx_buf, 0, sizeof(pctx->ble_rx_buf));
    pctx->ble_rx_size = 0;   //数据处理完成,才可对
  	pctx->ble_rx_state = BUP_RX_ST_IDLE;	//表示接受数据处理完成
    return PPlus_SUCCESS;
  }
  LOG("Ble_Rx_Action Error !\n");
  return PPlus_ERR_INVALID_STATE;
}


/**
 * @brief   会触发Ble_Rx_Action,就是接受动作函数
 * @param   无
 * @retval  无
 */
static void ble_rx_action_timer(uint16_t timeout)
{
  osal_start_timerEx(bleuart_TaskID, BUP_OSAL_EVT_BLE_TIMER, timeout);
}

/**
 * @brief   会触发函数,将数据从发送缓冲区,发送到蓝牙上
 * @param   无
 * @retval  无
 */
static void ble_tx_start_timer(uint16_t timeout)
{
  osal_start_timerEx(bleuart_TaskID, BUP_OSAL_EVT_UARTRX_TIMER, timeout);
}


/**
 * @brief 	将蓝牙接收到的数据复制到蓝牙接受缓冲区  
 * @param	无
 * @retval 	无
 */
int Receive_data_from_ble(uint8_t* pdata, uint16_t size)
{	
	BLE_Data_Buffer_Typedef* pctx = &Ble_Buffer;

	switch(pctx->ble_rx_state)
	{   //这里是将数据copy一下
		case BUP_RX_ST_IDLE:  //将蓝牙接收到的数据,放到蓝牙接收缓冲区
			memcpy(pctx->ble_rx_buf + pctx->ble_rx_size, pdata, size); 
			pctx->ble_rx_size += size; 
			pctx->ble_rx_state = BUP_RX_ST_DELAY_SLOT;
			ble_rx_action_timer(1);  //1ms delay slot  其实就是触发Ble_Rx_Action
			break;
		case BUP_RX_ST_DELAY_SLOT:
		case BUP_RX_ST_SENDING:  //表示正在进行数据处理
			memcpy(pctx->ble_rx_buf + pctx->ble_rx_size, pdata, size);
			pctx->ble_rx_size += size;
			break;
		default:
			Ble_Buffer.ble_rx_state = BUP_RX_ST_IDLE;
			break;
	}
	return PPlus_SUCCESS;
}

/****************************************************************************************************/

/**
 * @brief 	将蓝牙发送缓冲区的数据通过蓝牙发送出去
 * @param	无
 * @retval 	当前发送的状态
 */
int Ble_data_send(void)   
{
	BLE_Data_Buffer_Typedef* pctx = &Ble_Buffer;
	bool start_flg = FALSE;
	uint16 mtu_size=0;
	bStatus_t ret = 0;
	
	mtu_size=gAttMtuSize[gapConnHandle];
  	//LOG("mtu=%d,%d,%d\n",gAttMtuSize[gapConnHandle],pctx->ble_tx_state,pctx->ble_tx_size);
	{
		mtu_size -=3;
	}	
	
	if(pctx->ble_tx_state != BUP_TX_ST_IDLE && pctx->ble_tx_size)
	{ 
		if(bleuart_NotifyIsReady() == FALSE)  //还没有准备好
		{
			return PPlus_ERR_BLE_NOT_READY;
		}

		if(pctx->ble_tx_state == BUP_TX_ST_DELAY_SLOT)
		{	//处于合法的发送状态
			start_flg = TRUE;
			FLOW_CTRL_BLE_TX_LOCK();   //这个东东不知道是干啥的???
			pctx->ble_tx_state = BUP_TX_ST_SENDING;  //由BUP_RX_ST_DELAY_SLOT状态转变为BUP_RX_ST_SENDING
		}
		// BUP_RX_ST_IDLE = 0,
		// BUP_RX_ST_DELAY_SLOT,
		// BUP_RX_ST_SENDING
		while(1)
		{	//循环发送直到,发送完成   low_profile  
			uint16_t size =0;
			
			attHandleValueNoti_t notify_data={0};
			size = ((pctx->ble_tx_size - pctx->ble_tx_offset) > mtu_size) ? mtu_size : pctx->ble_tx_size - pctx->ble_tx_offset;
			//这里应该是控制每次发送的最大长度
			memcpy(notify_data.value,pctx->ble_tx_buf + pctx->ble_tx_offset, size);  //这里pctx->ble_tx_buf + pctx->rx_offset得好好探讨一下
			notify_data.len = size;  //要发送数据的长度
			
			ret = bleuart_Notify(gapConnHandle, &notify_data, bleuart_TaskID);  //这个函数是核心！！！！
			//LOG("bleuart_Notify: %d, %d, %d\n", ret,pctx->ble_tx_offset, pctx->ble_tx_size);
			if(ret == SUCCESS)
			{  //将rx_offset的大小加上 size
				pctx->ble_tx_offset += size;    //数据位置移动
			}
			else
			{  //这里是失败了
				//LOG("TX R=%x\n",ret);
				if(ret == MSG_BUFFER_NOT_AVAIL || ret == INVALID_INTERRUPT_ID)
				{
					if(start_flg)
					{
						ble_tx_start_timer(1);  //重新发送!!!!!!  触发Ble_data_send函数
					}
					else
					{ //
						ble_tx_start_timer(ble_conn_interval()-1); //重新发送!!!!!!  触发Ble_data_send函数 
					}
					return PPlus_SUCCESS;
				}
				else
				{
					return PPlus_ERR_BUSY;
				}
			}
		
			if(pctx->ble_tx_offset == pctx->ble_tx_size)	 //在发送的过程中rx_size还是会变化
			{	//rx_size是属于将要发送的数据得长度		这里表示数据发送完成
				//LOG("Success\n");     
				pctx->ble_tx_state = BUP_TX_ST_IDLE;   //标记为发送空闲
				pctx->ble_tx_offset = 0;
				pctx->ble_tx_size = 0;
				FLOW_CTRL_BLE_TX_UNLOCK();
				return PPlus_SUCCESS;
			}
		}
	}
	else
	{
		//LOG("U2B s=:%x,%x",pctx->ble_tx_state,pctx->ble_tx_size);
		pctx->ble_tx_state = BUP_TX_ST_IDLE;
		pctx->ble_tx_offset = 0;
		pctx->ble_tx_size = 0;
		FLOW_CTRL_BLE_TX_UNLOCK();  //解锁
	}	

	//LOG("U2B ret : %x\n",ret);
	return PPlus_ERR_INVALID_STATE;
}

/**
 * @brief 	将待发送的数据转运到蓝牙发送数据缓冲区
 * @param	无
 * @retval 	无
 */
int Transfer_data_to_ble_Txbuffer(void)
{
  	BLE_Data_Buffer_Typedef* pctx = &Ble_Buffer;
	//LOG("Transfer_data_to_ble_Txbuffer\n");
  	if(pctx->ble_conn_state == FALSE)
	{	//这里表示蓝牙没有连接
		//LOG("no cnt\n");
		pctx->ble_tx_size = 0;
		pctx->ble_tx_offset = 0;
		pctx->ble_tx_buffer_size = 0;
		return PPlus_ERR_INVALID_STATE;
  	}  //                     紧跟着以前数据的尾巴
	LOG("TX--%s\n",pctx->ble_tx_buffer);
  	memcpy(pctx->ble_tx_buf + pctx->ble_tx_size, pctx->ble_tx_buffer, pctx->ble_tx_buffer_size);  //ble_tx_buffer_size表示将要被复制的数据的大小
  	if(pctx->ble_tx_offset != 0)
	{	//此时,表示还没发送完成   就不会触发Ble_data_send函数  上面的复制就没有用了
		return PPlus_ERR_BUSY;
  	}
  	pctx->ble_tx_size += pctx->ble_tx_buffer_size;
  
	pctx->ble_tx_buffer_size = 0;
  	switch(pctx->ble_tx_state)
	{
		case BUP_TX_ST_IDLE:
			pctx->ble_tx_state = BUP_TX_ST_DELAY_SLOT;
			ble_tx_start_timer(1);  //触发将蓝牙发送区的数据,往蓝牙上发送  触发Ble_data_send函数
			break;
		case BUP_TX_ST_DELAY_SLOT:
		case BUP_TX_ST_SENDING:
		default:
			//drop data
			//LOG("U2B err:%d\n",pctx->ble_tx_state);
			return PPlus_ERR_INVALID_STATE;
  	}
  	return PPlus_SUCCESS;
}




/**
 * @brief  蓝牙发送函数 会触发调用Transfer_data_to_ble_Txbuffer函数  这个只是基础的函数,其它发送函数得调用它
 * @param  指向待发送的字符串
 * @retval 无
 */
void Ble_Tx_Action(char * TxData)
{
  BLE_Data_Buffer_Typedef* pctx = & Ble_Buffer;

  //memset(pctx->ble_tx_buffer, 0, sizeof(pctx->ble_tx_buffer));
	strncpy(pctx->ble_tx_buffer,TxData,osal_strlen(TxData)+1);
	pctx->ble_tx_buffer_size=osal_strlen(TxData)+1;
	if((pctx->ble_tx_buffer_size)>=UART_TX_BUF_SIZE)
	{  //数据超过缓冲区的大小
		return;
	}		
	uartrx_timeout_timer_stop();
	uartrx_timeout_timer_start();
}
/****************************************************************************************************/


/**
 * @brief   当蓝牙连接断开时,就会调用此函数
 * @param   无
 * @retval  无
 */
int BUP_disconnect_handler(void)
{
	memset(&Ble_Buffer, 0, sizeof(Ble_Buffer));
	return PPlus_SUCCESS;
}

/**
 * @brief   当蓝牙连接上时,就会调用此函数
 * @param   无
 * @retval  无
 */
int BUP_connect_handler(void)
{
	if(Ble_Buffer.ble_conn_state == FALSE)
	{
		memset(&Ble_Buffer, 0, sizeof(Ble_Buffer));
		Ble_Buffer.ble_conn_state = TRUE;
	}
	return PPlus_SUCCESS;
}









