#ifndef __BSP_KEY_H__
#define __BSP_KEY_H__

#include "types.h"

//这里是按键的输入管脚(包括泵计数)
#define KEY_GPIO		P7		//按键GPIO
#define PUMP_CNT_GPIO   P11	    //泵计数GPIO

#define KEY_CYCLE_TIMER     0x0002  //按键线程事件ID

#define KEY_NUM 		2
#define KEY_DOWN_TIME_2S_THRESHOLD		2000        //长按2s
#define KEY_DOWN_TIME_4S_THRESHOLD		4000        //长按4s

typedef enum
{
    KEY_DOWN_4S_FLAG,
    KEY_DOWN_2S_FLAG,
    KEY_DOWN_RELEASE_FLAG,
    KEY_DOWN_ALL_FLAG,
}KEY_DOWN_TIME_FLAG_Typedef;

extern uint8_t LongKeyFlag;

void Key_ProcessInit( uint8 task_id );
uint16 Key_ProcessEvent( uint8 task_id, uint16 events);

#endif