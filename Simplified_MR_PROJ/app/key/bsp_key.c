#include "board.h"

KEY_DOWN_TIME_FLAG_Typedef Key_Time_Flag = KEY_DOWN_2S_FLAG;
static uint8 key_TaskID;
static uint8_t execute = 0;
uint8_t LongKeyFlag = 0;

//定义按键当前状态数组变量
static uint8_t key_state[KEY_NUM] = { 0, 0 };
//记录当前按键按下的时间(单位ms)
static uint32_t key_down_time[KEY_NUM - 1] = { 0 }; //这样写对吗????

/**
 *	@brief  KEY管脚(包括泵计数管脚)初始化函数
 *	@param	无
 *	@return	无
 */
static void KEY_GPIO_Init(void)
{
	hal_gpio_pin_init(KEY_GPIO, IE);   //将他们配置为输入状态
	hal_gpio_pin_init(PUMP_CNT_GPIO, IE);
}

/**
 *	@brief  按键驱动函数,需要每隔一定时间进行一次调用
 *	@param	无
 *	@return	无
 */
static void Key_Driver(void)
{
	static uint8_t key_state_buff[KEY_NUM] = { 0, 0 };   //默认初始电平是低电平
	uint8_t i;

	for (i = 0; i < KEY_NUM; ++i)
	{	//这里是短按
		if (key_state_buff[i] != key_state[i]) //按键被按下或弹起(只执行变化时的值)
		{
			//备份按键的状态
			key_state_buff[i] = key_state[i];
			if (key_state[i] == 0) //按键弹起
			{
				if (execute == 1)
				{	//表示长按后弹起
					LongKeyFlag = 1;  //表示此时是长按电容按键后,按键弹起来的结果!!
					execute = 0;
					if (Key_Time_Flag == KEY_DOWN_4S_FLAG)
					{
						KeyAction(2);  //2秒按下
					}
					else if (Key_Time_Flag == KEY_DOWN_RELEASE_FLAG)
					{
						KeyAction(3);  //4秒按下
					}
					Key_Time_Flag = KEY_DOWN_2S_FLAG;
				}
				//调用KeyAction函数
				KeyAction(i);
			}
			else
			{	//按键被按下!!!
				LongKeyFlag = 0;
			}
		}
	}


	for (i = 0; i < (KEY_NUM - 1); ++i)
	{   //这里是长按  长短按键只针对电容按键
		if (key_down_time[i] > 0) //很关键
		{
			if ((key_down_time[i] > KEY_DOWN_TIME_2S_THRESHOLD) && (Key_Time_Flag == KEY_DOWN_2S_FLAG)) //按键按下时间超过2S阈值时间
			{
				execute = 1;
				Key_Time_Flag = KEY_DOWN_4S_FLAG;
				LOG("KEY_DOWN_2S\n");
				Buzzer_Beep_Three_Times(); //响三下

			}
			else if ((key_down_time[i] > KEY_DOWN_TIME_4S_THRESHOLD) && (Key_Time_Flag == KEY_DOWN_4S_FLAG))
			{
				execute = 1;
				Key_Time_Flag = KEY_DOWN_RELEASE_FLAG;
				LOG("KEY_DOWN_4S\n");
				Buzzer_Beep_Three_Times(); //响三下
			}
		}
	}
}

/**
 * @brief  按键扫描函数(通过软件滤波来更新当前的按键状态  4ms调用一次)
 * @param  无
 * @retval 无
 */
static void KEY_Scan(void)
{
	static uint8_t key_buffer[KEY_NUM] = { 0x00, 0x00 }; //软件滤波用数组
	uint8_t i = 0;

	key_buffer[0] = (key_buffer[0] << 1) | ((uint8_t)hal_gpio_read(KEY_GPIO));	     //采样按键的状态
	key_buffer[1] = (key_buffer[1] << 1) | ((uint8_t)hal_gpio_read(PUMP_CNT_GPIO));	 //采样泵计数按键的状态
	for (i = 0; i < KEY_NUM; ++i)
	{	//要有价值,value
		if ((key_buffer[i] & 0x0F) == 0x00)
		{	//没有被按下
			key_state[i] = 0;
			if (i == 0)
			{ //长按只针对,电容按键的
				key_down_time[i] = 0;
			}
		}
		else if ((key_buffer[i] & 0x0F) == 0x0F)
		{   //按下去了    每4ms调用一次,连续4次,就是16ms
			key_state[i] = 1;
			if (i == 0)
			{
				key_down_time[i] += 4;
			}
		}
	}
}


/**
 * @brief 按键线程初始化
 * @param task_id 线程的id号
 * @retval 无
 */
void Key_ProcessInit(uint8 task_id)
{
	key_TaskID = task_id;
	LOG("KEY task start...\n");
	KEY_GPIO_Init();
	osal_start_reload_timer(key_TaskID, KEY_CYCLE_TIMER, 4);
}


static void BUP_ProcessOSALMsg(osal_event_hdr_t* pMsg)
{
	switch (pMsg->event)
	{
	default:
		// do nothing
		break;
	}
}

/**
 * @brief 按键扫描线程
 * @param task_id 线程的id号
 * @param events 任务的事件参数
 * @retval 我也不知道返回啥??
 */
uint16 Key_ProcessEvent(uint8 task_id, uint16 events)
{
	VOID task_id; // OSAL required parameter that isn't used in this function

	if (events & SYS_EVENT_MSG)
	{
		uint8* pMsg;

		if ((pMsg = osal_msg_receive(key_TaskID)) != NULL)
		{
			BUP_ProcessOSALMsg((osal_event_hdr_t*)pMsg);
			// Release the OSAL message
			VOID osal_msg_deallocate(pMsg);
		}
		// return unprocessed events
		return (events ^ SYS_EVENT_MSG);
	}


	if (events & KEY_CYCLE_TIMER)
	{
		hal_watchdog_feed();
		KEY_Scan();
		Key_Driver();
		return (events ^ KEY_CYCLE_TIMER);
	}
	return 0;
}