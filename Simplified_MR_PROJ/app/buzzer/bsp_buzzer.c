#include "board.h"

static uint8 buzzer_TaskID;

static uint8_t beep_type;       


/**
 *	@brief		初始化蜂鸣器使用的PWM外设 
 *	@param	    none.
 *	@return		none.
 */
static void  Buzzer_Init(void)
{
	hal_gpio_pin2pin3_control(P3, Bit_ENABLE);
	hal_gpio_fmux(P3, Bit_DISABLE);
	hal_gpio_pin_init(P3, OEN);
	hal_gpio_write(P3, 0);
	
	hal_pwm_init(PWM_CH2, PWM_CLK_DIV_16, PWM_CNT_UP, PWM_POLARITY_FALLING);
	hal_pwm_open_channel(PWM_CH2,P3);
	hal_pwm_set_count_val(PWM_CH2, 0, 370);
	hal_pwm_start();
}

/**
 * @brief 蜂鸣器响一次
 * @param task_id 线程的id号
 * @retval 无
 */
void Buzzer_Beep_Once()
{
    beep_type=1;  //蜂鸣器响1下
    osal_start_timerEx(buzzer_TaskID, BUZZER_START_TIMER,50);
}


/**
 * @brief 蜂鸣器响三次
 * @param task_id 线程的id号
 * @retval 无
 */
void Buzzer_Beep_Three_Times()
{
    beep_type=3;  //蜂鸣器响1下
    osal_start_timerEx(buzzer_TaskID, BUZZER_START_TIMER,50);
}

/**
 * @brief 蜂鸣器线程初始化
 * @param task_id 线程的id号
 * @retval 无
 */
void Buzzer_ProcessInit( uint8 task_id)
{
	buzzer_TaskID = task_id;
	LOG("Buzzer task start...\n");
	Buzzer_Init();
}

static void BUP_ProcessOSALMsg( osal_event_hdr_t* pMsg )
{
    switch ( pMsg->event )
    {
		default:
        // do nothing
        break;
    }
}

	static uint8_t cnt=0;

/**
 * @brief 蜂鸣器线程
 * @param task_id 线程的id号
 * @param events 任务的事件参数
 * @retval 我也不知道返回啥??
 */
uint16 Buzzer_ProcessEvent( uint8 task_id, uint16 events)
{
    VOID task_id; // OSAL required parameter that isn't used in this function
    
    if (events & SYS_EVENT_MSG)
    {
      uint8* pMsg;

      if ((pMsg = osal_msg_receive(buzzer_TaskID)) != NULL)
      {
        BUP_ProcessOSALMsg((osal_event_hdr_t*)pMsg);
        // Release the OSAL message
        VOID osal_msg_deallocate(pMsg);
      }
      // return unprocessed events
      return (events ^ SYS_EVENT_MSG);
    }



	if( events & BUZZER_START_TIMER)
	{
		if(cnt==0)
		{
			if(beep_type==1)
			{
				cnt=2;		
				osal_start_timerEx(buzzer_TaskID, BUZZER_1TIMES_TIMER,50);
			}
			else if(beep_type==3)
			{
				cnt=6;
				osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,50);
			}
		}
		return (events ^ BUZZER_START_TIMER);
	}
	if( events & BUZZER_1TIMES_TIMER)
	{
		switch (cnt)
		{
			case 2: hal_pwm_set_count_val(PWM_CH2, 185, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_1TIMES_TIMER,100);break;
			case 1: hal_pwm_set_count_val(PWM_CH2, 0, 370);cnt--;break;
			default: break;
		}

		return (events ^ BUZZER_1TIMES_TIMER);
	}
	if( events & BUZZER_3TIMES_TIMER)
	{
		switch (cnt)
		{
			case 6: hal_pwm_set_count_val(PWM_CH2, 185, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,60);break;
			case 5: hal_pwm_set_count_val(PWM_CH2, 0, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,15);break;
			case 4: hal_pwm_set_count_val(PWM_CH2, 185, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,60);break;
			case 3: hal_pwm_set_count_val(PWM_CH2, 0, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,15);break;
			case 2: hal_pwm_set_count_val(PWM_CH2, 185, 370);cnt--;osal_start_timerEx(buzzer_TaskID, BUZZER_3TIMES_TIMER,60);break;
			case 1: hal_pwm_set_count_val(PWM_CH2, 0, 370);cnt--;break;
			default: break;
		}
		return (events ^ BUZZER_3TIMES_TIMER);
	}
    return 0;
}