#ifndef __BSP_BUZZER_H__
#define __BSP_BUZZER_H__

#include "types.h"

#define BUZZER_START_TIMER      0x0001
#define BUZZER_1TIMES_TIMER     0x0002
#define BUZZER_3TIMES_TIMER     0x0004


void Buzzer_Beep_Once(void);
void Buzzer_Beep_Three_Times(void);
void Buzzer_ProcessInit( uint8 task_id);
uint16 Buzzer_ProcessEvent( uint8 task_id, uint16 events);

#endif