/**
 *	@file		LC_UI_led_buzzer.h
 *	@author		YQ
 *	@data		09/16/2020
 *	@version	1.0.0
 *
 */

/*!
 * 	@defgroup	LC_UI_led_buzzer
 *	@brief
 *	@{*/

#ifndef	LC_UI_LED_BUZZER_H_
#define	LC_UI_LED_BUZZER_H_
/*------------------------------------------------------------------*/
/*						C++ guard macro								*/
/*------------------------------------------------------------------*/
#ifdef	__cplusplus
extern	"C"	{
#endif
/*------------------------------------------------------------------*/
/*						head files include 							*/
/*------------------------------------------------------------------*/
#include "LC_Common.h"
/*------------------------------------------------------------------*/
/*						Pins definitions							*/
/*------------------------------------------------------------------*/
#define		MY_GPIO_LED_R		P34
#define		MY_GPIO_LED_G		P2
#define		MY_GPIO_LED_B		P3
#define		MY_GPIO_LED_CW		GPIO_DUMMY
#define		MY_GPIO_LED_WW		P7


/*------------------------------------------------------------------*/
/*						MACROS										*/
/*------------------------------------------------------------------*/
#define		RGB_COLOR_MAX		(255)
#define		RGB_PWM_MAX			(255)
#define		RGB_WHITE_MAX		(180)
#define		RGB_LEVEL_PECENT	(100)
#define		RGB_PWM_COLOR_MUL	(RGB_PWM_MAX/RGB_COLOR_MAX)

//	BaoLianDeng APP Mode defines
#define		APP_STATIC_RED		0x80
#define		APP_STATIC_WHITE	0x86

#define		APP_JUMP_THREE		0x87
#define		APP_JUMP_SEVEN		0x88

#define		APP_FADE_THREE		0x89
#define		APP_FADE_G_B		0x94

#define		APP_FLASH_SEVEN		0x95
#define		APP_FLASH_WHITE		0x9C
/*------------------------------------------------------------------*/
/*						UI Task Events definitions					*/
/*------------------------------------------------------------------*/
#define		UI_EVENT_LEVEL1		0x0001
#define		UI_EVENT_LEVEL2		0x0002
#define		UI_EVENT_LEVEL3		0x0004
#define		UI_EVENT_LEVEL4		0x0008
/*------------------------------------------------------------------*/
/*						Data structures								*/
/*------------------------------------------------------------------*/
typedef struct
{
	uint16			offOn_Ms[2];
	uint8			offOn_cnt;
	uint8			next_mode;
}lc_ui_mode_para;

typedef struct
{
	lc_ui_mode_para *ui_type;
	uint8			cur_mode;
	uint8			cur_cnt;
	uint8			cur_state;
	uint32			next_wakeup_tick;
}lc_ui_run_para;
/*------------------------------------------------------------------*/
/*						external variables							*/
/*------------------------------------------------------------------*/
extern uint8	LC_Ui_Led_Buzzer_TaskID;
/*------------------------------------------------------------------*/
/*						User function prototypes					*/
/*------------------------------------------------------------------*/
extern	void	LC_RGB_Valeu_Deal					(uint8 evt		);
extern	void	LC_RGBLight_Reserve_Mode		(void			);
extern	void	LC_RGBLight_Reserve_Alarm		(void			);
extern	void 	LC_UI_Led_Buzzer_Task_Init		(uint8 task_id	);
extern	uint16	LC_UI_Led_Buzzer_ProcessEvent	(uint8 task_id, uint16 events);


#ifdef	__cplusplus
}
#endif

#endif	/*	LC_UI_LED_BUZZER_H_	*/
/**	@}*/
