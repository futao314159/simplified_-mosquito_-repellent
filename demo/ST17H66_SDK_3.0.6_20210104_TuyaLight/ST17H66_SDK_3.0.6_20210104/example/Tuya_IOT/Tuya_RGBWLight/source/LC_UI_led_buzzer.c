/**
 *	@file		LC_UI_led_buzzer.h
 *	@author		YQ
 *	@data		09/16/2020
 *	@version	1.0.0
 *
 */

/*!
 * 	@defgroup	LC_UI_led_buzzer
 *	@brief
 *	@{*/

/*------------------------------------------------------------------*/
/*						head files include 							*/
/*------------------------------------------------------------------*/
#include "LC_UI_Led_Buzzer.h"
#include "LC_RGBLight_Mode.h"

/*------------------------------------------------------------------*/
/* 					 	public variables		 					*/
/*------------------------------------------------------------------*/
uint8	LC_Ui_Led_Buzzer_TaskID;
/*------------------------------------------------------------------*/
/* 					 	public functions		 					*/
/*------------------------------------------------------------------*/
/*!
 *	@fn			LC_Gpio_UI_Led_Buzzer_Init
 *	@brief		Initialize the LED and Buzzer pins. 
 *	@param[in]	none.
 *	@return		none.
 */
void	LC_Gpio_UI_Led_Buzzer_Init(void)
{
	
	hal_gpio_fmux(MY_GPIO_LED_R, Bit_DISABLE);
	hal_gpio_pin_init(MY_GPIO_LED_R, OEN);
	hal_gpio_write(MY_GPIO_LED_R, 0);

	hal_gpio_pin2pin3_control(MY_GPIO_LED_G, Bit_ENABLE);
	hal_gpio_fmux(MY_GPIO_LED_G, Bit_DISABLE);
	hal_gpio_pin_init(MY_GPIO_LED_G, OEN);
	hal_gpio_write(MY_GPIO_LED_G, 0);
	
	hal_gpio_pin2pin3_control(MY_GPIO_LED_B, Bit_ENABLE);
	hal_gpio_fmux(MY_GPIO_LED_B, Bit_DISABLE);
	hal_gpio_pin_init(MY_GPIO_LED_B, OEN);
	hal_gpio_write(MY_GPIO_LED_B, 0);
	
	hal_gpio_fmux(MY_GPIO_LED_WW, Bit_DISABLE);
	hal_gpio_pin_init(MY_GPIO_LED_WW, OEN);
	hal_gpio_write(MY_GPIO_LED_WW, 0);

//	osal_snv_read(0x88, 16, Flash_Read_Buffer);
//	osal_snv_read(0x89, 16, Flash_Read_Buffer + 16);

//	LOG("read flash mode\n");
//	Printf_Hex(Flash_Read_Buffer,16);
//	LOG("read flash alarm\n");
//	Printf_Hex(Flash_Read_Buffer+16,16);


	
	hal_pwm_init(PWM_CH0, PWM_CLK_DIV_4, PWM_CNT_UP, PWM_POLARITY_FALLING);
	hal_pwm_open_channel(PWM_CH0, MY_GPIO_LED_R);
	
	hal_pwm_init(PWM_CH1, PWM_CLK_DIV_4, PWM_CNT_UP, PWM_POLARITY_FALLING);
	hal_pwm_open_channel(PWM_CH1, MY_GPIO_LED_G);
	
	hal_pwm_init(PWM_CH2, PWM_CLK_DIV_4, PWM_CNT_UP, PWM_POLARITY_FALLING);
	hal_pwm_open_channel(PWM_CH2, MY_GPIO_LED_B);

	hal_pwm_init(PWM_CH3, PWM_CLK_DIV_4, PWM_CNT_UP, PWM_POLARITY_FALLING);
	hal_pwm_open_channel(PWM_CH3, MY_GPIO_LED_WW);

	hal_pwm_set_count_val(PWM_CH0, LC_RGBLight_Param.RGB_rValue, RGB_PWM_MAX);
	hal_pwm_set_count_val(PWM_CH1, LC_RGBLight_Param.RGB_gValue, RGB_PWM_MAX);
	hal_pwm_set_count_val(PWM_CH2, LC_RGBLight_Param.RGB_bValue, RGB_PWM_MAX);
	hal_pwm_set_count_val(PWM_CH3, LC_RGBLight_Param.RGB_wValue, RGB_PWM_MAX);
	hal_pwm_start();
}
/*!
 *	@fn			LC_RGB_Valeu_Deal
 *	@brief		write new value to register.
 *	@param[in]	none.
 *	@return		none.
 */
void	LC_RGB_Valeu_Deal(uint8 evt)
{

	if(evt == HAL_EVT_TIMER_6){
		if(LC_RGBLight_Param.RGB_Light_Mode > RGB_Static_White){
			LC_RGBLight_Dynamic_Mode_Process();
		}

		hal_pwm_set_count_val(PWM_CH0, LC_RGBLight_Param.RGB_rValue, RGB_PWM_MAX);
		hal_pwm_set_count_val(PWM_CH1, LC_RGBLight_Param.RGB_gValue, RGB_PWM_MAX);
		hal_pwm_set_count_val(PWM_CH2, LC_RGBLight_Param.RGB_bValue, RGB_PWM_MAX);
		hal_pwm_set_count_val(PWM_CH3, LC_RGBLight_Param.RGB_wValue, RGB_PWM_MAX);
	}else if(evt == HAL_EVT_TIMER_5){
		LC_RGBLight_Dynamic_Basic_Timer();
	}
}
void	LC_RGBLight_Reserve_Mode(void)
{
	osal_start_timerEx(LC_Ui_Led_Buzzer_TaskID, UI_EVENT_LEVEL3, 500);
}

void	LC_RGBLight_Reserve_Alarm(void)
{
	osal_start_timerEx(LC_Ui_Led_Buzzer_TaskID, UI_EVENT_LEVEL4, 500);
}
/*!
 *	@fn			LC_UI_Led_Buzzer_Task_Init 
 *	@brief		Initialize function for the UI_LED_BUZZER Task. 
 *	@param[in]	task_id			:the ID assigned by OSAL,
 *								used to send message and set timer.
 *	@retrurn	none.
 */
void	LC_UI_Led_Buzzer_Task_Init(uint8 task_id)
{
	LC_Ui_Led_Buzzer_TaskID	=	task_id;
	LOG("LC_Gpio_UI_Led_Buzzer_Init:\n");
	LC_Timer_Start();
	LC_Gpio_UI_Led_Buzzer_Init();
}
/*!
 *	@fn			LC_UI_Led_Buzzer_ProcessEvent
 *	@brief		UI_LED_BUZZER Task event processor.This function
 *				is called to processs all events for the task.Events
 *				include timers,messages and any other user defined events.
 *	@param[in]	task_id			:The OSAL assigned task ID.
 *	@param[in]	events			:events to process.This is a bit map and can
 *									contain more than one event.
 */
uint16	LC_UI_Led_Buzzer_ProcessEvent(uint8 task_id, uint16 events)
{
	VOID task_id;	// OSAL required parameter that isn't used in this function
	if(events & SYS_EVENT_MSG){
		uint8	*pMsg;
		if((pMsg = osal_msg_receive(LC_Ui_Led_Buzzer_TaskID)) != NULL){
			LC_Common_ProcessOSALMsg((osal_event_hdr_t *)pMsg);
            // Release the OSAL message
			VOID osal_msg_deallocate(pMsg);
		}
		return(events ^ SYS_EVENT_MSG);
	}
	if(events & UI_EVENT_LEVEL1){
		if(LC_RGBLight_Param.RGB_Timer_Second_Num){
			LC_RGBLight_Param.RGB_Timer_Second_Num--;
			if(LC_RGBLight_Param.RGB_Timer_Second_Num == 0){
				if(LC_RGBLight_Param.RGB_Light_State == State_On){
					LC_RGBLight_Turn_Onoff(State_Off);
				}else if(LC_RGBLight_Param.RGB_Light_State == State_Off){
					LC_RGBLight_Turn_Onoff(State_On);
				}
				osal_stop_timerEx(LC_Ui_Led_Buzzer_TaskID, UI_EVENT_LEVEL1);
			}
		}
		osal_start_timerEx(LC_Ui_Led_Buzzer_TaskID, UI_EVENT_LEVEL1, 1000);
		return(events ^ UI_EVENT_LEVEL1);
	}
	// deal with datas from APP
	if(events & UI_EVENT_LEVEL2){
	
//		LOG("Decrype data len [%d]==",LC_App_Set_Param.app_write_len);
//		Printf_Hex(LC_App_Set_Param.app_write_data,LC_App_Set_Param.app_write_len);

		//	color & scene mode
		if((LC_App_Set_Param.app_write_len!=4) && (LC_App_Set_Param.app_write_data[0] == 0x02) && \
			(LC_App_Set_Param.app_write_data[1] == 0x04) && (LC_App_Set_Param.app_write_data[2] == 0x01)){
			if((LC_App_Set_Param.app_write_data[3] == 0x01) || (LC_App_Set_Param.app_write_data[3] == 0x02)){
				AsciiToHex(LC_App_Set_Param.app_write_data+7,LC_App_Set_Param.app_write_data+7,LC_App_Set_Param.app_write_len-7);
				LC_App_Set_Param.app_write_len	=	(LC_App_Set_Param.app_write_len - 7)/2 + 7;
//				LOG("data len [%d]==",LC_App_Set_Param.app_write_len);
//				Printf_Hex(LC_App_Set_Param.app_write_data,LC_App_Set_Param.app_write_len);
			}
		}
		//	music mode
		else if((LC_App_Set_Param.app_write_data[0] == 0x08) && (LC_App_Set_Param.app_write_data[1] == 0x03) && \
				(LC_App_Set_Param.app_write_data[2] == 0x15)){
			AsciiToHex(LC_App_Set_Param.app_write_data+4,LC_App_Set_Param.app_write_data+4,LC_App_Set_Param.app_write_len-4);
			LC_App_Set_Param.app_write_len		=	(LC_App_Set_Param.app_write_len - 4)/2 + 4;
			LC_App_Set_Param.app_write_data[3]	=	0;
//			LOG("music data len [%d]==",LC_App_Set_Param.app_write_len);
//			Printf_Hex(LC_App_Set_Param.app_write_data,LC_App_Set_Param.app_write_len);
		}
		
		// RGBLight Off On
		if((LC_App_Set_Param.app_write_data[0] == 0x01) && (LC_App_Set_Param.app_write_data[1] == 0x01) && (LC_App_Set_Param.app_write_data[2] == 0x01)){
			if((LC_App_Set_Param.app_write_data[3] == 0x00) && (LC_RGBLight_Param.RGB_Light_State == State_On)){
				LC_RGBLight_Turn_Onoff(State_Off);
			}else if((LC_App_Set_Param.app_write_data[3] == 0x01) && (LC_RGBLight_Param.RGB_Light_State == State_Off)){
				LC_RGBLight_Turn_Onoff(State_On);
			}
		}
		
		if((LC_App_Set_Param.app_write_len!=4) && (LC_App_Set_Param.app_write_data[0] == 0x02) && \
			(LC_App_Set_Param.app_write_data[1] == 0x04) && (LC_App_Set_Param.app_write_data[2] == 0x01)){
			//	white mode
			if(LC_App_Set_Param.app_write_data[3] == 0x00){
				LC_RGBLight_Param.RGB_Light_Mode	=	RGB_Plate_Mode;
				//	pecent of Cold & Warm
				if((LC_App_Set_Param.app_write_data[4] == 0x04)){
					uint8	color_pecent	=	0;
					color_pecent	=	(LC_App_Set_Param.app_write_data[9] *256 + LC_App_Set_Param.app_write_data[10])/10;
					LC_RGBLight_Param.RGB_rValue		=	RGB_COLOR_MAX*color_pecent*LC_RGBLight_Param.RGB_Light_Level/(RGB_LEVEL_PECENT*RGB_LEVEL_PECENT);
					LC_RGBLight_Param.RGB_gValue		=	RGB_COLOR_MAX*color_pecent*LC_RGBLight_Param.RGB_Light_Level/(RGB_LEVEL_PECENT*RGB_LEVEL_PECENT);
					LC_RGBLight_Param.RGB_bValue		=	RGB_COLOR_MAX*color_pecent*LC_RGBLight_Param.RGB_Light_Level/(RGB_LEVEL_PECENT*RGB_LEVEL_PECENT);
					LC_RGBLight_Param.RGB_wValue		=	RGB_COLOR_MAX*(RGB_LEVEL_PECENT - color_pecent)*LC_RGBLight_Param.RGB_Light_Level/(RGB_LEVEL_PECENT*RGB_LEVEL_PECENT);
					LC_RGBLight_Param.RGB_rValue_New	=	RGB_COLOR_MAX*color_pecent/RGB_LEVEL_PECENT;
					LC_RGBLight_Param.RGB_gValue_New	=	RGB_COLOR_MAX*color_pecent/RGB_LEVEL_PECENT;
					LC_RGBLight_Param.RGB_bValue_New	=	RGB_COLOR_MAX*color_pecent/RGB_LEVEL_PECENT;
					LC_RGBLight_Param.RGB_wValue_New	=	RGB_COLOR_MAX*(RGB_LEVEL_PECENT - color_pecent)/RGB_LEVEL_PECENT;
				}
				//	lightness level
				else if(LC_App_Set_Param.app_write_data[4] == 0x03){
//					uint8 lightness_level	=	0;
//					lightness_level	=	(LC_App_Set_Param.app_write_data[9] *256 + LC_App_Set_Param.app_write_data[10])/10;
					LC_RGBLight_Param.RGB_Light_Level	=	(LC_App_Set_Param.app_write_data[9] *256 + LC_App_Set_Param.app_write_data[10])/10;
					LC_RGBLight_Param.RGB_rValue		=	LC_RGBLight_Param.RGB_rValue_New*LC_RGBLight_Param.RGB_Light_Level/RGB_LEVEL_PECENT;
					LC_RGBLight_Param.RGB_gValue		=	LC_RGBLight_Param.RGB_gValue_New*LC_RGBLight_Param.RGB_Light_Level/RGB_LEVEL_PECENT;
					LC_RGBLight_Param.RGB_bValue		=	LC_RGBLight_Param.RGB_bValue_New*LC_RGBLight_Param.RGB_Light_Level/RGB_LEVEL_PECENT;
					LC_RGBLight_Param.RGB_wValue		=	LC_RGBLight_Param.RGB_wValue_New*LC_RGBLight_Param.RGB_Light_Level/RGB_LEVEL_PECENT;
				}
			}
			//	color mode
			else if(LC_App_Set_Param.app_write_data[3] == 0x01){
				uint16	HSV_value[3]	=	{0,};
				HSV_value[0]	=	LC_App_Set_Param.app_write_data[7]*256	+	LC_App_Set_Param.app_write_data[8];
				HSV_value[1]	=	LC_App_Set_Param.app_write_data[9]*256	+	LC_App_Set_Param.app_write_data[10];
				HSV_value[2]	=	LC_App_Set_Param.app_write_data[11]*256	+	LC_App_Set_Param.app_write_data[12];
				LC_RGBLight_Param.RGB_Light_Mode	=	RGB_Plate_Mode;
				HSVConvertRGB(&LC_RGBLight_Param.RGB_rValue,&LC_RGBLight_Param.RGB_gValue,&LC_RGBLight_Param.RGB_bValue, \
								HSV_value[0],HSV_value[1],HSV_value[2]);
				LC_RGBLight_Param.RGB_wValue		=	0;
				LC_RGBLight_Param.RGB_rValue_New	=	LC_RGBLight_Param.RGB_rValue;
				LC_RGBLight_Param.RGB_gValue_New	=	LC_RGBLight_Param.RGB_gValue;
				LC_RGBLight_Param.RGB_bValue_New	=	LC_RGBLight_Param.RGB_bValue;
				LC_RGBLight_Param.RGB_wValue_New	=	LC_RGBLight_Param.RGB_wValue;
			}
			//	scene mode
			else if(LC_App_Set_Param.app_write_data[3] == 0x02){
				if((LC_App_Set_Param.app_write_data[7] == 0x00) || (LC_App_Set_Param.app_write_data[7] == 0x01) || \
					(LC_App_Set_Param.app_write_data[7] == 0x02) || (LC_App_Set_Param.app_write_data[7] == 0x03)){
					uint8	light_level		=	0;
					uint8	color_pecent	=	0;
					light_level		=	(LC_App_Set_Param.app_write_data[17]*256 + LC_App_Set_Param.app_write_data[18])/10;
					color_pecent	=	(LC_App_Set_Param.app_write_data[19]*256 + LC_App_Set_Param.app_write_data[20])/10;
					LC_RGBLight_Param.RGB_Light_Mode	=	RGB_Plate_Mode;
					LC_RGBLight_Param.RGB_rValue		=	RGB_COLOR_MAX*light_level*(RGB_LEVEL_PECENT - color_pecent)/(RGB_LEVEL_PECENT*RGB_LEVEL_PECENT);
					LC_RGBLight_Param.RGB_gValue		=	RGB_COLOR_MAX*light_level*(RGB_LEVEL_PECENT - color_pecent)/(RGB_LEVEL_PECENT*RGB_LEVEL_PECENT);
					LC_RGBLight_Param.RGB_bValue		=	RGB_COLOR_MAX*light_level*(RGB_LEVEL_PECENT - color_pecent)/(RGB_LEVEL_PECENT*RGB_LEVEL_PECENT);
					LC_RGBLight_Param.RGB_wValue		=	RGB_COLOR_MAX*light_level*color_pecent/(RGB_LEVEL_PECENT*RGB_LEVEL_PECENT);
					LC_RGBLight_Param.RGB_rValue_New	=	LC_RGBLight_Param.RGB_rValue;
					LC_RGBLight_Param.RGB_gValue_New	=	LC_RGBLight_Param.RGB_gValue;
					LC_RGBLight_Param.RGB_bValue_New	=	LC_RGBLight_Param.RGB_bValue;
					LC_RGBLight_Param.RGB_wValue_New	=	LC_RGBLight_Param.RGB_wValue;
				}
			}

		}
		//	music mode
		else if((LC_App_Set_Param.app_write_data[0] == 0x08) && (LC_App_Set_Param.app_write_data[1] == 0x03) && \
			(LC_App_Set_Param.app_write_data[2] == 0x15)){
			uint16	HSV_value[3]	=	{0,};
			HSV_value[0]	=	LC_App_Set_Param.app_write_data[4]*256	+	LC_App_Set_Param.app_write_data[5];
			HSV_value[1]	=	LC_App_Set_Param.app_write_data[6]*256	+	LC_App_Set_Param.app_write_data[7];
			HSV_value[2]	=	LC_App_Set_Param.app_write_data[8]*256	+	LC_App_Set_Param.app_write_data[9];
			LC_RGBLight_Param.RGB_Light_Mode	=	RGB_Plate_Mode;
			HSVConvertRGB(&LC_RGBLight_Param.RGB_rValue,&LC_RGBLight_Param.RGB_gValue,&LC_RGBLight_Param.RGB_bValue, \
							HSV_value[0],HSV_value[1],HSV_value[2]);
			LC_RGBLight_Param.RGB_wValue		=	0;
//			LC_RGBLight_Param.RGB_rValue_New	=	LC_RGBLight_Param.RGB_rValue;
//			LC_RGBLight_Param.RGB_gValue_New	=	LC_RGBLight_Param.RGB_gValue;
//			LC_RGBLight_Param.RGB_bValue_New	=	LC_RGBLight_Param.RGB_bValue;
//			LC_RGBLight_Param.RGB_wValue_New	=	LC_RGBLight_Param.RGB_wValue;
		}
		//	set timer
		else if((LC_App_Set_Param.app_write_data[0] == 0x07) && (LC_App_Set_Param.app_write_data[1] == 0x02) && \
				(LC_App_Set_Param.app_write_data[2] == 0x04)){
			LC_RGBLight_Param.RGB_Timer_Second_Num	=	LC_App_Set_Param.app_write_data[3]*0x01000000 + \
														LC_App_Set_Param.app_write_data[4]*0x010000 + \
														LC_App_Set_Param.app_write_data[5]*0x0100 + \
														LC_App_Set_Param.app_write_data[6]*0x01;
			if(LC_RGBLight_Param.RGB_Timer_Second_Num == 0){
				osal_stop_timerEx(LC_Ui_Led_Buzzer_TaskID, UI_EVENT_LEVEL1);
			}else{
				osal_set_event(LC_Ui_Led_Buzzer_TaskID, UI_EVENT_LEVEL1);
			}
		}
		return(events ^ UI_EVENT_LEVEL2);
	}

	if(events & UI_EVENT_LEVEL3){
		uint8	Flash_Reserved_Mode[16]	=	{0x55,};

		Flash_Reserved_Mode[1]	=	LC_RGBLight_Param.RGB_Light_Mode;
		if((LC_RGBLight_Param.RGB_Light_Mode == RGB_Plate_Mode) || ((LC_RGBLight_Param.RGB_Light_Mode >= RGB_Static_Red) && (LC_RGBLight_Param.RGB_Light_Mode <= RGB_Static_White))){
			Flash_Reserved_Mode[2]	=	(uint8)((LC_RGBLight_Param.RGB_rValue >> 8) & 0xff);
			Flash_Reserved_Mode[3]	=	(uint8)(LC_RGBLight_Param.RGB_rValue & 0xff);
			Flash_Reserved_Mode[4]	=	(uint8)((LC_RGBLight_Param.RGB_gValue >> 8) & 0xff);
			Flash_Reserved_Mode[5]	=	(uint8)(LC_RGBLight_Param.RGB_gValue & 0xff);
			Flash_Reserved_Mode[6]	=	(uint8)((LC_RGBLight_Param.RGB_bValue >> 8) & 0xff);
			Flash_Reserved_Mode[7]	=	(uint8)(LC_RGBLight_Param.RGB_bValue & 0xff);
		}

		Flash_Reserved_Mode[12]	=	(uint8)((LC_RGBLight_Param.RGB_Mode_Change_Speed >> 8) & 0xff);
		Flash_Reserved_Mode[13]	=	(uint8)(LC_RGBLight_Param.RGB_Mode_Change_Speed  & 0xff);
		Flash_Reserved_Mode[14]	=	LC_RGBLight_Param.RGB_Light_Color_Sequence;
		osal_snv_write(0x88, 16, Flash_Reserved_Mode);
		LOG("write flash mode\n");
		return(events ^ UI_EVENT_LEVEL3);
	}
	

    // Discard unknown events
//	osal_start_timerEx(LC_Ui_Led_Buzzer_TaskID, UI_EVENT_LEVEL1, 1000);
    return 0;
}
/** @}*/

