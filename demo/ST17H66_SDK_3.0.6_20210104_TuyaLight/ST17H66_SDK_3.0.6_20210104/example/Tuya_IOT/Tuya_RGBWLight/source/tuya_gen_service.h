/**************************************************************************************************
THIS IS EMPTY HEADER
**************************************************************************************************/

/**************************************************************************************************
  Filename:       tuya_gen_service.h
  Revised:         
  Revision:        

  Description:    This file contains the definitions and prototypes of tuya_general_service.
**************************************************************************************************/

#ifndef _TUYA_GEN_SERVICE_H
#define _TUYA_GEN_SERVICE_H
#include "bcomdef.h"
#include "version.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "att.h"

/*********************************************************************
    CONSTANTS
*/
/*
// Profile Parameters
#define PROFILE_RAWPASS_CHAR_RX                   0  // RW uint8 - Profile Characteristic 1 value 
#define PROFILE_RAWPASS_CHAR_TX                   1  // RW uint8 - Profile Characteristic 2 value

// Simple Keys Profile Services bit fields
#define PROFILE_RAWPASS_SERVICE               0x00000001


#define RAWPASS_RX_BUFF_SIZE                  6
*/
enum
{
    TUYA_EVT_NOTI_DISABLED = 1,
    TUYA_EVT_NOTI_ENABLED,
    TUYA_EVT_BLE_DATA_RECIEVED,
};


typedef struct
{
    uint8_t   ev;
    uint8_t   len;
    uint8_t*  data;
} tuya_Srv_Evt_t;

typedef void (*tuya_ProfileChangeCB_t)(tuya_Srv_Evt_t* pev);




/*********************************************************************
    API FUNCTIONS
*/

extern bStatus_t tuya_gen_AddService( tuya_ProfileChangeCB_t cb);
//extern bStatus_t tuya_gen_AddService( void);
extern uint8 tuya_NotifyIsReady(void);
extern bStatus_t tuya_gen_Notify( uint16 connHandle, attHandleValueNoti_t* pNoti);
extern void set_tuya_Notify(void);
extern void clear_tuya_Notify(void);
extern void tuya_gen_ServiceEvt(tuya_Srv_Evt_t* pev);

#ifdef __cplusplus
}
#endif

#endif

