/**************************************************************************************************
THIS IS EMPTY HEADER
**************************************************************************************************/

/**************************************************************************************************
  Filename:       simpleBLEperipheral.h
  Revised:         
  Revision:        

  Description:    This file contains the Simple BLE Peripheral sample application
                  definitions and prototypes.

 
**************************************************************************************************/

#ifndef SIMPLEBLEPERIPHERAL_H
#define SIMPLEBLEPERIPHERAL_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */


// Simple BLE Peripheral Task Events
#define SBP_START_DEVICE_EVT                           0x0001
#define SBP_RESET_ADV_EVT                              0x0008

#define SBP_TUYA_EXEC_EVT                              0x0040
#define SBP_TUYA_BLE_CONN_EVT                          0x0080
#define SBP_TUYA_BLE_DISCONN_EVT                       0x0100

/*********************************************************************
 * MACROS
 */
#define TUYA_GAP_ADV_DATA_SIZE_MAX    (20)
#define TUYA_GAP_RSP_DATA_SIZE_MAX    (30)

extern uint8 simpleBLEPeripheral_TaskID;   // Task ID for internal task/event processing


/*********************************************************************
 * FUNCTIONS
 */

/*
 * Task Initialization for the BLE Application
 */
extern void SimpleBLEPeripheral_Init( uint8 task_id );

/*
 * Task Event Processor for the BLE Application
 */
extern uint16 SimpleBLEPeripheral_ProcessEvent( uint8 task_id, uint16 events );

extern uint8_t gap_adv_data_update(uint8_t const* p_ad_data, uint8_t ad_len);
extern uint8_t gap_rsp_data_update(uint8_t const* p_rsp_data, uint8_t rsp_len);
/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* SIMPLEBLEPERIPHERAL_H */
