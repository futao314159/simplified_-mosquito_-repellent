/**
 *	@file		LC_Common.h
 *	@author		YQ
 *	@data		10/20/2020
 *	@version	1.0.1
 *
 */

/*!
 * 	@defgroup	LC_Common
 *	@brief
 *	@{*/
#ifndef		LC_COMMON_H_
#define		LC_COMMON_H_
/*------------------------------------------------------------------*/
/*						C++ guard macro								*/
/*------------------------------------------------------------------*/
#ifdef	__cplusplus
	 extern  "C" {
#endif
/*------------------------------------------------------------------*/
/* 				 head files include 							 	*/
/*------------------------------------------------------------------*/

#include "att.h"
#include "bcomdef.h"
#include "gapbondmgr.h"
#include "gapgattserver.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "global_config.h"
#include "hci.h"
#include "hci_tl.h"
#include "linkdb.h"
#include "ll.h"
#include "ll_def.h"
#include "ll_hw_drv.h"
#include "OSAL.h"
#include "OSAL_PwrMgr.h"
#include "OSAL_Clock.h"
#include "ota_app_service.h"
#include "peripheral.h"
#include "pwm.h"
#include "pwrmgr.h"
#include "rf_phy_driver.h"
#include "simpleBLEPeripheral.h"
#include "adc.h"
#include "osal_snv.h"
/*------------------------------------------------------------------*/
/*						Pins definitions							*/
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/*						MACROS										*/
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/*						UI Task Events definitions					*/
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/* 					 Data structures							 	*/
/*------------------------------------------------------------------*/
typedef uint8_t				u8;
typedef uint16_t			u16;
typedef uint32_t			u32;
typedef signed   char		int8_t;			//!< Signed 8 bit integer
typedef unsigned char		uint8_t;		//!< Unsigned 8 bit integer
typedef signed   short		int16_t;		//!< Signed 16 bit integer
typedef unsigned short		uint16_t;		//!< Unsigned 16 bit integer
typedef signed   int		int32_t;		//!< Signed 32 bit integer
typedef unsigned int		uint32_t;		//!< Unsigned 32 bit integer
typedef signed   char		int8;			//!< Signed 8 bit integer
typedef unsigned char		uint8;			//!< Unsigned 8 bit integer
typedef signed   short		int16;			//!< Signed 16 bit integer
typedef unsigned short		uint16;			//!< Unsigned 16 bit integer
typedef signed   long		int32;			//!< Signed 32 bit integer
typedef unsigned long		uint32;			//!< Unsigned 32 bit integer


typedef struct
{
	 uint32 		 dev_timeout_poweroff_cnt;
	 uint8			 dev_poweron_switch_flag;
	 uint8			 dev_power_flag;
	 uint8			 dev_lowpower_flag;
	 uint8			 dev_ble_con_state;
	 uint8			 dev_batt_value;
}lc_dev_sys_param;

typedef struct
{
	uint8		app_write_data[250];	//	datas from APP
	uint8		app_decrype_data[250];
	uint8		app_notofy_data[250];	//	device notify
	uint8		app_write_len;
	uint8		app_notify_len;
	uint8		app_write_str_flag;		//	string data
}lc_app_set_t;

typedef	struct
{
	uint16		RGB_rValue;					//	value write in register
	uint16		RGB_gValue;
	uint16		RGB_bValue;
	uint16		RGB_wValue;
	uint16		RGB_rValue_New;				//	do not write register again 			
	uint16		RGB_gValue_New;
	uint16		RGB_bValue_New;
	uint16		RGB_wValue_New;
	uint16		RGB_Speed_Reserved;			//	original value of speed:0 - 100
	uint16		RGB_Mode_Change_Speed;		//	speed of mode
	uint32		RGB_Timer_Second_Num;		//	set second number
	uint8		RGB_Mode_Change_Color_Num;	//	number of color in dynamic mode
	uint8		RGB_Mode_Fade_Color_Num;	//	sequence of color in fade mode
	uint8		RGB_Mode_Flash_Time_Num;	//	flash times of one mode
	uint8		RGB_Light_State;			//	on	1,off	0
	uint8		RGB_Light_Color_Sequence;	//	default	0		1		2		3		4		5
											//		(R G B)	(R B G)	(G R B)	(G B R)	(B R G)	(B G R)
	uint8		RGB_Light_Level;			//	level of lightness:0--100
	uint8		RGB_Light_Mode;				//	static mode,	dynamic mode
	uint8		RGB_Light_Mode_Reserved;	//	reserved mode before turn off
	uint8		RGB_Light_Mode_Auto;		//	on	1,off	0
	uint8		RGB_Mic_Mode_Threshold;		//	
	uint8		RGB_Mic_Mode;				//	01:classical;02:soft;03:beats;04:disco
}lc_rgblight_t;

typedef	enum{
	State_Off	=	0,
	State_On,
}bit_dev_state_e;

/*------------------------------------------------------------------*/
/* 					 external variables							 	*/
/*------------------------------------------------------------------*/
extern		lc_dev_sys_param		LC_Dev_System_Param;
extern		lc_app_set_t			LC_App_Set_Param;
extern		lc_rgblight_t			LC_RGBLight_Param;
/*------------------------------------------------------------------*/
/* 					 User function prototypes					 	*/
/*------------------------------------------------------------------*/
extern		uint32		clock_time_exceed_func 			(uint32 ref, uint32 span_ms		);
extern		uint8		AsciiToHex						(uint8 * pAscii, uint8 * pHex, uint16 nLen);
extern		void		HSVConvertRGB					(uint16 *rValue, uint16 *gValue, uint16 *bValue,uint16 hValue, uint16 sValue, uint16 vValue);
extern		uint8		halfbyte_into_str				(uint8 byte						);
extern		void 		Printf_Hex 						(const uint8 *data, uint16 len	);
extern		void	 	LC_Common_ProcessOSALMsg	 	(osal_event_hdr_t *pMsg			);
extern		void 		LC_Timer_Start					(void);
extern		void 		LC_Timer_Stop					(void);


#ifdef	__cplusplus
}
#endif

#endif	/* LC_COMMON_H_ */
/** @}*/
