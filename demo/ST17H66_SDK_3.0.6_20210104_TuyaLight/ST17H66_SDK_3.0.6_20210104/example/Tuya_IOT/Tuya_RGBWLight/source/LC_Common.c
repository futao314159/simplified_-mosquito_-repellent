/**
 *	@file		LC_Common.c
 *	@author		YQ
 *	@data		09/17/2020
 *	@version	1.0.0
 *
 */

/*!
 * 	@defgroup	LC_Common
 *	@brief
 *	@{*/
 
#include "LC_Common.h"
#include "LC_UI_Led_Buzzer.h"
lc_dev_sys_param	LC_Dev_System_Param	=	
{
	.dev_timeout_poweroff_cnt	=	0xffff,
	.dev_poweron_switch_flag	=	0,
	.dev_power_flag				=	1,
	.dev_lowpower_flag			=	0,
	.dev_ble_con_state			=	0,
	.dev_batt_value				=	0,
};

lc_app_set_t		LC_App_Set_Param = 
{
	.app_write_data				=	{0},
	.app_decrype_data			=	{0},
	.app_notofy_data			=	{0},
	.app_write_len				=	0,
	.app_notify_len				=	0,
	.app_write_str_flag			=	0,
	
};
lc_rgblight_t		LC_RGBLight_Param	=
{
	.RGB_rValue					=	180,
	.RGB_gValue					=	180,
	.RGB_bValue					=	180,
	.RGB_wValue					=	180,
	.RGB_rValue_New				=	180,
	.RGB_gValue_New				=	180,
	.RGB_bValue_New				=	180,
	.RGB_wValue_New				=	180,
	.RGB_Speed_Reserved			=	50,
	.RGB_Mode_Change_Speed		=	50,
	.RGB_Timer_Second_Num		=	0,
	.RGB_Mode_Change_Color_Num	=	0,
	.RGB_Mode_Fade_Color_Num	=	0,
	.RGB_Mode_Flash_Time_Num	=	0,
	.RGB_Light_State			=	1,
	.RGB_Light_Color_Sequence 	=	0,
	.RGB_Light_Level			=	100,
	.RGB_Light_Mode				=	1,
	.RGB_Light_Mode_Reserved	=	0,
	.RGB_Light_Mode_Auto		=	0,
};

/*!
 *	@fn			clock_time_exceed_func
 *	@brief		
 */
uint32 clock_time_exceed_func(uint32 ref, uint32 span_ms)
{
#if 0
	u32 deltTick ,T0 ;
	T0 = hal_read_current_time();
	deltTick =TIME_DELTA(T0,ref);
	if(deltTick>span_ms){
		return 1 ;
	}else {
		return 0 ;
	}
#else 
	uint32 deltTick  = 0 ;
	deltTick = hal_ms_intv(ref) ;
	if(deltTick>span_ms){
		return 1 ;
	}else {
		return 0 ;
	}	
#endif
}
/*!
 *	@fn			AsciiToHex
 *	@brief		ASCII convert to Hex.
 *	@param[in]	pAscii:		pointer of ASCII data.
 *	@param[in]	pHex:		pointer of Hex data.
 *	@param[in]	nLen:		length of ASCII data,must be 2*n.
 *	@return		0 success, 1 fail.
 */
uint8	AsciiToHex(uint8 * pAscii, uint8 * pHex, uint16 nLen)
{
	uint16 nHexLen = nLen / 2;
	uint8 Nibble[2] = {0};
	uint16 i = 0;
	uint16 j = 0;

	if (nLen%2)
	{
		return 1;
	}

	for (i = 0; i < nHexLen; i ++)
	{
		Nibble[0] = *pAscii ++;		
		Nibble[1] = *pAscii ++;
		for (j = 0; j < 2; j ++)
		{
			if (Nibble[j] <= 'F' && Nibble[j] >= 'A')
				Nibble[j] = Nibble[j] - 'A' + 10;
			else if (Nibble[j] <= 'f' && Nibble[j] >= 'a')
				Nibble[j] = Nibble[j] - 'a' + 10;
			else if (Nibble[j] >= '0' && Nibble[j] <= '9')
				Nibble [j] = Nibble[j] - '0';
			else
				return 1;//Nibble[j] = Nibble[j] - 'a' + 10;
			
		}	// for (int j = ...)
		pHex[i] = Nibble[0] << 4;	// Set the high nibble
		pHex[i] |= Nibble[1];	//Set the low nibble
	}	// for (int i = ...)
	return 0;
}

void	HSVConvertRGB(uint16 *rValue, uint16 *gValue, uint16 *bValue,uint16 hValue, uint16 sValue, uint16 vValue)
{
	uint8	index	=	0;
	uint8	difs	=	0;
	float	RGB_Max,RGB_Min,RGB_Adj;

	index	=	hValue/60;
	difs	=	hValue%60;
	RGB_Max	=	vValue*0.255f;
	RGB_Min	=	RGB_Max*(1000 - sValue)/1000.0f;
	RGB_Adj	=	(RGB_Max - RGB_Min)*difs/60.0f;

	switch(index){
		case	0:
			*rValue	=	RGB_Max;
			*gValue	=	RGB_Min + RGB_Adj;
			*bValue	=	RGB_Min;
			break;
		case	1:
			*rValue	=	RGB_Max - RGB_Adj;
			*gValue	=	RGB_Max;
			*bValue	=	RGB_Min;
			break;
		case	2:
			*rValue	=	RGB_Min;
			*gValue	=	RGB_Max;
			*bValue	=	RGB_Min + RGB_Adj;
			break;
		case	3:
			*rValue	=	RGB_Min;
			*gValue	=	RGB_Max - RGB_Adj;
			*bValue	=	RGB_Max;
			break;
		case	4:
			*rValue	=	RGB_Min + RGB_Adj;
			*gValue	=	RGB_Min;
			*bValue	=	RGB_Max;
			break;
		default:
			*rValue	=	RGB_Max;
			*gValue	=	RGB_Min;
			*bValue	=	RGB_Max - RGB_Adj;
			break;
	}
//	LOG("Red %d green %d blur %d",*rValue,*gValue,*bValue);
}
/*!
 *	@fn			halfbyte_into_str
 *	@brief		MAC address transform into device name.
 *	@param[in]	byte:	half byte of MAC.
 *	@return		ASCII of halfbyte.
 */
uint8	halfbyte_into_str(uint8 byte)
{
	int8 temp = 0;
	if(byte < 0x0a)
		temp = byte + '0';
	else
		temp = byte - 0x0a + 'a';

	return temp;
}
/*!
 *	@fn			Printf_Hex
 *	@brief		printf data.
 *	@param[in]	data	:pointer of datas,
 *	@param[in]	len		:length of datas,
 *	@return		none.
 */
void Printf_Hex (const uint8 *data, uint16 len)
{
    uint16 i;

    for (i = 0; i < len - 1; i++)
    {
        LOG("%x,",data[i]);
        LOG(" ");
    }
    LOG("%x\n",data[i]);
}
/*!
 *	@fn			LC_Common_ProcessOSALMsg
 *	@brief		Process an incoming task message,nothing.
 *	@param[in]	pMsg	:message to process
 *	@return		none.
 */
void LC_Common_ProcessOSALMsg(osal_event_hdr_t *pMsg)
{
	switch(pMsg->event)
	{
		default:
			// do nothing
		break;
	}
}
void LC_Timer_Start(void)
{
	hal_timer_init(LC_RGB_Valeu_Deal);
	hal_timer_set(AP_TIMER_ID_5, 100);
	hal_timer_set(AP_TIMER_ID_6, 5*1000);
//	LOG("Start timer:\n");
}
void LC_Timer_Stop(void)
{
	hal_timer_stop(AP_TIMER_ID_5);
	hal_timer_stop(AP_TIMER_ID_6);
//	LOG("Stop timer\n");
}

/** @}*/

