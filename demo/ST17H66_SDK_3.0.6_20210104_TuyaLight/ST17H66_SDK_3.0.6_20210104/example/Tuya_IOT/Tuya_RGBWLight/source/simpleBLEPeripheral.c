/**************************************************************************************************
THIS IS EMPTY HEADER
**************************************************************************************************/

/**************************************************************************************************
  Filename:       simpleBLEPeripheral.c
  Revised:        
  Revision:       

  Description:    This file contains the Simple BLE Peripheral sample application
                  

**************************************************************************************************/
/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "rf_phy_driver.h"
#include "global_config.h"
#include "OSAL.h"
#include "OSAL_PwrMgr.h"
#include "gatt.h"
#include "hci.h"
#include "gapgattserver.h"
#include "gattservapp.h"
#include "devinfoservice.h"
#include "ota_app_service.h"
#include "peripheral.h"
#include "gapbondmgr.h"

#include "simpleBLEPeripheral.h"
#include "ll.h"
#include "ll_hw_drv.h"
#include "ll_def.h"
#include "hci_tl.h"


// start - header files for tuya ble sdk.
#include "tuya_ble_stdlib.h"
#include "tuya_ble_type.h"
#include "tuya_ble_heap.h"
#include "tuya_ble_mem.h"
#include "tuya_ble_api.h"
#include "tuya_ble_port.h"
#include "tuya_ble_main.h"
#include "tuya_ble_secure.h"
#include "tuya_ble_data_handler.h"
#include "tuya_ble_event_handler.h"
#include "tuya_ble_storage.h"
#include "tuya_ble_sdk_version.h"
#include "tuya_ble_utils.h"
#include "tuya_ble_event.h"
#include "tuya_ble_app_demo.h"
#include "tuya_ble_log.h"
#include "tuya_gen_service.h"

//#include "tuya_ble_common.h"
// end - header files for tuya ble sdk
/*********************************************************************
 * MACROS
 */
//#define LOG(...)  
/*********************************************************************
 * CONSTANTS
 */

// start - variables for tuya ble sdk.
static tuya_ble_device_param_t device_param = {0};


/// get A triple from tuya  !!!
static const char auth_key_test[] = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
static const char device_id_test[] = "xxxxxxxxxxxxxxxxxxxxxxxxx";
static const uint8_t mac_test[6] = {0x12,0x34,0x56,0x78,0x9A,0xBC}; //The actual MAC address is  

//static const char auth_key_test[] = "PAL9AvOAUryshkWmdKWwHANPjPd1OEpQ";
//static const char device_id_test[] = "tuyaeb3ce3154525";
//static const uint8_t mac_test[6] = {0x33,0x7A,0x23,0x4D,0x23,0xDC}; //The actual MAC address is  


#define APP_CUSTOM_EVENT_1  1
#define APP_CUSTOM_EVENT_2  2
#define APP_CUSTOM_EVENT_3  3
#define APP_CUSTOM_EVENT_4  4
#define APP_CUSTOM_EVENT_5  5

uint8_t uart_to_ble_enable = 0;

//static uint8_t dp_data_test[8] = {0x6C,0x02,0x05,0x00,0xAB,0x00,0x80,0x05};
static uint8_t dp_data_array[255+3];
static uint16_t dp_data_len = 0;

typedef struct {
    uint8_t data[50];
} custom_data_type_t;
// end - variables for tuya ble sdk


// How often to perform periodic event
#define SBP_PERIODIC_EVT_PERIOD					5000

#define DEVINFO_SYSTEM_ID_LEN					8
#define DEVINFO_SYSTEM_ID						0
 
#define DEFAULT_DISCOVERABLE_MODE				GAP_ADTYPE_FLAGS_GENERAL
// Minimum connection interval (units of 1.25ms, 80=100ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL		8//32//80
// Maximum connection interval (units of 1.25ms, 800=1000ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL		16//48//800
// Slave latency to use if automatic parameter update request is enabled
#define DEFAULT_DESIRED_SLAVE_LATENCY			0
// Supervision timeout value (units of 10ms, 1000=10s) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_CONN_TIMEOUT			300//1000
// Whether to enable automatic parameter update request when a connection is formed
#define DEFAULT_ENABLE_UPDATE_REQUEST			TRUE
// Connection Pause Peripheral time value (in seconds)
#define DEFAULT_CONN_PAUSE_PERIPHERAL			6
#define INVALID_CONNHANDLE						0xFFFF
// Default passcode
#define DEFAULT_PASSCODE						0//19655
// Length of bd addr as a string
#define B_ADDR_STR_LEN							15
#define RESOLVING_LIST_ENTRY_NUM				10


/*********************************************************************
 * build define
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
perStatsByChan_t g_perStatsByChanTest;
uint8 simpleBLEPeripheral_TaskID;   // Task ID for internal task/event processing

/*********************************************************************
 * EXTERNAL VARIABLES
 */
volatile uint8_t g_current_advType = LL_ADV_CONNECTABLE_UNDIRECTED_EVT;


/*********************************************************************
 * EXTERNAL FUNCTIONS
 */
/*********************************************************************
 * LOCAL VARIABLES
 */

static	gaprole_States_t	gapProfileState	=	GAPROLE_INIT;


uint8 adv_mac_data[6]= {0x00,0x00,0x00,0x00,0x00,0x00} ;   //设备的mac地址
// GAP - SCAN RSP data (max size = 31 bytes)


uint8 scanRspData[TUYA_GAP_RSP_DATA_SIZE_MAX] =
{
	0x03, 0x09, 0x54, 0x59,  //complete local name(not change)
	0x19, 0xff,             //Factory self-define data start(not change)
	0xD0, 0x07,             //COMPANY ID(TBD)
	0x00,                   //FLAG(TBD)
	0x03,                   //PROTOCOL VERSION(TBD)
	0x01,                   //ENCRYPT METHOD(TBD)
	0x00, 0x00,             //COM ABILITY(TBD)
	0x00,                   //RESERVED
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ID(16/6bytes, TBD)
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

uint8 advertData[TUYA_GAP_ADV_DATA_SIZE_MAX] =
{	
	0x02,   // length of this data
	GAP_ADTYPE_FLAGS,
	DEFAULT_DISCOVERABLE_MODE | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,

//	0x03, 0x02, 0x01, 0xa2,
	0x03, 0x02, 0x50, 0xfd,
	0x0c, 0x16,           // service data
//	0x01, 0xa2,           // service UUID ？
	0x50, 0xfd,           // service UUID ？
	0x00,                 // type 0 - pid
	'a', 'o', 'k', 'i', '8', 'v', 'o', 'r',
//	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // product PID? tbd 
/* or	
	0x03, 0x02, 0xB0, 0xFF,
	0x14, 0x16,           // service data
	0xB0, 0xFF,
	0x01,                 // type 1 - product_key
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // product_key(8/16 bytes)? tbd 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
*/
};

// GAP GATT Attributes
static uint8 attDeviceName[GAP_DEVICE_NAME_LEN] = "aoki8vor";
/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void simpleBLEPeripheral_ProcessOSALMsg( osal_event_hdr_t *pMsg );
static void peripheralStateNotificationCB( gaprole_States_t newState );
static void peripheralStateReadRssiCB( int8 rssi  );




//static uint8_t simpleBLEPeripheral_ScanRequestFilterCBack(void);
/*********************************************************************
 * PROFILE CALLBACKS
 */

// GAP Role Callbacks
static gapRolesCBs_t simpleBLEPeripheral_PeripheralCBs =
{
    peripheralStateNotificationCB,  // Profile State Change Callbacks
    peripheralStateReadRssiCB       // When a valid RSSI is read from controller (not used by application)
};

// GAP Bond Manager Callbacks, add 2017-11-15
static gapBondCBs_t simpleBLEPeripheral_BondMgrCBs =
{
  NULL,                     // Passcode callback (not used by application)
  NULL                      // Pairing / Bonding state Callback (not used by application)
};


/*********************************************************************
 * PUBLIC FUNCTIONS
 */
void custom_data_process(int32_t evt_id,void *data)
{
    custom_data_type_t *event_1_data;
    TUYA_APP_LOG_DEBUG("custom event id = %d",evt_id);
    switch (evt_id)
    {
        case APP_CUSTOM_EVENT_1:
            event_1_data = (custom_data_type_t *)data;
            TUYA_APP_LOG_HEXDUMP_DEBUG("received APP_CUSTOM_EVENT_1 data:",event_1_data->data,50);
            break;
        case APP_CUSTOM_EVENT_2:
            break;
        case APP_CUSTOM_EVENT_3:
            break;
        case APP_CUSTOM_EVENT_4:
            break;
        case APP_CUSTOM_EVENT_5:
            break;
        default:
            break;
  
    }
}


custom_data_type_t custom_data;

void custom_evt_1_send_test(uint8_t data)
{    
    tuya_ble_custom_evt_t event;
    
    for(uint8_t i=0; i<50; i++)
    {
        custom_data.data[i] = data;
    }
    event.evt_id = APP_CUSTOM_EVENT_1;
    event.custom_event_handler = (void *)custom_data_process;
    event.data = &custom_data;
    tuya_ble_custom_event_send(event);
}

void tuya_ble_get_mac(uint8_t mac[6])
{
	memcpy(mac,tuya_ble_current_para.auth_settings.mac,6);
}

static uint16_t sn = 0;
static uint32_t time_stamp = 1587795793;
static void tuya_cb_handler(tuya_ble_cb_evt_param_t* event)
{
    int16_t result = 0;
    switch (event->evt)
    {
    case TUYA_BLE_CB_EVT_CONNECTE_STATUS:
    	//tuya_uart_send_ble_state();
        TUYA_APP_LOG_INFO("received tuya ble conncet status update event,current connect status = %d",event->connect_status);
        break;
    case TUYA_BLE_CB_EVT_DP_WRITE:
        dp_data_len = event->dp_write_data.data_len;
        memset(dp_data_array,0,sizeof(dp_data_array));
        memcpy(dp_data_array,event->dp_write_data.p_data,dp_data_len);        
        TUYA_APP_LOG_HEXDUMP_DEBUG("received dp write data :",dp_data_array,dp_data_len);
        sn = 0;
        tuya_ble_dp_data_report(dp_data_array,dp_data_len);//1
        //custom_evt_1_send_test(dp_data_len);
        //tuya_ble_dp_data_report(dp_data_test,sizeof(dp_data_test));
        ///tuya_uart_send_ble_dpdata(dp_data_array,dp_data_len);
        break;
    case TUYA_BLE_CB_EVT_DP_DATA_REPORT_RESPONSE:
        TUYA_APP_LOG_INFO("received dp data report response result code =%d",event->dp_response_data.status);
        //tuya_ble_dp_data_with_flag_report(sn,REPORT_FOR_CLOUD_PANEL,dp_data_array,dp_data_len); //2       
        //sn++;
        break;
    case TUYA_BLE_CB_EVT_DP_DATA_WTTH_TIME_REPORT_RESPONSE:
        TUYA_APP_LOG_INFO("received dp data report response result code =%d",event->dp_response_data.status);
        break;
        case TUYA_BLE_CB_EVT_DP_DATA_WITH_FLAG_REPORT_RESPONSE:
        TUYA_APP_LOG_INFO("received dp data with flag report response sn = %d , flag = %d , result code =%d",event->dp_with_flag_response_data.sn,event->dp_with_flag_response_data.mode
    ,event->dp_with_flag_response_data.status);
        if(event->dp_with_flag_response_data.mode==REPORT_FOR_CLOUD_PANEL)
        {
            tuya_ble_dp_data_with_flag_report(sn,REPORT_FOR_CLOUD,dp_data_array,dp_data_len);//3
        }
        else if(event->dp_with_flag_response_data.mode==REPORT_FOR_CLOUD)
        {
            tuya_ble_dp_data_with_flag_report(sn,REPORT_FOR_PANEL,dp_data_array,dp_data_len);//4
        }
        else if(event->dp_with_flag_response_data.mode==REPORT_FOR_PANEL)
        {
            tuya_ble_dp_data_with_flag_report(sn,REPORT_FOR_NONE,dp_data_array,dp_data_len);//5
        }
        else if(event->dp_with_flag_response_data.mode==REPORT_FOR_NONE)
        {
            tuya_ble_dp_data_with_flag_and_time_report(sn,REPORT_FOR_CLOUD_PANEL,time_stamp,dp_data_array,dp_data_len);//6
        }
        else
        {
            
        }
        sn++;
        break;
    case TUYA_BLE_CB_EVT_DP_DATA_WITH_FLAG_AND_TIME_REPORT_RESPONSE:
        TUYA_APP_LOG_INFO("received dp data with flag and time report response sn = %d , flag = %d , result code =%d",event->dp_with_flag_and_time_response_data.sn,
    event->dp_with_flag_and_time_response_data.mode,event->dp_with_flag_and_time_response_data.status);
    
        if(event->dp_with_flag_and_time_response_data.mode==REPORT_FOR_CLOUD_PANEL)
        {
            tuya_ble_dp_data_with_flag_and_time_report(sn,REPORT_FOR_CLOUD,time_stamp,dp_data_array,dp_data_len);//7
        }
        else if(event->dp_with_flag_and_time_response_data.mode==REPORT_FOR_CLOUD)
        {
            tuya_ble_dp_data_with_flag_and_time_report(sn,REPORT_FOR_PANEL,time_stamp,dp_data_array,dp_data_len);//8
        }
        else if(event->dp_with_flag_and_time_response_data.mode==REPORT_FOR_PANEL)
        {
            tuya_ble_dp_data_with_flag_and_time_report(sn,REPORT_FOR_NONE,time_stamp,dp_data_array,dp_data_len);//9
        }
        else
        {
            
        }
        sn++;
       
        break;
    case TUYA_BLE_CB_EVT_UNBOUND:
        
        TUYA_APP_LOG_INFO("received unbound req");

        break;
    case TUYA_BLE_CB_EVT_ANOMALY_UNBOUND:
        
        TUYA_APP_LOG_INFO("received anomaly unbound req");

        break;
    case TUYA_BLE_CB_EVT_DEVICE_RESET:
        
        TUYA_APP_LOG_INFO("received device reset req");

        break;
    case TUYA_BLE_CB_EVT_DP_QUERY:
        TUYA_APP_LOG_INFO("received TUYA_BLE_CB_EVT_DP_QUERY event");
        uart_to_ble_enable=1;
        tuya_ble_dp_data_report(dp_data_array,dp_data_len);
        break;
    case TUYA_BLE_CB_EVT_OTA_DATA:
			  TUYA_APP_LOG_INFO("received TUYA_BLE_CB_EVT_OTA_DATA event");
        //tuya_ota_proc(event->ota_data.type,event->ota_data.p_data,event->ota_data.data_len);
        break;
    case TUYA_BLE_CB_EVT_NETWORK_INFO:
        TUYA_APP_LOG_INFO("received net info : %s",event->network_data.p_data);
        tuya_ble_net_config_response(result);
        break;
    case TUYA_BLE_CB_EVT_WIFI_SSID:

        break;
    case TUYA_BLE_CB_EVT_TIME_STAMP:
        TUYA_APP_LOG_INFO("received unix timestamp : %s ,time_zone : %d",event->timestamp_data.timestamp_string,event->timestamp_data.time_zone);
        break;
    case TUYA_BLE_CB_EVT_TIME_NORMAL:

        break;
    case TUYA_BLE_CB_EVT_DATA_PASSTHROUGH:
        TUYA_APP_LOG_HEXDUMP_DEBUG("received ble passthrough data :",event->ble_passthrough_data.p_data,event->ble_passthrough_data.data_len);
        tuya_ble_data_passthrough(event->ble_passthrough_data.p_data,event->ble_passthrough_data.data_len);
        break;
    default:
        TUYA_APP_LOG_WARNING("app_tuya_cb_queue msg: unknown event type 0x%04x",event->evt);
        break;
    }
}

/*********************************************************************
 * @fn      ty_app_init
 *
 * @brief   Initialization function for tuya_ble_sdk.
 *
 * @param   none
 *
 * @return  none
 */
void ty_app_init()   //初始化
{
    //device_param.device_id_len = 0;    //If use the license stored by the SDK,initialized to 0, Otherwise 16 or 20.
	  device_param.device_id_len = 16; 
    
	if(device_param.device_id_len==16)
	{
		memcpy(device_param.auth_key,(void *)auth_key_test,AUTH_KEY_LEN);
		memcpy(device_param.device_id,(void *)device_id_test,DEVICE_ID_LEN);
		memcpy(device_param.mac_addr.addr,mac_test,6);
		device_param.mac_addr.addr_type = TUYA_BLE_ADDRESS_TYPE_RANDOM;
	}
	else if(device_param.device_id_len==20)
	{
		memcpy(device_param.auth_key,"6DD9UMW5nNUwW4J9rXdcnpvRr9NFBGgG",AUTH_KEY_LEN);
		memcpy(device_param.device_id,"tuya86545f071ad50319",20);
		memcpy(device_param.mac_addr.addr,mac_test,6);
		device_param.mac_addr.addr_type = TUYA_BLE_ADDRESS_TYPE_RANDOM;
	}
			
	device_param.p_type = TUYA_BLE_PRODUCT_ID_TYPE_PID;
	device_param.product_id_len = 8;
	memcpy(device_param.product_id,APP_PRODUCT_ID,8);
	device_param.firmware_version = TY_APP_VER_NUM;
	device_param.hardware_version = TY_HARD_VER_NUM;

	tuya_ble_sdk_init(&device_param);
	tuya_ble_callback_queue_register(tuya_cb_handler);

//	tuya_ota_init();
//	app_ringbuffer_init();

//	tuya_log_init();
//	elog_set_output_enabled(true);

//	tuya_flash_init();
//	tuya_ota_init();

//	ty_factory_flag = 1;//?????????�??? ?????
//	tuya_timer_start(TIMER_FIRST,1000);

//	tuya_print_sysInfor();
	TUYA_APP_LOG_INFO("app version : "TY_APP_VER_STR);
//	TUYA_APP_LOG_INFO("adv data : ");
//	TUYA_BLE_HEXDUMP(advertData , TUYA_GAP_ADV_DATA_SIZE_MAX);
//	TUYA_APP_LOG_INFO("rsp data : ");
//	TUYA_BLE_HEXDUMP(scanRspData, TUYA_GAP_RSP_DATA_SIZE_MAX);
		
}
/*********************************************************************
 * @fn      SimpleBLEPeripheral_Init
 *
 * @brief   Initialization function for the Simple BLE Peripheral App Task.
 *          This is called during initialization and should contain
 *          any application specific initialization (ie. hardware
 *          initialization/setup, table initialization, power up
 *          notificaiton ... ).
 *
 * @param   task_id - the ID assigned by OSAL.  This ID should be
 *                    used to send messages and set timers.
 *
 * @return  none
 */
void SimpleBLEPeripheral_Init( uint8 task_id )
{
    simpleBLEPeripheral_TaskID = task_id;

    // Setup the GAP
    VOID GAP_SetParamValue( TGAP_CONN_PAUSE_PERIPHERAL, DEFAULT_CONN_PAUSE_PERIPHERAL );
  
    // Setup the GAP Peripheral Role Profile
    {
        // device starts advertising upon initialization
        uint8 initial_advertising_enable = TRUE;

        uint8 enable_update_request			=	DEFAULT_ENABLE_UPDATE_REQUEST;
        uint8 advChnMap						=	GAP_ADVCHAN_37 | GAP_ADVCHAN_38 | GAP_ADVCHAN_39; 
        
        // By setting this to zero, the device will go into the waiting state after
        // being discoverable for 30.72 second, and will not being advertising again
        // until the enabler is set back to TRUE
        uint16 gapRole_AdvertOffTime	=	0;
    
        uint16 desired_min_interval		=	DEFAULT_DESIRED_MIN_CONN_INTERVAL;
        uint16 desired_max_interval		=	DEFAULT_DESIRED_MAX_CONN_INTERVAL;
        uint16 desired_slave_latency	=	DEFAULT_DESIRED_SLAVE_LATENCY;
        uint16 desired_conn_timeout		=	DEFAULT_DESIRED_CONN_TIMEOUT;
		
        uint8 peerPublicAddr[] = {
			0x01,
			0x02,
			0x03,
			0x04,
			0x05,
			0x06
		};

        uint8 advType =g_current_advType;// LL_ADV_NONCONNECTABLE_UNDIRECTED_EVT;//LL_ADV_SCANNABLE_UNDIRECTED_EVT;//LL_ADV_CONNECTABLE_LDC_DIRECTED_EVT;//;    // it seems a  bug to set GAP_ADTYPE_ADV_NONCONN_IND = 0x03
        GAPRole_SetParameter( GAPROLE_ADV_EVENT_TYPE,	sizeof( uint8 ),		&advType );
		GAPRole_SetParameter( GAPROLE_ADV_DIRECT_ADDR,	sizeof(peerPublicAddr), peerPublicAddr);
        // set adv channel map
        GAPRole_SetParameter( GAPROLE_ADV_CHANNEL_MAP,	sizeof( uint8 ),		&advChnMap);        
        // Set the GAP Role Parameters
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED,	sizeof( uint8 ),		&initial_advertising_enable );
        GAPRole_SetParameter( GAPROLE_ADVERT_OFF_TIME,	sizeof( uint16 ),		&gapRole_AdvertOffTime 	);
        GAPRole_SetParameter( GAPROLE_SCAN_RSP_DATA,	sizeof( scanRspData),	scanRspData	);
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA,		sizeof( advertData ),	advertData	);
        GAPRole_SetParameter( GAPROLE_PARAM_UPDATE_ENABLE,	sizeof( uint8  ),	&enable_update_request	);
        GAPRole_SetParameter( GAPROLE_MIN_CONN_INTERVAL,	sizeof( uint16 ),	&desired_min_interval	);
        GAPRole_SetParameter( GAPROLE_MAX_CONN_INTERVAL,	sizeof( uint16 ),	&desired_max_interval	);
        GAPRole_SetParameter( GAPROLE_SLAVE_LATENCY,		sizeof( uint16 ),	&desired_slave_latency	);
        GAPRole_SetParameter( GAPROLE_TIMEOUT_MULTIPLIER,	sizeof( uint16 ),	&desired_conn_timeout	);
    }

	// Set the GAP Characteristics
	GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, attDeviceName );

    // Set advertising interval
    {
        uint16 advInt = 400;//2400;//1600;//1600;//800;//1600;   // actual time = advInt * 625us

        GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MIN, advInt );
        GAP_SetParamValue( TGAP_LIM_DISC_ADV_INT_MAX, advInt );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, advInt );
        GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, advInt );
    }
    
    // Setup the GAP Bond Manager, add 2017-11-15
    {
//		uint32 passkey = DEFAULT_PASSCODE;
//		uint8 pairMode = GAPBOND_PAIRING_MODE_WAIT_FOR_REQ;
//		uint8 mitm = TRUE;
//		uint8 ioCap = GAPBOND_IO_CAP_NO_INPUT_NO_OUTPUT;
//		uint8 bonding = TRUE;
//		GAPBondMgr_SetParameter( GAPBOND_DEFAULT_PASSCODE, sizeof ( uint32 ), &passkey );
//		GAPBondMgr_SetParameter( GAPBOND_PAIRING_MODE, sizeof ( uint8 ), &pairMode );
//		GAPBondMgr_SetParameter( GAPBOND_MITM_PROTECTION, sizeof ( uint8 ), &mitm );
//		GAPBondMgr_SetParameter( GAPBOND_IO_CAPABILITIES, sizeof ( uint8 ), &ioCap );
//		GAPBondMgr_SetParameter( GAPBOND_BONDING_ENABLED, sizeof ( uint8 ), &bonding );
    }
    
    // Initialize GATT attributes
	GGS_AddService( GATT_ALL_SERVICES );            	// 	GAP
	GATTServApp_AddService( GATT_ALL_SERVICES );    	// 	GATT attributes
//	gattServApp_RegisterCB(tuya_ProcessGATTMsg);
    tuya_gen_AddService(tuya_gen_ServiceEvt);                           // Device Information Service  Harris 12-30
//	ota_app_AddService();

	#if (0)  // max MTU shoud be 23 for Tuya
    {
		uint8_t mtuSet = 247;
		llInitFeatureSet2MPHY(TRUE);
		llInitFeatureSetDLE(TRUE);
		ATT_SetMTUSizeMax(mtuSet);
//		LOG("[2Mbps | DLE | MTU %d] \n",mtuSet);
    }
    #else
	    ATT_SetMTUSizeMax(23);
	    llInitFeatureSet2MPHY(FALSE);
	    llInitFeatureSetDLE(FALSE);
    #endif
    // Setup a delayed profile startup
    osal_set_event( simpleBLEPeripheral_TaskID, SBP_START_DEVICE_EVT );
    // for receive HCI complete message
    GAP_RegisterForHCIMsgs(simpleBLEPeripheral_TaskID);

	LL_PLUS_PerStats_Init(&g_perStatsByChanTest);

	LOG("=====SimpleBLEPeripheral_Init Done=======\n");
    ty_app_init();

}

/*********************************************************************
 * @fn      SimpleBLEPeripheral_ProcessEvent
 *
 * @brief   Simple BLE Peripheral Application Task event processor.  This function
 *          is called to process all events for the task.  Events
 *          include timers, messages and any other user defined events.
 *
 * @param   task_id  - The OSAL assigned task ID.
 * @param   events - events to process.  This is a bit map and can
 *                   contain more than one event.
 *
 * @return  events not processed
 */
uint16 SimpleBLEPeripheral_ProcessEvent( uint8 task_id, uint16 events )
{
    VOID task_id; // OSAL required parameter that isn't used in this function

    if ( events & SYS_EVENT_MSG )
    {
        uint8 *pMsg;

        if ( (pMsg = osal_msg_receive( simpleBLEPeripheral_TaskID )) != NULL )
        {
            simpleBLEPeripheral_ProcessOSALMsg( (osal_event_hdr_t *)pMsg );

            // Release the OSAL message
            VOID osal_msg_deallocate( pMsg );
        }

        // return unprocessed events
        return (events ^ SYS_EVENT_MSG);
    }

    if ( events & SBP_START_DEVICE_EVT )
    {
        // Start the Device
        VOID GAPRole_StartDevice( &simpleBLEPeripheral_PeripheralCBs );
        // Start Bond Manager, 2017-11-15
        VOID GAPBondMgr_Register( &simpleBLEPeripheral_BondMgrCBs );
        HCI_LE_ReadResolvingListSizeCmd();

        return ( events ^ SBP_START_DEVICE_EVT );
    }
    // enable adv
    if ( events & SBP_RESET_ADV_EVT )
    {
        uint8 initial_advertising_enable = TRUE;
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &initial_advertising_enable );
        return ( events ^ SBP_RESET_ADV_EVT );
    }  

	if ( events & SBP_TUYA_EXEC_EVT )
	{
		LOG("[TUYA_EXEC_EVT]\n");
		tuya_ble_main_tasks_exec();
//		tuya_exec_timer_start();

		return ( events ^ SBP_TUYA_EXEC_EVT );
	}

	if ( events & SBP_TUYA_BLE_DISCONN_EVT )
	{
		LOG("[SBP_TUYA_BLE_DISCONN_EVT]\n");
		tuya_ble_evt_param_t event;
		event.hdr.event = TUYA_BLE_EVT_CONNECT_STATUS_UPDATE;
		event.connect_change_evt = TUYA_BLE_DISCONNECTED;
		tuya_ble_handle_connect_change_evt(&event);
//		tuya_ble_event_send(&event);
//		tuya_ble_main_tasks_exec();

		return ( events ^ SBP_TUYA_BLE_DISCONN_EVT );
	}

	if ( events & SBP_TUYA_BLE_CONN_EVT )
	{
		LOG("[SBP_TUYA_BLE_CONN_EVT]\n");
		tuya_ble_evt_param_t event;
		event.hdr.event = TUYA_BLE_EVT_CONNECT_STATUS_UPDATE;
		event.connect_change_evt = TUYA_BLE_CONNECTED;
		tuya_ble_handle_connect_change_evt(&event);
//		tuya_ble_event_send(&event);
//		tuya_ble_main_tasks_exec();

		return ( events ^ SBP_TUYA_BLE_CONN_EVT );
	}

    // Discard unknown events// SBP_TUYA_BLE_DISCONN_EVT
    return 0;
}

/*********************************************************************
 * @fn      simpleBLEPeripheral_ProcessOSALMsg
 *
 * @brief   Process an incoming task message.
 *
 * @param   pMsg - message to process
 *
 * @return  none
 */
static void simpleBLEPeripheral_ProcessOSALMsg( osal_event_hdr_t *pMsg )
{
	hciEvt_CmdComplete_t *pHciMsg;

	switch ( pMsg->event ){  
		case HCI_GAP_EVENT_EVENT:{
			switch( pMsg->status ){
				case HCI_COMMAND_COMPLETE_EVENT_CODE:
					pHciMsg = (hciEvt_CmdComplete_t *)pMsg;
					LOG("==> HCI_COMMAND_COMPLETE_EVENT_CODE: %x\n", pHciMsg->cmdOpcode);
					//safeToDealloc = gapProcessHCICmdCompleteEvt( (hciEvt_CmdComplete_t *)pMsg );
				break;
				
				default:
					//safeToDealloc = FALSE;  // Send to app
				break;
			}
		}
	}
}
/*********************************************************************
 * @fn      peripheralStateReadRssiCB
 *
 * @brief   Notification from the profile of a state change.
 *
 * @param   newState - new state
 *
 * @return  none
 */
static void peripheralStateReadRssiCB( int8  rssi )
{

}

/*********************************************************************
 * @fn      peripheralStateNotificationCB
 *
 * @brief   Notification from the profile of a state change.
 *
 * @param   newState - new state
 *
 * @return  none
 */
static void peripheralStateNotificationCB( gaprole_States_t newState )
{
    switch ( newState )
    {
        case GAPROLE_STARTED:
        {
            uint8 ownAddress[B_ADDR_LEN];
            uint8 initial_advertising_enable = FALSE;//true
			GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &initial_advertising_enable );
            GAPRole_GetParameter(GAPROLE_BD_ADDR, ownAddress);
			uint8 AT_mac_address[6] = {1,2,3,4,5,6} ; 
			volatile uint8_t * P_ownPublicAddr=(volatile uint8_t *)0x1fff1231;	//0x1fff0965
			//volatile uint8_t * P_ownPublicAddr=(volatile uint8_t *)0x1fff0965;
			*(P_ownPublicAddr++)=AT_mac_address[0];  
			*(P_ownPublicAddr++)=AT_mac_address[1];  
			*(P_ownPublicAddr++)=AT_mac_address[2];  
			*(P_ownPublicAddr++)=AT_mac_address[3]; 
			*(P_ownPublicAddr++)=AT_mac_address[4];  
			*(P_ownPublicAddr++)=AT_mac_address[5];  
			HCI_ReadBDADDRCmd();
			osal_set_event(simpleBLEPeripheral_TaskID, SBP_RESET_ADV_EVT);
        }
        break;
        
        case GAPROLE_ADVERTISING:
		{
			LOG("Gaprole_adversting:\n");
		}   
        break;
        
        case GAPROLE_CONNECTED:
		{
			HCI_PPLUS_ConnEventDoneNoticeCmd(simpleBLEPeripheral_TaskID, NULL);
			VOID osal_set_event( simpleBLEPeripheral_TaskID, SBP_TUYA_BLE_CONN_EVT);
            LOG("Gaprole_Connected:\n");
		}
        break;
        case GAPROLE_CONNECTED_ADV:
	    break;      
        case GAPROLE_WAITING:
        case GAPROLE_WAITING_AFTER_TIMEOUT:
			if(gapProfileState == GAPROLE_CONNECTED)
				VOID osal_set_event( simpleBLEPeripheral_TaskID, SBP_TUYA_BLE_DISCONN_EVT);
        	LOG("Gaprole_Disconnection:\n");
		break;
        
        case GAPROLE_ERROR:
        break;
        
        default:
        break;        
    }  
    gapProfileState = newState;
	LOG("[GAP ROLE %d]\n",newState);
     
    VOID gapProfileState;     
}



/*********************************************************************
 * @fn      gap_adv_data_update
 *
 * @brief   update adv data 
 *
 */
uint8_t gap_adv_data_update(uint8_t const* p_ad_data, uint8_t ad_len)
{
	if(ad_len > TUYA_GAP_ADV_DATA_SIZE_MAX)
		return PPlus_ERR_DATA_SIZE;
	else
	{
		osal_memcpy((void *)advertData, (void *)p_ad_data, ad_len);
		GAPRole_SetParameter(GAPROLE_ADVERT_DATA, sizeof( advertData ), advertData);
		return PPlus_SUCCESS;
	}
}

/*********************************************************************
 * @fn      gap_rsp_data_update
 *
 * @brief   update rsp data 
 *
 */
uint8_t gap_rsp_data_update(uint8_t const* p_rsp_data, uint8_t rsp_len)
{
	if(rsp_len > TUYA_GAP_RSP_DATA_SIZE_MAX)
		return PPlus_ERR_DATA_SIZE;
	else
	{
		osal_memcpy((void *)scanRspData, (void *)p_rsp_data, rsp_len);
		GAPRole_SetParameter( GAPROLE_SCAN_RSP_DATA, sizeof ( scanRspData ), scanRspData );
		return PPlus_SUCCESS;
	}
}





/*********************************************************************
*********************************************************************/
